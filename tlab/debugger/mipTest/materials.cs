//==============================================================================
// GameLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

singleton CubemapData( MipCubemap ) {
	cubeFace[0] = "./TEST";
	cubeFace[1] = "./TEST";
	cubeFace[2] = "./TEST";
	cubeFace[3] = "./TEST";
	cubeFace[4] = "./TEST";
	cubeFace[5] = "./TEST";
};

singleton Material( MipCubeMat ) {
	cubemap = MipCubemap;
	materialTag0 = "Skies";
	isSky = true;
};