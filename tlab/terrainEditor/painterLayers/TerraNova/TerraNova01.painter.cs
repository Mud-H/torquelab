
new SimGroup(TPG_LayerGroup_Saved) {
   canSave = "1";
   canSaveDynamicFields = "1";

   new ScriptObject() {
      internalName = "Layer_132";
      canSave = "1";
      canSaveDynamicFields = "1";
         activeCtrl = "29327";
         coverage = "99";
         fieldsValidated = "1";
         heightMax = "175";
         heightMin = "0";
         Inactive = "0";
         isValidated = "1";
         matIndex = "1";
         matInternalName = "tn_GrassShort";
         matObject = "17570";
         pill = "29325";
         slopeMax = "17";
         slopeMin = "0";
         useTerrainHeight = "0";
   };
   new ScriptObject() {
      internalName = "Layer_185";
      canSave = "1";
      canSaveDynamicFields = "1";
         activeCtrl = "29354";
         coverage = "99";
         fieldsValidated = "1";
         heightMax = "300";
         heightMin = "0";
         Inactive = "0";
         isValidated = "1";
         matIndex = "4";
         matInternalName = "tn_SandSeaSide";
         matObject = "17582";
         pill = "29352";
         slopeMax = "29";
         slopeMin = "17";
   };
   new ScriptObject() {
      internalName = "Layer_186";
      canSave = "1";
      canSaveDynamicFields = "1";
         activeCtrl = "29381";
         coverage = "99";
         fieldsValidated = "1";
         heightMax = "300";
         heightMin = "0";
         Inactive = "0";
         isValidated = "0";
         matIndex = "2";
         matInternalName = "tn_RockNobel";
         matObject = "17576";
         pill = "29379";
         slopeMax = "90";
         slopeMin = "29";
   };
};
