//==============================================================================
// TorqueLab -> AlterVerse Tools adaptation script
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

$pref::Console::DevLogLevel = 1;
$pref::Mumble::useVoice = 0;
// Overide default tools binds with the TorqueLab ones
//devMap.unbind(keyboard,"F10");
//devMap.unbind(keyboard,"F11");
//devMap.bindCmd(keyboard,"F10","initTorqueLab(\"Gui\",true);","");
//devMap.bindCmd(keyboard,"F11","initTorqueLab(\"World\");","");
GlobalActionMap.bindCmd(keyboard,"F10","initTorqueLab(\"Gui\",true);","");
GlobalActionMap.bindCmd(keyboard,"F11","initTorqueLab(\"World\");","");
package LabTools
{
	function lateToolStart()
	{

	}
	function toggleToolNow(%type)
	{
		//  globalActionMap.unbind( keyboard, "F10");
		if (%type $= "gui")
			initTorqueLab("Gui",true);
		else
			initTorqueLab("World");
	}
	function guiEditorToggle(%make)
	{
		if (!%make)
			return;
		//devMap.unbind( keyboard, "Ctrl F10");
		//devMap.bindCmd( keyboard, "Ctrl f10", "toggleGuiEdit(true);","" );
		//GlobalActionMap.bindCmd(keyboard,"F10","initTorqueLab(\"Gui\",true);","");
		toggleGuiEdit(true);
	}

	function worldEditorToggle(%make)
	{
		if (!%make)
			return;
		//devMap.unbind( keyboard, "f11");
		//devMap.bind(keyboard, "f11", toggleEditor);
		toggleEditor(true);
	}

	function GuiEd::launchEditor( %this,%loadLast )
	{
		//hudMap.unbind( mouse, button1);
		Parent::launchEditor( %this,%loadLast );
	}
	function GuiEd::closeEditor( %this )
	{
		GlobalActionMap.bind( mouse, button1, toggleCursor);
		Parent::closeEditor( %this );
	}
	function EditorGui::onWake( %this )
	{
		GlobalActionMap.unbind( mouse, button1);
		Parent::onWake( %this );
	}
	function EditorGui::onSleep( %this )
	{
		GlobalActionMap.bind( mouse, button1, toggleCursor );
		PlayGui.startHUDDot();
		Parent::onSleep( %this );
	}

};
activatePackage(LabTools);

