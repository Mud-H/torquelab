//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
return;
$TLab_PluginName_["ModelLab"] = "Model Lab";
//------------------------------------------------------------------------------
// Shape Lab
//------------------------------------------------------------------------------

function initModelLab() {
	info( "TorqueLab","->","Initializing Model Lab");
	newScriptObject("ModelLab");
	
	execModelLab(true);
	//Lab.createPlugin("ModelLab","Shape Lab");
	Lab.addPluginGui("ModelLab",ModelLabTools);
   Lab.addPluginEditor("ModelLab",ModelLabGui);
	//%map = Lab.addPluginEditor("ModelLab",ModelLabPreview,true);
	Lab.addPluginToolbar("ModelLab",ModelLabToolbar);
	Lab.addPluginPalette("ModelLab",   ModelLabPalette);
	Lab.addPluginDlg("ModelLab",   ModelLabDialogs);
	
	ModelLabPlugin.editorGui = ModelLabGui;
	// Add windows to editor gui
	%map = Lab.addPluginMap("ModelLab");
	//%map = new ActionMap();
	%map.bindCmd( keyboard, "escape", "LabPluginArray->SceneEditorPalette.performClick();", "" );
	%map.bindCmd( keyboard, "1", "ModelLabNoneModeBtn.performClick();", "" );
	%map.bindCmd( keyboard, "2", "ModelLabMoveModeBtn.performClick();", "" );
	%map.bindCmd( keyboard, "3", "ModelLabRotateModeBtn.performClick();", "" );
	//%map.bindCmd( keyboard, "4", "ModelLabScaleModeBtn.performClick();", "" ); // not needed for the shape editor
	%map.bindCmd( keyboard, "n", "ModelLabToolbar->showNodes.performClick();", "" );
	%map.bindCmd( keyboard, "t", "ModelLabToolbar->ghostMode.performClick();", "" );
	%map.bindCmd( keyboard, "r", "ModelLabToolbar->wireframeMode.performClick();", "" );
	%map.bindCmd( keyboard, "f", "ModelLabToolbar->fitToShapeBtn.performClick();", "" );
	%map.bindCmd( keyboard, "g", "ModelLabToolbar->showGridBtn.performClick();", "" );
	%map.bindCmd( keyboard, "h", "ModelLabPropWindow->tabBook.selectPage( 2 );", "" ); // Load help tab
	%map.bindCmd( keyboard, "l", "ModelLabPropWindow->tabBook.selectPage( 1 );", "" ); // load Library Tab
	%map.bindCmd( keyboard, "j", "ModelLabPropWindow->tabBook.selectPage( 0 );", "" ); // load scene object Tab
	%map.bindCmd( keyboard, "SPACE", "ModelLabPreview.togglePause();", "" );
	%map.bindCmd( keyboard, "i", "ModelLabSequences.onEditSeqInOut(\"in\", ModelLabSeqSlider.getValue());", "" );
	%map.bindCmd( keyboard, "o", "ModelLabSequences.onEditSeqInOut(\"out\", ModelLabSeqSlider.getValue());", "" );
	%map.bindCmd( keyboard, "shift -", "ModelLabSeqSlider.setValue(ModelLabPreview-->seqIn.getText());", "" );
	%map.bindCmd( keyboard, "shift =", "ModelLabSeqSlider.setValue(ModelLabPreview-->seqOut.getText());", "" );
	%map.bindCmd( keyboard, "=", "ModelLabPreview-->stepFwdBtn.performClick();", "" );
	%map.bindCmd( keyboard, "-", "ModelLabPreview-->stepBkwdBtn.performClick();", "" );
	ModelLabPlugin.map = %map;
	
	
	//ModelLabPlugin.initSettings();
}
//==============================================================================
// Load the Scene Editor Plugin scripts, load Guis if %loadgui = true
function execModelLab(%loadGui) {
	if (%loadGui) {
		exec("tlab/ModelLab/gui/ModelLabGui.gui");
		exec("tlab/ModelLab/gui/ModelLabDialogs.gui");
		exec("tlab/ModelLab/gui/ModelLabToolbar.gui");
		exec("tlab/ModelLab/gui/ModelLabPalette.gui");
		exec("tlab/ModelLab/gui/ModelLabTools.gui");
	}

	exec("tlab/ModelLab/ModelLabPlugin.cs");
	exec("tlab/ModelLab/ModelLabTools.cs");


}
//------------------------------------------------------------------------------

function destroyModelLab() {
}
