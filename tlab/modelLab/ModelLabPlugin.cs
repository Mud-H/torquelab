//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// Scene Editor Params - Used set default settings and build plugins options GUI
//==============================================================================
//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function ModelLabPlugin::initParamsArray( %this,%array )
{
	$SceneEdCfg = newScriptObject("ModelLabCfg");
	%array.group[%gId++] = "General settings";
	%array.setVal("PreviewColorBG",     "0 0 0 0.9"   TAB "BackgroundColor" TAB "ColorEdit" TAB "" TAB "ModelLabPreviewGui-->previewBackground.setColor(*val*);" TAB %gid);
	%array.setVal("HighlightMaterial",   "1" TAB "HighlightMaterial" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.setVal("ShowNodes","1" TAB "ShowNodes" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.setVal("ShowBounds",       "0" TAB "ShowBounds" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.setVal("ShowObjBox",       "1" TAB "ShowObjBox" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.setVal("RenderMounts",       "1" TAB "RenderMounts" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.setVal("RenderCollision",       "0" TAB "RenderCollision" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.setVal("AdvancedWindowVisible",       "1" TAB "RenderCollision" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.setVal("AnimationBarVisible",       "1" TAB "RenderCollision" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.group[%gId++] = "Grid settings";
	%array.setVal("ShowGrid",       "1" TAB "ShowGrid" TAB "TextEdit" TAB "" TAB "ModelLabShapeView" TAB %gId);
	%array.setVal("GridSize",       "0.1" TAB "GridSize" TAB "TextEdit" TAB "" TAB "ModelLabShapeView" TAB %gId);
	%array.setVal("GridDimension",       "40 40" TAB "GridDimension" TAB "TextEdit" TAB "" TAB "ModelLabShapeView" TAB %gId);
	%array.group[%gId++] = "Sun settings";
	%array.setVal("SunDiffuseColor",       "255 255 255 255" TAB "SunDiffuseColor" TAB "TextEdit" TAB "" TAB "ModelLabShapeView" TAB %gId);
	%array.setVal("SunAmbientColor",       "180 180 180 255" TAB "SunAmbientColor" TAB "TextEdit" TAB "" TAB "ModelLabShapeView" TAB %gId);
	%array.setVal("SunAngleX",       "45" TAB "SunAngleX" TAB "TextEdit" TAB "" TAB "ModelLabShapeView" TAB %gId);
	%array.setVal("SunAngleZ",       "135" TAB "SunAngleZ" TAB "TextEdit" TAB "" TAB "ModelLabShapeView" TAB %gId);
}
//------------------------------------------------------------------------------

//==============================================================================
// Plugin Object Callbacks - Called from TLab plugin management scripts
//==============================================================================

//==============================================================================
// Called when TorqueLab is launched for first time
function ModelLabPlugin::onPluginLoaded( %this )
{
	

}
//------------------------------------------------------------------------------
//==============================================================================
// Called when the Plugin is activated (Active TorqueLab plugin)
$SimpleActivation = false;
function ModelLabPlugin::onActivated(%this)
{
	
}
//------------------------------------------------------------------------------
//==============================================================================
// Called when the Plugin is deactivated (active to inactive transition)
function ModelLabPlugin::onDeactivated(%this)
{
	
}
//------------------------------------------------------------------------------
//==============================================================================
// Called from TorqueLab after plugin is initialize to set needed settings
function ModelLabPlugin::onPluginCreated( %this )
{

}
//------------------------------------------------------------------------------
//==============================================================================
// Called from TorqueLab when exitting the mission
function ModelLabPlugin::onExitMission( %this )
{
	
}
//------------------------------------------------------------------------------
//==============================================================================




function ModelLabPlugin::onPreSave( %this )
{
	
}


// Replace the command field in an Editor PopupMenu item (returns the original value)
function ModelLabPlugin::replaceMenuCmd(%this, %menuTitle, %id, %newCmd)
{
}


//==============================================================================
// Called when the Plugin is activated (Active TorqueLab plugin)
function ModelLabPlugin::preparePlugin(%this)
{
}
