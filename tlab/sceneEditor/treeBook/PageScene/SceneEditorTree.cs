//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
postEvent("SceneChanged","There's no DATA");
joinEvent("SceneChanged",SceneEditorTree);
//==============================================================================
function SceneEditorTree::onSceneChanged( %this,%data )
{
    devLog("SceneEditorTree::onSceneChanged DATA:",%data);
    %this.rebuild();
}
//------------------------------------------------------------------------------
//SceneEditorTree.showInternalNames = 0;
//SceneEditorTree.showObjectNames = 1;
//SceneEditorTree.showClassNames = 0;

//==============================================================================
function SceneEditorTree::rebuild( %this )
{
    %this.clear();
    %this.open(MissionGroup);
    %this.buildVisibleTree();
}
//------------------------------------------------------------------------------

//==============================================================================
function SceneEditorTree::toggleLock( %this )
{
    if (  SceneTreeWindow-->LockSelection.command $= "EWorldEditor.lockSelection(true); SceneEditorTree.toggleLock();" )
    {
        SceneTreeWindow-->LockSelection.command = "EWorldEditor.lockSelection(false); SceneEditorTree.toggleLock();";
        SceneTreeWindow-->DeleteSelection.command = "";
    }
    else
    {
        SceneTreeWindow-->LockSelection.command = "EWorldEditor.lockSelection(true); SceneEditorTree.toggleLock();";
        SceneTreeWindow-->DeleteSelection.command = "EditorMenuEditDelete();";
    }
}
//------------------------------------------------------------------------------

