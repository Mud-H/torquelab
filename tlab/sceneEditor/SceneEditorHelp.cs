//==============================================================================
// TorqueLab -> Scene Editor Plugin Help Texts
// Copyright �2016 Alpha Entertainment Group LLC
//------------------------------------------------------------------------------
//==============================================================================
$HelpLab["SEP_InspectorMode","Title"] = "Scene Editor Inspector Settings";
$HelpLab["SEP_InspectorMode","Text"] = "The inspector is quite slow when multiple objects are selected so it's recommended to only use it when needed. To not inspect selected objects, you can simply switch to another tab like \"Builder\"" SPC
   "or uncheck \"auto\" options which will disable automatic inspecting of selected objects. When auto is off, you can still manually inspect selection by clicking the \"Do Inspect\" button.";

$HelpLab["SEP_BuilderMode","Title"] = "Scene Editor Builder Mode";
$HelpLab["SEP_BuilderMode","Text"] = "The scene editor builder mode allow fast object manipulation as the selected object are not inspected which cause slow selection when many objects are selected. The builder mode" SPC
   "is always activated when the inspector tab is not open. By unchecking the \"auto\" checkbox in inspector tab, it will work like builder mode and you can inspect object manually by clicking \"inspect\" button";
