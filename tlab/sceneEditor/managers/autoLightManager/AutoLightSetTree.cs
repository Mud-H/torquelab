//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function AutoLightSetTree::onSelect(%this,%obj)
{
	devLog("AutoLightSetTree::onSelect(%this -> %obj)",AutoLightSetTree.getSelectedObjectList());
  // Scene.selectObject(%obj);
	if (strFind(%obj.getClassName(),"Light"))
		SceneAutoLightManager.activeLight = %obj;
   
   //Make only the object selected in tree selected
   //Scene.clearSelection();
	//%selected = AutoLightSetTree.getSelectedObjectList();	
	//foreach$(%obj in %selected)
	  // Scene.selectObject(%obj);

}
//------------------------------------------------------------------------------
//==============================================================================
// Called when object added to selection, when selecting multiple using shift
// only the last will have %isLastSelection true so scene selection is updated
// only after the last is selected 
function AutoLightSetTree::onAddSelection(%this,%obj,%isLastSelection)
{
	devLog("AutoLightSetTree::onAddSelection(%this -> %obj)",%obj,%isLastSelection);
	
	
 if (!%isLastSelection)
   return;
   devLog("Scene Selection List=",AutoLightSetTree.getSelectedObjectList());
   Scene.clearSelection();
   AutoLightInspector.inspect("");
   AutoLight_PLInspectorNone.visible = 1;
	%selected = AutoLightSetTree.getSelectedObjectList();	
	foreach$(%obj in %selected)
	{
	   Scene.selectObject(%obj);
	   if (%obj.getClassName() !$= "PointLight")
	      continue;
      
      AutoLightInspector.addInspect(%obj);
        AutoLight_PLInspectorNone.visible = 0;
	}
}
//------------------------------------------------------------------------------