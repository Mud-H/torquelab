//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================
//FONTS -> Change the font to all profile or only those specified in the list
function SceneAutoLightManager::onWake(%this)
{
	SceneAutoLightManager.initSystem();
}
//------------------------------------------------------------------------------

//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
//SEP_ScatterSkyManager.buildParams();
function SceneAutoLightManager::initSystem(%this)
{
	if (isObject(sceneAutoLightShapes))
	{
		AutoLightSetTree.open(sceneAutoLightShapes);
		AutoLightSetTree.buildVisibleTree();
	}	
}

//------------------------------------------------------------------------------
function Lab::setAutoLight(%this,%obj)
{
	if (!isObject(%obj))
		return;

	Lab.activeAutoLightObj = %obj;
	SceneAutoLightManager.activeLight = %obj;
}

