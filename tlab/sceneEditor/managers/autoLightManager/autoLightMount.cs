//==============================================================================
// Boost! -> GuiControl Functions Helpers
// Copyright NordikLab Studio, 2013
//==============================================================================
//==============================================================================
// Schedule global on-off - Used to limit output of fast logs
//==============================================================================
$HLab_AutoLightFields = "brightness radius attenuationRatio flareType flareScale castShadows shadowType texSize";

function checkActiveGroupLightShapes()
{
   $SceneEdCheckLightRoot = Scene.getActiveSimGroup();
	checkLightShapes(Scene.getActiveSimGroup());
	
}
function checkMissionGroupLightShapes()
{
   $SceneEdCheckLightRoot = MissionGroup;
	checkLightShapes(MissionGroup);	
}
function checkGroupLightShapes(%group)
{
   $SceneEdCheckLightRoot = %group;
	checkLightShapes(%group);	
}

function checkLightShapes(%group, %depth)
{
   %depth = %group.getGroupDepth($SceneEdCheckLightRoot);
   devLog("checkLightShapes",%group,"Depth",%depth,%group.internalName);
   
	if (%depth > 20)
	{
		return;
	}

	if (!isObject(%group))
		%group = mgLightShapes;

	if (!isObject(%group))
		return;

	foreach(%obj in %group)
	{
		if (%obj.isMemberOfClass("TSStatic"))
			AddLightObj(%obj);
		else if (%obj.getClassName() $= "SimSet" && !$Cfg_SceneEditor_Autolight_NoRecurse)
			checkLightShapes(%obj,%depth++);
      else if (%obj.getClassName() $= "SimGroup" && !$Cfg_SceneEditor_Autolight_NoRecurse)
			checkLightShapes(%obj,%depth++);
	}
}
function AddLightObj(%obj)
{
	if (!isObject(MissionGroup))
	{
		warnLog("Can't add lights, no MissionGroup found.");
		return;
	}

   if (%obj.getClassName() !$= "TSStatic")
	{
		warnLog("Trying to mount light to something else than a TSStatic",%obj.getClassName());
		if (%obj.getClassName() $= "SimGroup")
		   checkGroupLightShapes(%obj);
		return;
	}
	
	if (!isObject(%obj))
	{
		warnLog("Can't add lights, invalid object supplied:",%obj);
		return;
	}

	if (%obj.refLight !$= "" && !isObject(%obj.refLight))
		%obj.refLight = "";

	// Get a TSShapeConstructor for this object (use the ShapeLab
	// utility functions to create one if it does not already exist).
	%shapePath = getObjectShapeFile(%obj);
	%shape = findConstructor(%shapePath);

	if (!isObject(%shape))
		%shape = createConstructor(%shapePath);

	if (!isObject(%shape))
	{
		echo("Failed to create TSShapeConstructor for " @ %obj.getId());
		return;
	}
	//devLog("Shape is:",%shape,"Path:",%shapePath);

	%lightsGroup = "Lights";

	if (%shape.getNodeIndex(%lightsGroup) $= "-1")
		return;

	%objTransform = %obj.getTransform();

	
	if (!isObject(sceneAutoLights))
	{
		newSimSet("sceneAutoLights");
	}

	if (!isObject(sceneAutoLightShapes))
	{
		newSimSet("sceneAutoLightShapes");
	}

	if (%obj.name $= "")
	{
		%name = getUniqueName("LightShape_");
		%obj.setName(%name);
	}

	

	%lightCreated = false;
	%shapeName = fileBase(%obj.shapeName);
	%count = %shape.getNodeChildCount(%lightsGroup);

	for(%i = 0; %i < %count; %i++)
	{
		// get node transform in object space, then transform to world space
		%child = %shape.getNodeChildName(%lightsGroup, %i);
		%txfm = %shape.getNodeTransform(%child, true);
		%txfm = MatrixMultiply(%objTransform, %txfm);

		if (isObject(%obj.myLight[%i]))
		{
			%light = %obj.myLight[%i];
			%light.position = getWords(%txfm, 0, 2);
			%light.rotation = getWords(%txfm, 3, 6);
		}
		else
		{
			// create a new light at the object node
			%light = new PointLight()
			{
				position = getWords(%txfm, 0, 2);
				rotation = getWords(%txfm, 3, 6);
				radius = "20";
				isEnabled = "1";
				color = "0.996078 0.996078 0.478431 1";
				brightness = "1";
				shadowType = "DualParaboloidSinglePass";
				texSize = "512";
			};
			%light.myShape = %obj.getName();
			%light.constructObj = %shape;
			%obj.myLight[%i] = %light;
			%light.internalName = %shapeName;
			%light.myNodeId = %i;
		}

		if (!isObject(%obj.refLight))
			%obj.refLight = %light;

      %obj.add(%light);

		//AutoMountLights.add(%light);
		sceneAutoLights.add(%light);
		%lightCreated = true;

		foreach$(%field in $HLab_AutoLightFields)
		{
			%value = %obj.getFieldValue(%field);

			if (%value $= "")
				continue;

			%light.setFieldValue(%field,%value);
		}
	}

	if (%lightCreated)
	{
		sceneAutoLightShapes.add(%obj);
		//%subGroup.add(%obj);
	}
}

