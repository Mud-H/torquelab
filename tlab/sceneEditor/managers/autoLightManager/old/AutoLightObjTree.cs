//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function AutoLightObjTree::onSelect(%this,%obj)
{
	devLog("AutoLightObjTree::onSelect(%this -> %obj)",%obj);
   Scene.selectObject(%obj);
	if (strFind(%obj.getClassName(),"Light"))
		SceneAutoLightManager.activeLight = %obj;
}
//------------------------------------------------------------------------------
