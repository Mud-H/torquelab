//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
//SceneEd.genAutoLightSelection()
//SceneEd.updateAutoLightTree();
function SceneEd::genAutoLightSelection( %this ) {
	%count = EWorldEditor.getSelectionSize();
	if (%count < 1) {
		warnLog("There's no selected objects to generate lights!");
		return;
	}
	
	for( %j=0; %j<%count; %j++) {
		%obj = EWorldEditor.getSelectedObject( %j );
		AddLightObj(%obj);
	}	
}
//------------------------------------------------------------------------------

//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function SceneEd::updateAutoLightTree( %this ) {
	AutoLightShapeTree.open(AutoLightsSet);
	AutoLightShapeTree.buildVisibleTree();
}
//------------------------------------------------------------------------------


//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function SceneEd::scanAutoLights( %this ) {
	//%lightShapes = getMissionObjectClassList("TSStatic","refLight NotEmpty");
	//foreach$(%shape in %lightShapes)
		//AutoLightsSet.add(%shape);
		
	SceneEd.updateAutoLightTree();
}
//------------------------------------------------------------------------------
