//==============================================================================
// TorqueLab -> SceneEditor Inspector script
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
// Allow to manage different GUI Styles without conflict
//==============================================================================

//==============================================================================
// Prefabs
//==============================================================================
function SceneEditorPlugin::onSelectObject( %this,%obj ) {
	logd("SceneEditorPlugin::onSelectObject:",%obj);
	SceneEd.setActiveObject(%obj);

	//if (%obj.getClassname() $= "Prefab")
		//SceneEd.onPrefabSelected(%obj);
	//else
		//SceneEd.noPrefabSelected();

	
}
function SceneEditorPlugin::onUnselect( %this,%obj ) {
	logd("SceneEditorPlugin::onUnselect:",%obj);

}
