//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================


//==============================================================================
//FONTS -> Change the font to all profile or only those specified in the list
function SceneEd::initAreaObjectTools( %this ) {
   
   SETools_CreateOptClassMenu.clear();
   SETools_CreateOptClassMenu.add("Trigger",0);
	SETools_CreateOptClassMenu.add("Zone",1);
	SETools_CreateOptClassMenu.add("Portal",2);
	SETools_CreateOptClassMenu.add("OcclusionVolume",3);
	SETools_CreateOptClassMenu.setText("Zone");
	
	%transformStack = SEP_TransformRollout-->toolsStack;
	if (!isObject(%transformStack))
		return;
	%stackList = %transformStack.getId() SPC "SEP_PageSetupStack SEP_PageUtilitypStack";
	foreach$(%stack in %stackList){
		foreach(%gui in %stack){
			%gui.expanded = false;
		}
	}
	
	
   %triggerDatas = getDatablockClassList("TriggerData");
   foreach$(%trigger in %triggerDatas)
      SETools_TriggerDatablock.add(%trigger.getName(),%trigger.getId());
   
   SETools_TriggerDatablock.setSelected(0,false);
   

}
//------------------------------------------------------------------------------


//==============================================================================
//FONTS -> Change the font to all profile or only those specified in the list
function SceneEd::createAreaObject( %this ) {
   %class = SETools_CreateOptClassMenu.getText();
   if (SEPutil_AreaObject-->TriggerData.visible)
      %datablock = SETools_TriggerDatablock.getText();
      
   %margin = SETools_CreateOptClassMargin.getText();
   Scene.createPolyhedralFromSelection(%class,"",%datablock,%margin);

}
//------------------------------------------------------------------------------

//==============================================================================
//FONTS -> Change the font to all profile or only those specified in the list
function SETools_CreateOptClassMenu::onSelect( %this,%id,%text ) {
  SceneEd.areaObjectClass = %text;
   
  SEPutil_AreaObject-->TriggerData.visible = (%text $= "Trigger");

}
//------------------------------------------------------------------------------


function SETools_CreateOptClassMargin::onValidate( %this ) {
	%text = SETools_CreateOptClassMargin.getText();
	if (!strIsNumeric(%text.x))
		%failed = true;
	if (!strIsNumeric(%text.y))
		%failed = true;
	if (!strIsNumeric(%text.z))
		%failed = true;
	
	if (%failed){
		SETools_CreateOptClassMargin.setText("0 0 0");
		return;
	}
	SETools_CreateOptClassMargin.setText(%text.x SPC %text.y SPC %text.z);
	
}