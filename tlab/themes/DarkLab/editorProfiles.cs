//==============================================================================
// GameLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

singleton GuiControlProfile( EditorGuiGenericProfile ) {
	canKeyFocus = true;
	opaque = true;
	fillColor = "192 192 192 192";
	category = "Editor";		
};


singleton GuiControlProfile (EditorDefaultProfile : ToolsDefaultProfile) {
	opaque = true;
	category = "Editor";
};



singleton GuiControlProfile (WorldEditorProfile) {
	canKeyFocus = true;
	category = "Editor";
	modal = true;
};


