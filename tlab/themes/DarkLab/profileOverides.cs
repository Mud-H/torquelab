//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
return;
new GuiControlProfile (ToolsDefaultProfile ) {
   tab = false;
   canKeyFocus = false;
   hasBitmapArray = false;
   mouseOverSelected = false;
   
    // fill color
   opaque = false;
   fillColor = "243 241 241 56";
	fillColorHL ="229 229 236";
	fillColorSEL = "99 101 138 ";
	fillColorNA = "255 255 255 ";

   // border color
   border = 0;

    borderColor = "101 101 101 69";
    borderColorHL = "3 224 48 141";
    borderColorNA = "30 3 240 255";   
    bevelColorLL = "101 101 101 0";
   bevelColorHL = "124 124 124 84";
   
   
   // font
   //fontType = "Calibri";
   fontType = "Calibri";
   fontSize = 14;
   fontCharset = ANSI;

   fontColor = "0 0 0";
   fontColorHL = "0 0 0";
   fontColorNA = "0 0 0";
   fontColorSEL= "255 255 255";

   // bitmap information
   bitmapBase = "";
   textOffset = "0 0";

   // used by guiTextControl
   modal = true;
   justify = "left";
   autoSizeWidth = false;
   autoSizeHeight = false;
   returnTab = false;
   numbersOnly = false;
   cursorColor = "0 0 0 255";		
   fillColorERR = "Red";
   borderThickness = "0";
   category = "Tools";
};

devLog("Profile overides loaded before stock profiles",isObject(GuiTextEditProfile));
singleton GuiControlProfile( GuiTextEditProfile : ToolsDefaultProfile)
{
   opaque = true;
   hasBitmapArray = "0"; 
   border = "1"; // fix to display textEdit img
   //borderWidth = "1";  // fix to display textEdit img
   //borderColor = "100 100 100";
   fillColor = "2 2 2 255";
   fillColorHL = "255 255 255";
   fontColor = "254 240 164 255";
   fontColorHL = "255 255 255";
   fontColorSEL = "98 100 137";
   fontColorNA = "200 200 200";
   textOffset = "0 0";
   autoSizeWidth = false;
   autoSizeHeight = true;
   justify = "Right";
   tab = true;
   canKeyFocus = true;   
   category = "Core";
   borderColor = "30 108 182 255";
   bevelColorHL = "67 127 215 255";
   bevelColorLL = "46 132 199 255";
   fontType = "Calibri";
   fontColors[0] = "254 240 164 255";
   fontColors[1] = "255 255 255 255";
   fontColors[2] = "200 200 200 255";
   fontColors[3] = "98 100 137 255";
   fontColors[6] = "255 0 255 255";
   bitmap = "tlab/themes/DarkLab/element-assets/GuiTextEditMain.png";
};


singleton GuiControlProfile( GuiPopUpMenuProfile : ToolsDefaultProfile )
{
   textOffset         = "6 4";
   bitmap             = "tlab/themes/DarkLab/element-assets/GuiDropdownBase_20.png";
   hasBitmapArray     = true;
   border             = 1;
   profileForChildren = "ToolsDropdownProfile_List";
   category = "Core";
   fontSize = "17";
   fontColors[0] = "210 210 210 255";
   fontColors[1] = "171 171 171 255";
   fontColors[2] = "248 248 248 255";
   fontColor = "210 210 210 255";
   fontColorHL = "171 171 171 255";
   fontColorNA = "248 248 248 255";
   opaque = "1";
   fillColor = "41 41 41 255";
   fillColorHL = "2 2 2 255";
   fillColorNA = "2 2 2 255";
   fontColors[3] = "243 243 243 255";
   fontColorSEL = "243 243 243 255";
};
