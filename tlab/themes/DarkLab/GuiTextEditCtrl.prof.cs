//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
// ->Added Gui style support
// -->Delete Profiles when reloaded
// -->Image array store in Global
//==============================================================================

//==============================================================================
// GuiTextEditCtrl Profiles
//==============================================================================

//==============================================================================
//ToolsTextEdit - Base profile
singleton GuiControlProfile( ToolsTextEdit : ToolsDefaultProfile ) {	
	bitmap = "tlab/themes/DarkLab/element-assets/GuiTextEditProfile.png";
	bevelColorLL = "255 0 255 255";
	fontSize = "15";
	textOffset = "2 0";
	justify = "Left";
	fontColors[0] = "252 254 252 255";
	fontColors[1] = "34 102 132 150";
	fontColors[2] = "243 243 243 255";
	fontColors[3] = "254 185 5 255";
	fontColors[9] = "255 0 255 255";
	fontColor = "252 254 252 255";
	fontColorHL = "34 102 132 150";
	fontColorNA = "243 243 243 255";
	fontColorSEL = "254 185 5 255";
	fontColors[7] = "255 0 255 255";
	fontColors[6] = "Magenta";
	hasBitmapArray = "1";
   border = "-2";
   borderThickness = "2";
	
	tab = "1";
	canKeyFocus = "1";
   fontColors[4] = "Fuchsia";
   fontColorLink = "Fuchsia";

   category = "ToolsText";
   fillColorNA = "138 138 138 222";
   fillColorSEL = "99 101 138 241";
   borderColor = "0 255 21 255";
   borderColorNA = "2 175 64 255";
   opaque = "1";
   fontColors[5] = "255 0 255 255";
   fontColorLinkHL = "255 0 255 255";
   borderColorHL = "3 12 250 255";
};
//------------------------------------------------------------------------------
// ToolsTextEdit_S1 - Child profile
singleton GuiControlProfile(ToolsTextEdit_S1 : ToolsTextEdit) {
	fontSize = "14";  
};
//------------------------------------------------------------------------------

//==============================================================================
// ToolsTextEditMain - Variation of TextEdit
//==============================================================================

//==============================================================================
// ToolsTextEditMain - Base profile
singleton GuiControlProfile(ToolsTextEditMain : ToolsTextEdit)
{
   bitmap = "tlab/themes/DarkLab/element-assets/GuiTextEditMain.png";
   border = "-1";
   fontColors[6] = "255 0 255 255";
};
//------------------------------------------------------------------------------

//==============================================================================
// ToolsTextEditAlt - Variation of TextEdit
//==============================================================================

//==============================================================================
//ToolsTextEditAlt - Base profile
singleton GuiControlProfile(ToolsTextEditAlt : ToolsTextEditMain)
{
   bitmap = "tlab/themes/DarkLab/element-assets/GuiTextEditAlt.png";
   border = "-2";
   borderThickness = "2";
};
//------------------------------------------------------------------------------

//==============================================================================
// ToolsTextPadEdit - MLTextEdit profile used by TextPad and other MLTextEdit
//==============================================================================

//==============================================================================
//ToolsTextEditNum - Base profile
singleton GuiControlProfile(ToolsTextPadEdit : ToolsTextEdit)
{
   fillColor = "238 236 241 9";
   borderColorHL = "231 231 231 236";

   fontSize = "17";
   fontColors[0] = "236 236 236 255";
   fontColors[1] = "3 3 3 255";
   fontColors[2] = "254 227 83 255";
   fontColors[3] = "0 0 2 255";
   fontColors[4] = "62 99 254 255";
   fontColor = "236 236 236 255";
   fontColorHL = "3 3 3 255";
   fontColorNA = "254 227 83 255";
   fontColorSEL = "0 0 2 255";
   fontColorLink = "62 99 254 255";
   category = "ToolsText";
   dynamicField = "defaultValue";
};
//------------------------------------------------------------------------------

//==============================================================================
// ToolsTextEditNum - TextEdit for numeric integers
//==============================================================================

//==============================================================================
//ToolsTextEditNum - Base profile
singleton GuiControlProfile( ToolsTextEditNum : ToolsTextEdit ) {
	numbersOnly = true;	
};
//------------------------------------------------------------------------------
//==============================================================================
// ToolsTextEditNum_L1 - Larger font
singleton GuiControlProfile(ToolsTextEditNum_L1 : ToolsTextEditNum)
{
   fontType = "Calibri Bold";
   fontSize = "18";
   justify = "Left";
};
//------------------------------------------------------------------------------

//==============================================================================
// ToolsTextEditNumSlider - TextEdit for numeric integers
//==============================================================================

//==============================================================================
//ToolsTextEditNumSlider - Base profile
singleton GuiControlProfile(ToolsTextEditNumSlider : ToolsTextEditNum)
{
   bitmap = "tlab/themes/DarkLab/element-assets/GuiTextEditNumBox.png";
   opaque = "0";
   border = "-3";
   borderThickness = "3";
   hasBitmapArray = "1";
   renderType = "4";
   fillColorHL = "5 9 92 204";
   borderColor = "143 173 196 241";
   fontColors[6] = "Magenta";
   textOffset = "4 0";
};
//------------------------------------------------------------------------------

singleton GuiControlProfile(ToolsTextEditFrame : ToolsTextEdit)
{
   fontColors[7] = "Fuchsia";
   border = "-2";
};

singleton GuiControlProfile(ToolsTextEdit_L1 : ToolsTextEdit)
{
   fontSize = "17";
   textOffset = "4 2";
};
