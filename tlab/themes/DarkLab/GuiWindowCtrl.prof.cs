//==============================================================================
// TorqueLab -> Default Containers Profiles
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// GuiWindowCtrl Profiles
//==============================================================================

//==============================================================================
//ToolsWindowPanel Style
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

singleton GuiControlProfile(ToolsWindowPanel : ToolsDefaultProfile)
{
   fontColors[8] = "255 0 255 255";
   bitmap = "tlab/themes/DarkLab/container/GuiWindowPanel.png";
   
   fontSize = "18";
   fontColors[0] = "234 234 234 255";
   fontColor = "234 234 234 255";
   textOffset = "6 0";
   border = "-1";
};
singleton GuiControlProfile(ToolsWindowPanel_L1 : ToolsWindowPanel)
{   
   bitmap = "tlab/themes/DarkLab/container/GuiWindowPanel_L1.png";
   fontSize = "22";
  
};
//==============================================================================
//ToolsWindowDialog Style
//------------------------------------------------------------------------------
singleton GuiControlProfile(ToolsWindowDialog : ToolsWindowPanel) {
	bitmap = "tlab/themes/DarkLab/container/GuiWindowDialog.png";
	fillColor = "37 37 37 255";
	fontColors[0] = "245 245 245 255";
	fontColor = "245 245 245 255";
	textOffset = "6 -1";
	fontColors[3] = "255 255 255 255";
	fontColorSEL = "255 255 255 255";
	fontColors[9] = "Fuchsia";
	fillColorHL = "228 228 235 255";
	fillColorNA = "255 255 255 255";
	fillColorSEL = "194 194 196 255";
	borderColor = "200 200 200 255";

	fontSize = "19";
	fontColors[7] = "255 0 255 255";
	bevelColorLL = "Black";
   fontColors[5] = "255 0 255 255";
   fontColorLinkHL = "255 0 255 255";
   border = "-2";
   justify = "Left";
};
//------------------------------------------------------------------------------


//==============================================================================
//ToolsWindowRollout Style (A- Dark and B - Light)
//------------------------------------------------------------------------------

singleton GuiControlProfile(ToolsWindowRolloutA : ToolsWindowPanel)
{
   bitmap = "tlab/themes/DarkLab/container/GuiWindowRolloutA.png";
   border = "-2";
   fontSize = "20";
   textOffset = "6 0";
   fontColors[0] = "199 199 199 255";
   fontColor = "199 199 199 255";
   fillColor = "15 15 15 255";
   fillColorERR = "255 0 0 255";
   fontColors[4] = "Fuchsia";
   fontColorLink = "Fuchsia";
   fillColorSEL = "99 101 138 255";
   borderThickness = "0";
};

singleton GuiControlProfile(ToolsWindowRolloutA_S1 : ToolsWindowRolloutA)
{   
   fontSize = "15";   
   bitmap = "tlab/themes/DarkLab/container/GuiWindowRolloutA_S1.png";
   fontColors[5] = "Magenta";
   fontColorLinkHL = "Magenta";
   textOffset = "8 1";
};

singleton GuiControlProfile(ToolsWindowRolloutB : ToolsWindowRolloutA)
{
   bitmap = "tlab/themes/DarkLab/container/GuiWindowRolloutB.png";
   fontSize = "19";
   textOffset = "8 2";
};

singleton GuiControlProfile(ToolsWindowRolloutB_S1 : ToolsWindowRolloutB)
{
   fontSize = "16";   
   bitmap = "tlab/themes/DarkLab/container/GuiWindowRolloutB_S1.png";
   fontColors[0] = "254 254 254 255";
   fontColor = "254 254 254 255";
   textOffset = "8 2";
};



singleton GuiControlProfile(ToolsWindowBox : ToolsWindowPanel)
{
   bitmap = "tlab/themes/DarkLab/container/GuiWindowBox.png";
   fillColor = "12 12 12 255";
};
