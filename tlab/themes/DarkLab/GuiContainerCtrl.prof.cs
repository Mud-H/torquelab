//==============================================================================
// TorqueLab -> Default ToolBoxes Profiles
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// Box Content Profiles - Define areas for contents
//==============================================================================
//==============================================================================
//ToolsBoxContentA Style
singleton GuiControlProfile( ToolsBoxContentA : ToolsDefaultProfile ) {	
	category = "ToolsContainers";	
   fillColor = "18 18 18 220";
   bitmap = "tlab/themes/DarkLab/container/GuiBoxContentPanelA.png";
   hasBitmapArray = "1";
   border = "-2";
   opaque = "1";
   
  
   fontType = "Calibri Bold";
   fontSize = "17";
   fontColors[0] = "254 254 254 255";
   fontColor = "254 254 254 255";
   textOffset = "8 4";
   borderThickness = "0";
   borderColorNA = "30 3 240 255";
   borderColor = "101 101 101 69";
   borderColorHL = "3 224 48 141";
   bevelColorHL = "124 124 124 84";
   bevelColorLL = "101 101 101 0";
};
//------------------------------------------------------------------------------
//==============================================================================
//ToolsBoxContentA Style
singleton GuiControlProfile( ToolsBoxContentB : ToolsBoxContentA ) {		
   fillColor = "25 25 25 0";
   bitmap = "tlab/themes/DarkLab/container/GuiBoxContentPanelB.png";    
   fontSize = "19";
   fontColors[0] = "254 254 254 255";
   fontColor = "254 254 254 255";
   opaque = "0";
};
//------------------------------------------------------------------------------

//==============================================================================
//ToolsBoxContentB Style
singleton GuiControlProfile( ToolsBoxContentC : ToolsBoxContentA ) {		
   bitmap = "tlab/themes/DarkLab/container/GuiBoxContentPanelC.png";
   fillColor = "32 32 32 0";
   fontSize = "19";
   fontColors[0] = "254 254 254 255";
   fontColor = "254 254 254 255";  
   fillColorERR = "255 0 0 255";   
   fontColors[8] = "Magenta";
   opaque = "0";
};
//------------------------------------------------------------------------------

singleton GuiControlProfile(ToolsBoxContentA_NM : ToolsBoxContentA)
{
   modal = "0";
   opaque = "0";
};

singleton GuiControlProfile(ToolsBoxContentB_NM : ToolsBoxContentB)
{
   modal = "0";
};
singleton GuiControlProfile(ToolsBoxContentC_NM : ToolsBoxContentC)
{
   modal = "0";
};
//==============================================================================
// Box Content Title - Same as stock box but with the window panel header
//==============================================================================
singleton GuiControlProfile(ToolsBoxContentTitleA : ToolsBoxContentA)
{   
   bitmap = "tlab/themes/DarkLab/container/GuiBoxContentTitleA.png";   
   border = "0";
   fontType = "Calibri";
   fontSize = "18";
   textOffset = "8 1";
};
//------------------------------------------------------------------------------
singleton GuiControlProfile(ToolsBoxContentTitleB : ToolsBoxContentB)
{
  bitmap = "tlab/themes/DarkLab/container/GuiBoxContentTitleB.png";   
   opaque = "1";
   borderThickness = "1";
   fontType = "Calibri";
   fontSize = "18";
   textOffset = "4 0";
};
//------------------------------------------------------------------------------
singleton GuiControlProfile(ToolsBoxContentTitleC : ToolsBoxContentC)
{
   bitmap = "tlab/themes/DarkLab/container/GuiBoxContentTitleC.png";
};


singleton GuiControlProfile(ToolsBoxTitleB : ToolsBoxContentTitleB)
{
   border = "0";
   fontType = "Calibri Bold";
   fontSize = "19";
   textOffset = "8 4";
};

singleton GuiControlProfile(ToolsBoxDarkA_T40 : ToolsBoxContentA)
{
   bitmap = "tlab/themes/DarkLab/container/GuiBoxDarkA_T40.png";
   border = "-2";
   borderThickness = "2";
};

singleton GuiControlProfile(ToolsBoxDarkB_T40 : ToolsBoxContentA)
{
   bitmap = "tlab/themes/DarkLab/container/GuiBoxDarkB_T40.png";
   border = "-2";
};

singleton GuiControlProfile(ToolsBoxDarkTitleA_T40 : ToolsBoxDarkA_T40)
{
   bitmap = "tlab/themes/DarkLab/container/GuiBoxDarkTitleA_T40.png";
   fontType = "Calibri";
   fontSize = "19";
   textOffset = "4 0";
};

singleton GuiControlProfile(ToolsBoxDarkTitleB_T40 : ToolsBoxDarkB_T40)
{
   bitmap = "tlab/themes/DarkLab/container/GuiBoxDarkTitleB_T40.png";
   textOffset = "6 -1";
   fontType = "Calibri";
   fontSize = "19";
};
