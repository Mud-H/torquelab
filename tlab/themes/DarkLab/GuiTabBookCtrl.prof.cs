//==============================================================================
// TorqueLab -> Default Containers Profiles
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// GuiTabBookCtrl Profiles
//==============================================================================

//==============================================================================
//ToolsTabBookMain Style
//------------------------------------------------------------------------------
singleton GuiControlProfile( ToolsTabBookMain : ToolsDefaultProfile ) {
	hasBitmapArray = true;
	category = "ToolsContainers";
	fontColors[9] = "255 0 255 255";
	autoSizeWidth = "1";
	autoSizeHeight = "1";
	profileForChildren = "ToolsBoxDarkC_Top";
	bitmap = "tlab/themes/DarkLab/container/GuiTabBookProfile.png";
	border = "0";
	borderThickness = "1";

	fontSize = "16";
	justify = "Center";
	fontColors[0] = "254 254 254 255";
	fontColor = "254 254 254 255";
	textOffset = "0 0";
	fontColors[3] = "146 146 146 255";
	fontColorSEL = "146 146 146 255";
	opaque = "1";
	fillColor = "3 3 3 0";
	borderColor = "46 46 46 102";
   fontColors[1] = "164 164 164 255";
   fontColorHL = "164 164 164 255";
   fillColorHL = "43 43 43 255";
   fillColorNA = "76 76 76 255";
   borderColorHL = "64 234 139 255";
   borderColorNA = "64 64 64 103";
   bevelColorHL = "43 43 43 204";
   bevelColorLL = "115 115 115 50";
};
//------------------------------------------------------------------------------

singleton GuiControlProfile(ToolsTabBookMain_S1 : ToolsTabBookMain)
{
   fontSize = "14";
   fontColors[0] = "254 248 217 255";
   fontColor = "254 248 217 255";
   bitmap = "tlab/themes/DarkLab/container/GuiTabBookProfileThin.png";
   justify = "Bottom";
   textOffset = "0 -1";
   opaque = "1";
   border = "1";
   fillColor = "2 2 2 94";
   borderThickness = "1";
   borderColor = "46 46 46 102";

};



singleton GuiControlProfile(ToolsTabBookMainDrag : ToolsTabBookMain)
{
   opaque = "1";
   fillColor = "22 22 22 150";
   fillColorHL = "229 229 236 255";
};

singleton GuiControlProfile(ToolsTabBookAlt : ToolsTabBookMain)
{
   bitmap = "tlab/themes/DarkLab/container/GuiTabBookAlt.png";
};

singleton GuiControlProfile(ToolsTabBookBasic : ToolsTabBookMain)
{
   bitmap = "tlab/themes/DarkLab/container/GuiTabBookBasic.png";
};
