//==============================================================================
// TorqueLab -> Default Containers Profiles
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================


//==============================================================================
// GuiRolloutCtrl Profiles
//==============================================================================
singleton GuiControlProfile(ToolsRolloutBaseA : ToolsDefaultProfile)
{  
   bitmap = "tlab/themes/DarkLab/container/GuiRolloutBaseA.png";  
   fontSize = "18";
   fontColors[0] = "254 254 254 255";
   fontColor = "254 254 254 255";
   textOffset = "6 0";
   border = "-5";
   autoSizeHeight = "1";
};
singleton GuiControlProfile(ToolsRolloutBaseB : ToolsRolloutBaseA)
{  
   bitmap = "tlab/themes/DarkLab/container/GuiRolloutBaseB.png";  
};

singleton GuiControlProfile(ToolsRolloutBaseA_S1 : ToolsRolloutBaseA)
{  
   bitmap = "tlab/themes/DarkLab/container/GuiRolloutBaseA_S1.png";  
   fontSize = "15";
};

singleton GuiControlProfile(ToolsRolloutBaseB_S1 : ToolsRolloutBaseB)
{  
   bitmap = "tlab/themes/DarkLab/container/GuiRolloutBaseB_S1.png";  
   fontSize = "15";
};
