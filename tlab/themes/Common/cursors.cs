//==============================================================================
// GameLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

new GuiCursor(EditorHandCursor)
{
	hotSpot = "7 0";
	bitmapName = "tlab/art/icons/default/cursor/Cur_hand.png";
};

new GuiCursor(EditorRotateCursor)
{
	hotSpot = "11 18";
	bitmapName = "tlab/art/icons/default/cursor/Cur_rotate.png";
};

new GuiCursor(EditorMoveCursor)
{
	hotSpot = "9 13";
	bitmapName = "tlab/art/icons/default/cursor/Cur_grab.png";
};

new GuiCursor(EditorArrowCursor)
{
	hotSpot = "0 0";
	bitmapName = "tlab/art/icons/default/cursor/Cur_3darrow.png";
};

new GuiCursor(EditorUpDownCursor)
{
	hotSpot = "5 10";
	bitmapName = "tlab/art/icons/default/cursor/Cur_3dupdown";
};
new GuiCursor(EditorLeftRightCursor)
{
	hotSpot = "9 5";
	bitmapName = "tlab/art/icons/default/cursor/Cur_3dleftright";
};

new GuiCursor(EditorDiagRightCursor)
{
	hotSpot = "8 8";
	bitmapName = "tlab/art/icons/default/cursor/Cur_3ddiagright";
};

new GuiCursor(EditorDiagLeftCursor)
{
	hotSpot = "8 8";
	bitmapName = "tlab/art/icons/default/cursor/Cur_3ddiagleft";
};

new GuiControl(EmptyControl)
{
	profile = "ToolsButtonBaseA";
};

new GuiCursor(LeftRightCursor)
{
	hotSpot = "0.5 0";
	renderOffset = "0.5 0";
	bitmapName = "./Images/leftRight";
};

new GuiCursor(UpDownCursor)
{
	hotSpot = "1 1";
	renderOffset = "0 1";
	bitmapName = "./Images/upDown";
};

new GuiCursor(NWSECursor)
{
	hotSpot = "1 1";
	renderOffset = "0.5 0.5";
	bitmapName = "./Images/NWSE";
};

new GuiCursor(NESWCursor)
{
	hotSpot = "1 1";
	renderOffset = "0.5 0.5";
	bitmapName = "./Images/NESW";
};

new GuiCursor(MoveCursor)
{
	hotSpot = "1 1";
	renderOffset = "0.5 0.5";
	bitmapName = "./Images/move";
};

new GuiCursor(TextEditCursor)
{
	hotSpot = "1 1";
	renderOffset = "0.5 0.5";
	bitmapName = "./Images/textEdit";
};
