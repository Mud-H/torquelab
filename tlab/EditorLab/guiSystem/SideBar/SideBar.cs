//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================


//------------------------------------------------------------------------------
// Called from onInitialEditorLaunch to set the initial sidebar states
function Lab::initSideBar(%this)
{

	$SideBarVIS_Initialized = 0;
	timerStepStart("InitSideBar","SideBar");
	timerStep("registerObjects","SideBar");
	Scene.registerObjects();


	timerStep("SideBarVIS","SideBar");
	SideBarVIS.init();
	SideBarVIS.onCtrlResized();


	timerStep("initFileBrowser","SideBar");
	Lab.initFileBrowser();

	timerStep("initObjectCreator","SideBar");
	Lab.initObjectCreator();


	FileBrowser.onCtrlResized();

	SideBarMainBook.selectPage($SideBarMainBook_CurrentPage);
	timerStep("bringToFront","SideBar");
	EditorGui-->SideBarContainer.bringToFront(LabSideBar);

	timerStep("Done","SideBar");

	execPattern("tlab/EditorLab/guiSystem/*.preset.cs");
	//timerStepDump("SideBar");

}
//==============================================================================
function Lab::setSidebarWidth(%this,%width)
{
	FW.setSideBarWidth(%width);



}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::toggleSidebar(%this)
{
	if (LabSideBar.visible)
		Lab.closeSidebar();
	else
		Lab.openSidebar();
}
//==============================================================================
function Lab::openSidebar(%this,%check)
{

	//Set Window default extents
	%window = EditorGui-->SideBarContainer;
	//if (%window.openExtent !$= "")
	// %window.setExtent(%window.openExtent);

	show(%window);
	show(LabSideBar);
	hide(LabSideBarExpander);
	FW.setSideBarToggleButton(%window);

}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::closeSidebar(%this)
{

	%window = EditorGui-->SideBarContainer;
	%window.openExtent = %window.extent;

	//%window.setExtent("18",%window.extent.y);
	hide(%window);
	show(LabSideBarExpander);
	FW.setSideBarToggleButton(%window);
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::setSideBarToggle(%this)
{
	%sideContainer =  EditorGui-->SideBarContainer;
	%button = %sideContainer->SideBarToggle;

	if (!isObject(%button))
	{
		%button = %this.getSideBarCloseButton();
		%sideContainer.add(%button);
	}

	%sideContainer.pushToBack(%button);
	%button.AlignCtrlToParent("right","4");
	%button.AlignCtrlToParent("top","4");
	%button.visible = 1;
}
//------------------------------------------------------------------------------
//==============================================================================
//Called from Toolbar and TerrainManager
function Lab::getSideBarCloseButton(%this)
{

	%button =  new GuiBitmapButtonCtrl()
	{
		bitmap = "tlab/art/icons/24-assets/arrow_triangle_left.png";
		bitmapMode = "Stretched";
		autoFitExtents = "0";
		useModifiers = "0";
		useStates = "1";
		groupNum = "-1";
		buttonType = "PushButton";
		useMouseEvents = "0";
		position = "188 0";
		extent = "11 15";
		minExtent = "8 2";
		horizSizing = "left";
		vertSizing = "bottom";
		profile = "GuiDefaultProfile";
		command = "Lab.closeSideBar();";
		tooltipProfile = "GuiToolTipProfile";
		internalName = "SideBarToggle";

	};
	return %button;
}
//------------------------------------------------------------------------------

//==============================================================================
//LabSideBarNotify Unused since LabBoxCtrl use
function LabSideBarNotify::onParentResized(%this)
{
	logd("LabSideBarNotify::onParentResized",FileBrowserArray.isVisible());


}
//------------------------------------------------------------------------------
