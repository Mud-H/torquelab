//==============================================================================
// TorqueLab -> WorldEditor Grid and Snapping System
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
$TLab_SideBar_Page["VIS"] = 0;
$TLab_SideBar_Page["ObjectCreator"] = 1;
$TLab_SideBar_Page["FileBrowser"] = 2;
//==============================================================================
// LabSideBar Functions
//==============================================================================
function LabSideBar::onWake(%this)
{

	//WIP Fast - Called from EditorFrameContent
	//Lab.schedule(100,"initSideBar");
	Lab.setSideBarToggle();
}
function LabSideBar::onVisible(%this)
{

	//WIP Fast - Called from EditorFrameContent
	//Lab.schedule(100,"initSideBar");
	devLog("OnWakeSide");
	Lab.setSideBarToggle();
}
function LabSideBar::onCtrlResized(%this)
{
	devLog("LabSideBar::onCtrlResized",%this);
	Lab.setSideBarToggle();
}
function TLabSideContainer::onCtrlResized(%this)
{
	devLog("TLabSideContainer::onCtrlResized",%this);
//Lab.setSideBarToggle();
}


function LabSideBar::onPreEditorSave(%this)
{

	FileBrowserArray.clear();
	ObjectCreatorArray.clear();
	return;
	SideBarVIS-->theVisOptionsList.clear();
	SideBarVIS-->theClassVisList.clear();
	SideBarVIS-->theClassVisArray.clear();
	SideBarVIS-->theClassSelList.clear();
	SideBarVIS-->theClassSelArray.clear();
}
function LabSideBar::onPostEditorSave(%this)
{

	//SideBarVIS.init();

	FileBrowser.initFiles();
	ObjectCreator.initFiles();

}
//==============================================================================
// SideBar Tab Book
//==============================================================================

//==============================================================================
//Set a plugin as active (Selected Editor Plugin)
function SideBarMainBook::onTabSelected(%this,%text,%id)
{
	logd("SideBarMainBook::onTabSelected",%text,%id);
	$SideBarMainBook_CurrentPage = %id;

	switch$(%id)
	{
		case "0":
			if (!$FileBrowserInit && isObject(FileBrowser))
				Lab.initFileBrowser();

		case "1":
			Lab.schedule(200,"initSceneBrowser");
	}
}
//------------------------------------------------------------------------------
function LabSideBar::setPage(%this,%page)
{

	%pageIndex = $TLab_SideBar_Page[%page];

	if (%pageIndex $= "")
		return;

	SideBarMainBook.selectPage(%pageIndex);

}
