//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================



//==============================================================================
function SB_ObjectCreatorBox::onResize(%this)
{
	logb("SB_ObjectCreatorBox::onResize",%this);

	if (isObject(ObjectCreatorArray))
	{
		%cols = $ObjectCreator_ViewId + 1;
		ObjectCreatorArray.extent.x = ObjectCreatorArray.getParent().extent.x - 20;
		%width = (ObjectCreatorArray.extent.x - 10) / %cols;
		ObjectCreatorArray.colSize = %width;
		ObjectCreatorArray.refresh();
	}
}
//------------------------------------------------------------------------------