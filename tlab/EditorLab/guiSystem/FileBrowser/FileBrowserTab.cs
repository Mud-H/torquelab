//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================


//==============================================================================
// Called from onInitialEditorLaunch to set the initial sidebar states
function SB_FileBrowserBox::onResize(%this)
{
	logb("SB_FileBrowserBox::onResize",%this);
	if (isObject(FileBrowserArray))
	{
      %cols = $FileBrowser_ViewId + 1;
      FileBrowserArray.extent.x = FileBrowserArray.getParent().extent.x - 20;
      %width = (FileBrowserArray.extent.x - 10) / %cols;
      FileBrowserArray.colSize = %width;
      FileBrowserArray.refresh();
	}
}
//------------------------------------------------------------------------------