//==============================================================================
// TorqueLab -> Editor Gui Open and Closing States
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
$TLabVisPresetFolder = "tlab/EditorLab/guiSystem/VIS/presets/";
//SideBarVIS.updatePresetMenu
function SideBarVIS::updatePresetMenu(%this,%noCallback)
{
	%searchFolder = $TLabVisPresetFolder@"*.layers.ucs";
	//Now go through each files again to add a brush with latest items
	
	%selected = 0;

   SideBarVIS.menuIds = strAddWord(SideBarVIS.menuIds,MenuBarVISMenu);
	foreach$(%menu in SideBarVIS.menuIds)
	{
		%pid = 0;
		%menu.clear();
		%menu.add("Select a preset",0);

		for(%presetFile = findFirstFile(%searchFolder); %presetFile !$= ""; %presetFile = findNextFile(%searchFolder))
		{
			%presetName = strreplace(fileBase(%presetFile),".layers","");
			%menu.add(%presetName,%pid++);

			if (SideBarVIS.currentPresetFile $= %presetName)
				%selected = %pid;
		}

		%menu.setSelected(%selected,!%noCallback);
	}
}
function SideBarVIS::toggleNewPresetCtrl(%this,%state)
{
	if (%state $= "")
		%state = !SideBarVIS_NewPreset.visible;

	if (%state)
	{
		hide(%this-->newPresetButton);
		SideBarVIS_NewPreset.visible = 1;
		return;
	}

	show(%this-->newPresetButton);
	SideBarVIS_NewPreset.visible = 0;
}

//SideBarVIS.exportPresetSample
function SideBarVIS::savePresetToFile(%this,%isNew)
{
	//if (%isNew)
	%name = SideBarVIS_NewPreset-->presetName.getText();
	//else
	//%name = strreplace(SideBarVIS.activePreset,"*","");

	%savedPreset = SideBarVIS.savePreset(%name);

	if (%savedPreset $= "")
		return;

	SideBarVIS.activePreset = %savedPreset;

}
function SideBarVIS::savePreset(%this,%name)
{

	//devLog("Saving Preset:",%name,"IsNew",	%isNew);
	if (strFind(%name,"["))
	{
		return;
	}

	%layerExampleObj = newScriptObject();
	%layerExampleObj.internalName = %name;

	for(%i = 0; %i < %this.classArray.count(); %i++)
	{
		%class = %this.classArray.getKey(%i);
		eval("%selectable = $" @ %class @ "::isSelectable;");
		eval("%renderable = $" @ %class @ "::isRenderable;");
		%layerExampleObj.selectable[%class] = %selectable;
		%layerExampleObj.visible[%class] = %renderable;
	}

	for(%i = 0; %i < %this.array.count(); %i++)
	{
		%text = "  " @ %this.array.getValue(%i);
		%val = %this.array.getKey(%i);
		%var = getWord(%val, 0);
		eval("%value = "@%var@";");
		%formatVar = strreplace(%var,"$","");
		%formatVar = strreplace(%formatVar,"::","__");
		%formatVar = strreplace(%formatVar,".","_dot_");
		%layerExampleObj.visOptions[%formatVar] = %value;
	}

	%layerExampleObj.save($TLabVisPresetFolder@%name@".layers.ucs",0,"%presetLayers = ");
	//%this.toggleNewPresetCtrl(false);
	SideBarVIS.loadPresetFile(%name);
	%this.updatePresetMenu(true);

	return %name;

}


//SideBarVIS.loadPresetFile("visBuilder");
function SideBarVIS::loadPresetFile(%this,%filename,%noStore)
{

	%activePreset = %this.loadPresetFromFile(%filename,%noStore);

	SideBarVIS.activePreset = %activePreset;
	SideBarVIS_NewPreset-->presetName.setText(%activePreset);
}

function MenuVisibilityPreset::onSelect(%this,%id,%text)
{
	if (%id $= "0")
		return;

	SideBarVIS.loadPresetFile(%text);
}


//==============================================================================
function MenuVisibilityPreset::onAdd(%this,%value)
{
	
	SideBarVIS.menuIds = strAddWord(SideBarVIS.menuIds,%this.getId(),1);
	//devLog("MenuVisibilityPreset::onAdd","List:",SideBarVIS.menuIds);
	$TLabVisMenuListed[%this.getId()] = true;
	
}

function MenuVisibilityPreset::onWake(%this,%value)
{
	//devLog("MenuVisibilityPreset::onWake");
	if (!$TLabVisMenuListed[%this.getId()])
	  SideBarVIS.menuIds = strAddWord(SideBarVIS.menuIds,%this.getId(),1);
}

$TLabVisCommonClasses = "TSStatic StaticShape Prefab Trigger TerrainBlock ScatterSky PointLight SpotLight Zone OcclusionVolume Portal MissionMarker WaterPlane";




$TLabVisGroups = "Objects Environment Mission Lights Zones Player AI FX Rare";
$TLabVisGroupClass["Objects"] = "FlyingVehicle WheeledVehicle StaticShape RigidShape Item HoverVehicle Debris TSStatic TurretShape SceneObject Vehicle AITurretShape ConvexShape ProximityMine Prefab ShapeBase PhysicsDebris PhysicsShape";
$TLabVisGroupClass["Environment"] = "Forest Lightning River Sun WaterPlane WaterBlock VolumetricFog GroundCover  ScatterSky SkyBox Precipitation VolumetricFogRTManager WaterObject fxFoliageReplicator AccumulationVolume BasicClouds CloudLayer GroundPlane ForestWindEmitter TerrainBlock TimeOfDay";
$TLabVisGroupClass["Lights"] = "SpotLight LightBase PointLight";
$TLabVisGroupClass["Zones"] = "Zone PhysicalZone PhysicsForce OcclusionVolume Trigger Portal VehicleBlocker";
$TLabVisGroupClass["Player"] = "Player SpawnSphere Projectile Camera CameraBookmark ";
$TLabVisGroupClass["AI"] = "PathCamera CoverPoint NavMesh AIPlayer NavPath";
$TLabVisGroupClass["FX"] = "Splash Explosion ParticleEmitter Ribbon RibbonNode SFXEmitter SFXSpace";
$TLabVisGroupClass["Rare"] = "GameBase ScopeAlwaysShape RenderMeshExample	RenderObjectExample RenderShapeExample fxShapeReplicatedStatic	fxShapeReplicator";
$TLabVisGroupClass["Mission"] = "Marker WayPoint MissionMarker DecalRoad MeshRoad DecalManager";
//EVisibilityLayers.loadPresetFile("visBuilder");
function SideBarVIS::loadPresetFromFile(%this,%filename,%noStore)
{

	if (!isObject(SideBarVIS.classArray))
		SideBarVIS.initClassArray();

	%file = $TLabVisPresetFolder@%filename@".layers.ucs";

	if (!isFile(%file))
		return;

	exec(%file);

	for(%i = 0; %i < %this.classArray.count(); %i++)
	{
		%class = %this.classArray.getKey(%i);
		%selectable = %presetLayers.selectable[%class];
		%renderable = %presetLayers.visible[%class];

		if (%selectable !$= "")
		{
			eval("$" @ %class @ "::isSelectable = \""@%selectable@"\";");
			//info("Class:",%class,"isSelectable set to:",%selectable);
		}

		if (%renderable !$= "")
		{
			eval("$" @ %class @ "::isRenderable = \""@%renderable@"\";");
			//info("Class:",%class,"isRenderable set to:",%renderable);
		}
	}

	for(%i = 0; %i < %this.array.count(); %i++)
	{
		%text = "  " @ %this.array.getValue(%i);
		%val = %this.array.getKey(%i);
		%var = getWord(%val, 0);
		%formatVar = strreplace(%var,"$","");
		%formatVar = strreplace(%formatVar,"::","__");
		%formatVar = strreplace(%formatVar,".","_dot_");
		%value = %presetLayers.visOptions[%formatVar];

		if (%value $= "")
			continue;

		eval(%var@" = %value;");
		//info("Variable:",%var," set to:",%value);
	}

   Lab.showLights(%presetLayers.visiblePointLight);
	%presetName = %filename;

	if (!%noStore)
	{
		%this.currentPresetFile = %filename;
	}
	else
	{
		%presetName = "*"@%presetName;
	}

	LabSideDualVisList.refresh();
	%this.activePreset = %presetName;
	return %presetName;
}
