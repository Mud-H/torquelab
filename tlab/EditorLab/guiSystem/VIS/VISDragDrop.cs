//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================
// Toolbar drag controls callbacks
//==============================================================================

//==============================================================================
// Start dragging a toolbar icon
function VisPill::onMouseDragged(%this)
{
	Lab.startVISDrag(%this);
}
//------------------------------------------------------------------------------


//==============================================================================
// Start dragging a toolbar icons group
function Lab::startVISDrag(%this,%ctrl)
{
	if (!isObject(%this))
	{
		devLog("startVISDrag class is corrupted! Fix it!");
		return;
	}

	%callback = "Lab.onVISPillDropped";
	startDragAndDropCtrl(%ctrl.parentGroup,"Toolbar",%callback);
	hide(%ctrl.parentGroup);
}
//------------------------------------------------------------------------------

//==============================================================================
// Dragged control dropped over undefined control (gonna check if it's droppable)
function Lab::onVISPillDropped(%this,%dropOnCtrl, %draggedControl, %dropPoint)
{
	%droppedCtrl = %draggedControl.dragSourceControl;
	show(%droppedCtrl);

	if (%dropOnCtrl.superClass !$= "VisPill")
	{
		warnLog("VIS dropped on non VIS pill:",%dropOnCtrl.getClassName(),%dropOnCtrl);
		return;
	}

	info("Dropped on",%droppedCtrl,"Pill",%dropOnCtrl.parentGroup,"Dropped",%droppedCtrl);
	LabSideDualVisList.reorderChild(%droppedCtrl,%dropOnCtrl.parentGroup);
	SideBarVis.schedule(100,exportOrder);
}
//------------------------------------------------------------------------------

//==============================================================================
function SideBarVIS::exportOrder(%this)
{


	foreach$(%class in $TLabVisCommonClasses)
	{
		if (%done[%class])
			continue;

		$SideBarVIS_OrderClass[%class] = %orderID++;
		%done[%class] = true;
	}

	%orderID = 100;

	foreach(%ctrl in LabSideDualVisList)
	{
		%class = trim(%ctrl-->class.text);

		if (%done[%class])
			continue;

		$SideBarVIS_OrderClass[%class] = %orderID++;
	}

	export("$SideBarVIS_OrderClass*","tlab/EditorLab/guiSystem/VIS/classOrder.preset.cs");
}
//------------------------------------------------------------------------------

//==============================================================================
function SideBarVIS_SortMenu::onSelect(%this,%id,%text)
{

	if (%id $= "1")
		%mode = "Name";

	SideBarVIS.sortClass(%mode);

	$Cfg_VisOrderMode = %id;
}
//------------------------------------------------------------------------------

//==============================================================================
function SideBarVIS::sortClass(%this,%mode)
{

	if (%mode $= "Name")
		%func = "sideVisSortNameAsc";
	else
		%func = "sideVisOrder";

	LabSideDualVisList.sort(%func);
	LabSideDualVisList.updateStack();
}
//------------------------------------------------------------------------------

//==============================================================================
function sideVisOrder(%a,%b)
{

	%classA = trim(%a-->class.text);
	%classB = trim(%b-->class.text);
	%orderA = $SideBarVIS_OrderClass[%classA];
	%orderB = $SideBarVIS_OrderClass[%classB];

	if (%orderA > %orderB)
		return 1;

	if (%orderB > %orderA)
		return -1;

	return 0;
}
//------------------------------------------------------------------------------
//==============================================================================
function sideVisSortNameAsc(%a,%b)
{

	%classA = trim(%a-->class.text);
	%classB = trim(%b-->class.text);
	%result = strcmp(%classA,%classB);
	return %result;
}
//------------------------------------------------------------------------------

//==============================================================================
function refreshVis()
{

	LabSideDualVisList.refresh();
}
//------------------------------------------------------------------------------
//==============================================================================
function LabSideDualVisList::refresh(%this)
{

	foreach(%ctrl in %this)
	{
		%class = trim(%ctrl-->class.text);
		eval("%render = $" @ %class @ "::isRenderable;");
		eval("%select = $" @ %class @ "::isSelectable;");
		%ctrl-->visible.setStateOn(%render);
		%ctrl-->select.setStateOn(%select);

	}
}
//------------------------------------------------------------------------------
