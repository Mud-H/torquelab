//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function Lab::showLights(%this,%visible,%group)
{
	if (%visible $= "")
		%visible = true;
		
   ObjSet_PointLight.callOnChildren("setHidden",!%visible);
   return;
   %lights = getMissionObjectClassList("PointLight");
   foreach$(%id in %lights)
      %id.hidden = !%visible;
   
   return;

	EVisibilityLayers.setLightsVisible(%visible,%group);
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::shrinkLights(%this,%shrinked,%group)
{
	if (%shrinked $= "")
		%shrinked = true;
   %lights = getMissionObjectClassList("PointLight");
   %shrinkRadius = "0.1";
   if (%shrinked)
   {    
      foreach$(%id in %lights)
      {  
          if (%id.radius !$= %shrinkRadius)
            $LabShrinkedLightDefault[%id] = %id.radius;
            
         //%id.doAddInspect(%id);
         //%id.setFieldValue("radius",%shrinkRadius);
      }
      LabObj.updateList(%lights,"radius",%shrinkRadius);
      return;
   }
   foreach$(%id in %lights)
      {  
          if ($LabShrinkedLightDefault[%id] !$= "")
            LabObj.set(%id,"radius",$LabShrinkedLightDefault[%id]);
             //%id.setFieldValue("radius",$LabShrinkedLightDefault[%id]);            
       
         $LabShrinkedLightDefault[%id] = "";
      }
  
   
 
}
//------------------------------------------------------------------------------


