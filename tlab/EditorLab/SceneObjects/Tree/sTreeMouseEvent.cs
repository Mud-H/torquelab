//==============================================================================
// TorqueLab -> Scene Tree Mouse Events Callbacks
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// Tree Items Drag and Dropping
//==============================================================================
//==============================================================================
// Called when an item as been dropped
function SceneObjectsTree::onDragDropped(%this)
{
	Scene.setDirty();
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneObjectsTree::isValidDragTarget(%this, %id, %obj)
{
	if (%obj.isMemberOfClass("Path"))
		return EWorldEditor.areAllSelectedObjectsOfType("Marker");

	if (%obj.name $= "CameraBookmarks")
		return EWorldEditor.areAllSelectedObjectsOfType("CameraBookmark");
	else
		return (%obj.isMemberOfClass("SimSet")); // $= "SimGroup" );
}
//------------------------------------------------------------------------------

//==============================================================================
// Set object selected in scene (Trees and WorldEditor)
//==============================================================================
function SceneEditorTree::onMouseUp(%this,%hitItemId, %mouseClickCount)
{
	logd("SceneEditorTree::onMouseUp( %this,%hitItemId, %mouseClickCount )",%hitItemId, %mouseClickCount);


	if (%mouseClickCount > 1)
	{
		%obj = %this.getItemValue(%hitItemId);

		if (%obj.getClassName() $= "SimGroup")
			Scene.setActiveSimGroup(%obj);
		else if (isObject(%obj.parentGroup))
			Scene.setActiveSimGroup(%obj.parentGroup);
	}

}

//==============================================================================
function SceneObjectsTree::onMouseDown(%this,%hitItemId, %mouseClickCount)
{
	//devLog("SceneObjectsTree::onMonMouseDownouseUp( %this,%hitItemId, %mouseClickCount )",%hitItemId, %mouseClickCount);
	//Nothing as now (expand moved to onAddSelection)
}
function SceneObjectsTree::onRightMouseUp(%this, %itemId, %mouse, %obj)
{
	%this.showContextMenu(%itemId, %mouse, %obj);
}
function SceneObjectsTree::onMiddleMouseDown(%this,%hitItemId, %mouseClickCount)
{
	devlog("SceneObjectsTree::onMiddleMouseDown( %this,%hitItemId, %mouseClickCount )",%hitItemId, %mouseClickCount);
	%obj = %this.getItemValue(%hitItemId);

	if (%obj.getClassName() $= "SimGroup")
		Scene.setActiveSimGroup(%obj);
	else if (isObject(%obj.parentGroup))
		Scene.setActiveSimGroup(%obj.parentGroup);

}

//==============================================================================
// Set object selected in scene (Trees and WorldEditor)
//==============================================================================
function SceneObjectsTree::showContextMenu(%this, %itemId, %mouse, %obj)
{
	logd("SceneObjectsTree::showContextMenu( %this, %itemId, %mouse, %obj )",%this, %itemId, %mouse, %obj);

	if (!isObject(%obj))
		return;

	%haveObjectEntries = false;
	%haveLockAndHideEntries = true;
	delObj(SceneTreePopup);
	%popup = new PopupMenu(SceneTreePopup)
	{
		superClass = "MenuBuilder";
		isPopup = "1";
	};
	%popup.object = %obj;
	%id = -1;

	if (%this.internalName $= "LayerTree")
	{
		%popup.addItem(%id++,"Remove from layer" TAB "" TAB "Lab.removeObjectFromLayer("@%obj@");");
	}

	// Open context menu if this is a CameraBookmark
	if (%obj.isMemberOfClass("CameraBookmark"))
	{
		%popup.addItem(%id++,"Go To Bookmark" TAB "" TAB "EManageBookmarks.jumpToBookmark( %this.bookmark.getInternalName() );");
		SceneTreePopup.bookmark = %obj;
	}
	// Open context menu if this is set CameraBookmarks group.
	else if (%obj.name $= "CameraBookmarks")
	{
		%popup.addItem(%id++,"Add Camera Bookmark" TAB "" TAB "EManageBookmarks.addCameraBookmarkByGui();");
	}
	// Open context menu if this is a SimGroup
	else if (%obj.isMemberOfClass("SimGroup"))
	{
		%popup.addItem(%id++,"Rename" TAB "" TAB "SceneEditorTree.showItemRenameCtrl( SceneEditorTree.findItemByObjectId( %this.object ) );");
		%popup.addItem(%id++,"Delete" TAB "" TAB "EWorldEditor.deleteMissionObject( %this.object );");
		%popup.addItem(%id++,"Inspect" TAB "" TAB "inspectObject( %this.object );");
		%popup.addItem(%id++,"-");
		//%popup.addItem(%id++,"Toggle Lock Children" TAB "" TAB "EWorldEditor.toggleLockChildren( %this.object );");
		//%popup.addItem(%id++,"Toggle Hide Children" TAB "" TAB "EWorldEditor.toggleHideChildren( %this.object );");
		//%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Show Childrens" TAB "" TAB "Scene.showGroupChilds( "@%obj@" );");
		%popup.addItem(%id++,"Hide Childrens" TAB "" TAB "Scene.hideGroupChilds( "@%obj@" );");
		%popup.addItem(%id++,"Toggle Children Visibility" TAB "" TAB "Scene.toggleHideGroupChilds( "@%obj@" );");
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Group" TAB "" TAB "Scene.addSimGroup( true );");
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Add New Objects Here" TAB "" TAB "Scene.setNewObjectGroup( %this.object );");
		%popup.addItem(%id++,"Add Children to Selection" TAB "" TAB "EWorldEditor.selectAllObjectsInSet( "@%obj@" , false );");
		%popup.addItem(%id++,"Remove Children from Selection" TAB "" TAB "EWorldEditor.selectAllObjectsInSet( "@%obj@" , true );");
		%popup.addItem(%id++,"Lock Childrens" TAB "" TAB "Scene.lockGroupObjects( "@%obj@" , true );");
		%popup.addItem(%id++,"Lock Childrens" TAB "" TAB "Scene.lockGroupObjects( "@%obj@" , false );");
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Lock auto-arrange" TAB "" TAB "SceneEd.toggleAutoArrangeGroupLock("@%obj@");");
		%popup.autoLockId = %id;


  
		%popup.addItem(%id++,"Check for Light Shapes" TAB "" TAB "checkLightShapes("@%obj@");");

		if (%obj.prefabFile !$= "")
		{
			%popup.addItem(%id++,"Collapse Prefab group" TAB "" TAB "Lab.CollapsePrefab( "@%obj.getId()@");");
		}

		if (%obj.autoArrangeLocked)
			%popup.setItem(%popup.autoLockId,"Unlock auto-arrange","","SceneEd.toggleAutoArrangeGroupLock( %this.object);");
		else
			%popup.setItem(%popup.autoLockId,"Lock auto-arrange","","SceneEd.toggleAutoArrangeGroupLock( %this.object);");

		%hasChildren = %obj.getCount() > 0;
		%popup.enableItem(10, %hasChildren);
		%popup.enableItem(11, %hasChildren);
		%haveObjectEntries = true;
		%haveLockAndHideEntries = false;
	}
	// Open generic context menu.
	else
	{
		%popup.addItem(%id++,"Rename" TAB "" TAB "SceneEditorTree.showItemRenameCtrl( SceneEditorTree.findItemByObjectId( %this.object ) );");
		%popup.addItem(%id++,"Delete" TAB "" TAB "EWorldEditor.deleteMissionObject( %this.object );");
		%popup.addItem(%id++,"Inspect" TAB "" TAB "inspectObject( %this.object );");
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Locked" TAB "" TAB "%this.object.setLocked( !%this.object.locked ); EWorldEditor.syncGui();");
		%popup.addItem(%id++,"Hidden" TAB "" TAB "EWorldEditor.hideObject( %this.object, !%this.object.hidden ); EWorldEditor.syncGui();");
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Group" TAB "" TAB "Scene.addSimGroup( true );");

		if (%obj.isMemberOfClass("Prefab"))
		{
			%popup.addItem(%id++, "Expand Prefab to group" TAB "" TAB "devlog(\"Expanding\");Lab.ExpandPrefab( "@%obj.getId()@");");
		}

		%haveObjectEntries = true;
	}

	// Specialized version for ConvexShapes.
	if (%obj.isMemberOfClass("ConvexShape"))
	{
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Convert to Zone" TAB "" TAB "EWorldEditor.convertSelectionToPolyhedralObjects( \"Zone\" );");
		%popup.addItem(%id++,"Convert to Portal" TAB "" TAB "EWorldEditor.convertSelectionToPolyhedralObjects( \"Portal\" );");
		%popup.addItem(%id++,"Convert to Occluder" TAB "" TAB "EWorldEditor.convertSelectionToPolyhedralObjects( \"OcclusionVolume\" );");
		%popup.addItem(%id++,"Convert to Sound Space" TAB "" TAB "EWorldEditor.convertSelectionToPolyhedralObjects( \"SFXSpace\" );");

	}

	// Specialized version for polyhedral objects.
	else if (%obj.isMemberOfClass("Zone") || %obj.isMemberOfClass("Portal") || %obj.isMemberOfClass("OcclusionVolume") || %obj.isMemberOfClass("SFXSpace"))
	{
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Convert to ConvexShape" TAB "" TAB "EWorldEditor.convertSelectionToConvexShape();");
	}


// Handle multi-selection.
	if (%this.getSelectedItemsCount() > 1)
	{
	   %popup.addItem(%id++,"-");
	   %popup.addItem(%id++,"Selection Options");
	   %popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Delete Selection" TAB "" TAB "EditorMenuEditDelete();");
		%popup.addItem(%id++,"Group Selection" TAB "" TAB "Scene.addSimGroup( true );");
		
		 %popup.addItem(%id++,"Generate Light from Selection" TAB "" TAB "SceneEd.genAutoLightSelection();");
	}

	if (%haveObjectEntries)
	{
		%popup.enableItem(0, %obj.isNameChangeAllowed() && %obj.getName() !$= "MissionGroup");
		%popup.enableItem(1, %obj.getName() !$= "MissionGroup");

		if (%haveLockAndHideEntries)
		{
			%popup.checkItem(4, %obj.locked);
			%popup.checkItem(5, %obj.hidden);
		}

		%popup.enableItem(7, %this.isItemSelected(%itemId));
	}
%popup.addItem(%id++,"Cancel and close menu" TAB "" TAB "info(\"Action cancelled\");");
	%popup.showPopup(Canvas);
}
