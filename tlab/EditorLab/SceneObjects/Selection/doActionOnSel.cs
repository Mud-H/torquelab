//==============================================================================
// TorqueLab -> Copy-Paste-Cut-Delete-Deselect
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================

//==============================================================================
function Lab::DeleteSel(%this)
{
	devLog(" Lab::DeleteSel() ");

	//Call the global scene object deletion function
	Scene.deleteSelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::DeselectSel(%this)
{
	devLog(" Lab::DeselectSel() ","Count",EWorldEditor.getSelectionSize());

	if (EWorldEditor.getSelectionSize() <= 0)
		return;

	EWorldEditor.clearSelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::CutSel(%this)
{
	devLog(" Lab::CutSel() ","Count",EWorldEditor.getSelectionSize());

	if (EWorldEditor.getSelectionSize() <= 0)
		return;

	EWorldEditor.cutSelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::CopySel(%this)
{
	devLog(" Lab::DeCopySelleteSel() ","Count",EWorldEditor.getSelectionSize());

	if (EWorldEditor.getSelectionSize() <= 0)
		return;

	EWorldEditor.copySelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::PasteSel(%this)
{
	devLog(" Lab::PasteSel() ","Count",EWorldEditor.getSelectionSize());

	if (EWorldEditor.getSelectionSize() <= 0)
		return;

	EWorldEditor.pasteSelection();
}
//------------------------------------------------------------------------------

function Lab::ToggleVisibleSel(%this)
{
	devLog(" Lab::DeleteSel() ");

	//Call the global scene object deletion function
	Scene.ToggleVisibleSelection();
}
//------------------------------------------------------------------------------
function Scene::ToggleVisibleSelection(%this)
{

	//By deleting the object itself, the SceneTree remove it instantly.
	for(%i=EWorldEditor.getSelectionSize()-1; %i>=0; %i--)
	{
		%obj = EWorldEditor.getSelectedObject(%i);
		EWorldEditor.hideObject(%obj,!%obj.hidden);
	}

}

EWorldEditor.lockSelection(true); 
SceneEditorTree.toggleLock();

function Lab::ToggleLockSel(%this)
{
	devLog(" Lab::ToggleLockSel() ");

	//Call the global scene object deletion function
	Scene.ToggleLockSelection();
}
//------------------------------------------------------------------------------
function Scene::ToggleLockSelection(%this)
{

	//By deleting the object itself, the SceneTree remove it instantly.
	for(%i=EWorldEditor.getSelectionSize()-1; %i>=0; %i--)
	{
		%obj = EWorldEditor.getSelectedObject(%i);
		%obj.locked = !%obj.locked;
	}

}
