//==============================================================================
// TorqueLab -> Prefab Creator
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
// Allow to manage different GUI Styles without conflict
//==============================================================================


function Scene::onPrefabSelected(%this,%prefab)
{
	warnLog("Scene::onPrefabSelected:",%prefab);
	%name = fileBase(%prefab.filename);
	%path = filePath(%prefab.filename);
	LabPrefabSelectedName.setText(%name);
	LabPrefabSelectedFolder.setText(%path);
	Scene.selectedPrefab = %prefab;
	SEP_PrefabUnpackIcon.visible = 1;
	//SceneEd_PrefabInfoCtrl.visible = 1;
}

function Scene::noPrefabSelected(%this)
{
	warnLog("Scene::noPrefabSelected:");

}

function LabPrefabSelectedName::onValidate(%this)
{
	warnLog("Current prefab name changed to:",%this.getText());
}


function Scene::unpackSelectedPrefab(%this)
{
	warnLog("Current prefab name changed to:",%this.getText());

	if (!isObject(Scene.selectedPrefab))
	{
		Scene.noPrefabSelected();
		return;
	}

	%success = Lab.ExpandPrefab(Scene.selectedPrefab);

	if (%success)
		Scene.noPrefabSelected();
}
function SceneEd::unpackSelectedPrefab(%this)
{
	warnLog("OLD SceneEd unpackSelectedPrefab redirected to new Scene method");
	Scene.unpackSelectedPrefab();
}
