//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
// Allow to manage different GUI Styles without conflict
//==============================================================================

function SceneEd::setActiveLayer(%this,%simSet)
{

	if (!isObject(%simSet))
		return;

	%oldGroup = SceneEd.activeLayer;
	SceneEd.activeLayer = %simSet;

	%oldItem = SceneLayerTree.findItemByObjectId(%oldGroup.getId());

	if (%oldItem !$= "-1")
		SceneLayerTree.markItem(%oldItem,false);

	%newItem = SceneLayerTree.findItemByObjectId(%simSet.getId());

	if (%newItem !$= "-1")
		SceneLayerTree.markItem(%newItem,true);
}


function SceneEd::addNewLayer(%this,%addSelection)
{
	devLog("SceneEd::addNewLayer");
	%set = Lab.addLayerSet("NewLayer");
	SceneEd.setActiveLayer(%set);
	if (%addSelection)
	   	SceneEd.addSelectionToLayer(%set);
	//SceneEd.schedule(10,setActiveLayer,%set);
	//if (%addSelection)
	  //SceneEd.schedule(20,addSelectionToLayer,%set);
}

function SceneEd::addSelectionToLayer(%this,%layerSet)
{
   
   if (!isObject(%layerSet))
		%layerSet = SceneEd.activeLayer;
	if (!isObject(%layerSet))
		return;

	%layerSet.addSelectedObjects();

}

function SceneEd::selectObjectsInSet(%this, %simGroup,%noRecurse)
{
	%isLast = false;
	%mid = SceneLayerTree.findItemByObjectId(%simGroup);
	SceneLayerTree.removeSelection(%mid);

	foreach(%child in %simGroup)
	{
		if (%child.getClassName() $= "SimSet" && !%noRecurse)
		{
			%this.selectObjectsInSet(%child);
			continue;
		}

		//Scene.onAddSelection(%child,%isLast,SceneGroup);
		//SceneLayerTree.setSelectedItem( %child,false,true);
		%mid = SceneLayerTree.findItemByObjectId(%child);
		SceneLayerTree.addSelection(%mid,false);
	}
}
//------------------------------------------------------------------------------
function SceneEd::setLayerClassFilter(%this, %classes, %hidden)
{

	foreach$(%class in %classes)
	{
		if (%hidden)
		{
			%this.layerDisabledClasses = strAddWord(%this.layerDisabledClasses,%class,1);
		}
		else
		{
			%this.layerDisabledClasses = strRemoveWord(%this.layerDisabledClasses,%class);
		}
	}

	Lab.filterLayerClass(%this.layerDisabledClasses);
}
//------------------------------------------------------------------------------

$LabSceneClassType["Zone"] = "Zone Trigger";
$LabSceneClassType["Shape"] = "TSStatic StaticShape";
$LabSceneClassType["Entity"] = "ScatteredSky SkyBox LevelInfo";
//------------------------------------------------------------------------------
function SceneLayerClassToggle::onMouseDown(%this, %classes, %hidden)
{
	%type = %this.internalName;
	%this.variable = "$SceneLayerClass"@%type;
	$SceneLayerClass[%type] = !$SceneLayerClass[%type];

	SceneEd.setLayerClassFilter($LabSceneClassType[%type],$SceneLayerClass[%type]);
}
//------------------------------------------------------------------------------
