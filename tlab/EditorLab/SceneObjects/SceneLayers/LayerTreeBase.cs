//==============================================================================
// TorqueLab -> Editor Gui General Scripts
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================
function LayerSetTree::rebuild(%this)
{
	%this.clear();
	%this.open(ObjSetGroup_Layers);
	%this.buildVisibleTree();
}
//------------------------------------------------------------------------------

//==============================================================================
function LayerSetTree::onDefineIcons(%this)
{
	%icons = "tlab/art/icons/iconTables/TreeViewBase/default:" @
	         "tlab/art/icons/iconTables/TreeViewBase/default:" @
	         "tlab/art/icons/iconTables/TreeViewBase/default:" @
	         "tlab/art/icons/iconTables/TreeViewBase/activegroup_close:" @//Marked Folder
	         "tlab/art/icons/iconTables/TreeViewBase/activegroup_open:" @//Marked Expanded Folder
	         "tlab/art/icons/iconTables/TreeViewBase/hidden:" @ //Hidden
	         "tlab/art/icons/iconTables/TreeViewBase/shll_icon_passworded_hi:" @ //Locked
	         "tlab/art/icons/iconTables/TreeViewBase/shll_icon_passworded:" @
	         "tlab/art/icons/iconTables/TreeViewBase/default:" @
	         "tlab/art/icons/iconTables/TreeViewBase/simgroup:" @ //Default Folder
	         "tlab/art/icons/iconTables/TreeViewBase/default";
	%this.buildIconTable(%icons);
}
//------------------------------------------------------------------------------

//==============================================================================
function LayerSetTree::onAdd(%this)
{
	Scene.SceneTrees = strAddWord(Scene.SceneTrees,%this.getId(),1);
}
//------------------------------------------------------------------------------
//==============================================================================
function LayerSetTree::onRemove(%this)
{
	Scene.SceneTrees = strRemoveWord(Scene.SceneTrees,%this.getId());
}
//------------------------------------------------------------------------------
//==============================================================================
/// @name EditorPlugin Methods
/// @{
function LayerSetTree::handleRenameObject(%this, %name, %obj)
{
	logd(" LayerSetTree::handleRenameObject(",%name, %obj);

	if (!isObject(%obj))
		return;

	%field = (%this.renameInternal) ? "internalName" : "name";
	%isDirty = LabObj.set(%obj,%field,%name);
	//info("Group:",%obj,"Is Dirty",%isDirty);
}

//------------------------------------------------------------------------------
//==============================================================================
function LayerSetTree::onAddGroupSelected(%this, %simGroup)
{
	logd("LayerSetTree::onAddGroupSelected %simGroup",%simGroup);

	Scene.setNewObjectGroup(%group);
}
//------------------------------------------------------------------------------
//==============================================================================
function LayerSetTree::onAddMultipleSelectionBegin(%this)
{
	logd("LayerSetTree::onAddMultipleSelectionBegin");
}
//------------------------------------------------------------------------------
//==============================================================================
function LayerSetTree::onAddMultipleSelectionEnd(%this)
{
	logd("LayerSetTree::onAddMultipleSelectionEnd");
}
//------------------------------------------------------------------------------

//==============================================================================
// Set object selected in scene (Trees and WorldEditor)
//==============================================================================

//==============================================================================
function LayerSetTree::onBeginReparenting(%this)
{
	if (isObject(%this.reparentUndoAction))
		%this.reparentUndoAction.delete();

	%action = UndoActionReparentObjects::create(%this);
	%this.reparentUndoAction = %action;
}
//------------------------------------------------------------------------------

//==============================================================================
function LayerSetTree::onReparent(%this, %obj, %oldParent, %newParent)
{
	%this.reparentUndoAction.add(%obj, %oldParent, %newParent);
}
//------------------------------------------------------------------------------

//==============================================================================
function LayerSetTree::onEndReparenting(%this)
{
	%action = %this.reparentUndoAction;
	%this.reparentUndoAction = "";

	if (%action.numObjects > 0)
	{
		if (%action.numObjects == 1)
			%action.actionName = "Reparent Object";
		else
			%action.actionName = "Reparent Objects";

		%action.addToManager(Editor.getUndoManager());
		EWorldEditor.syncGui();
	}
	else
		%action.delete();
}
//------------------------------------------------------------------------------
