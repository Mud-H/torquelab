//==============================================================================
// TorqueLab -> Scene Tree Selection Callbacks
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// SceneTree Select/Unselect related callbacks
//==============================================================================


//==============================================================================
function LayerSetTree::onSelect(%this, %obj)
{
	//This do nothing, the onAddSelection function is used
	//if (%obj.getClassName() $= "SimGroup")
	//  Scene.setActiveSimGroup(%obj);
	%item = SceneLayerTree.findItemByObjectId(%obj.getId());
	%parent = SceneLayerTree.getParentItem(%item);
	%parentObj = SceneLayerTree.getItemValue(%parent);
	if (%parentObj.groupLocked)
		   {
		   EWorldEditor.clearSelection();
		   EWorldEditor.selectAllObjectsInSet(%parentObj,false);
		   return;
		   }
}
//------------------------------------------------------------------------------
//==============================================================================
function LayerSetTree::onUnselect(%this, %obj)
{
	logd("LayerSetTree::onUnSelect",%obj);

	//Scene.onUnSelect(%obj);
	//Scene.unselectObject(%obj,%this);
	//Scene.onRemoveSelection(%obj, %this);
}
//------------------------------------------------------------------------------
//==============================================================================

function LayerSetTree::onAddSelection(%this, %obj, %isLastSelection)
{
	logd("LayerSetTree::onAddSelection IsLast",%isLastSelection);
	//%item = SceneEditorTree.getSelectedItem(0);
	%item	= %this.findItemByObjectId(%obj);

	switch$(%obj.getClassName())
	{
		case "SimSet":
		   if (%obj.groupLocked)
		   {
		   EWorldEditor.clearSelection();
		   EWorldEditor.selectAllObjectsInSet(%obj,false);
		   return;
		   }
			//Special Expand script to overlap auto expand behavior
			//Get the simgroup expanded state
			%expand = %obj.isExpanded();
			logd("Expanded",%expand,%item);

			//If already selected, toggle expanded state
			//if (isObject(%this.activeLayerGroup))
			// if ( %this.activeLayerGroup.getId() $= %obj.getId())
			// %expand = !%expand;

			//expand the item based on SimGroup expanded state
			%this.expandItem(%item,true);
			%obj.setIsExpanded(true);

		//Set the group as active
		//%this.activeLayerGroup = %obj;

		default:
			//if (isObject(%obj.parentGroup))
			// Scene.setActiveSimGroup(%obj.parentGroup);
	}

	//if ($SelectTreeOnly)
	if ($SelectTreeOnly)
		return;

	Scene.onAddSelection(%obj, %isLastSelection,%this);
}
//------------------------------------------------------------------------------
//==============================================================================
// Called when an item with no child is selected
function LayerSetTree::onInspect(%this, %obj)
{

	//if (isObject(%this.myInspector)){
	//devLog("Updating tree owned inspector",%this.myInspector);
	//%this.myInspector.inspect(%obj);
	//}

}
//------------------------------------------------------------------------------
//==============================================================================
// SceneTree Selection related callbacks
//==============================================================================


//==============================================================================
// Called after the current tree selection was cleared
function LayerSetTree::onClearSelection(%this)
{
	logd("SceneBrowserTree::onClearSelection");
	//Scene.doInspect("");
}
//------------------------------------------------------------------------------
//==============================================================================
// Called after a single object was removed from tree selection
function LayerSetTree::onRemoveSelection(%this, %obj)
{
	logd("LayerSetTree::onRemoveSelection",%obj);

	//This is important to unselect object in WorldEditor
	Scene.onRemoveSelection(%obj,%this);

}
//------------------------------------------------------------------------------

//==============================================================================
// SceneTree Deletion related callbacks
//==============================================================================
//==============================================================================
// Called just before selection deletion process start
function LayerSetTree::onDeleteSelection(%this)
{
	%this.undoDeleteList = "";
}
//------------------------------------------------------------------------------
//==============================================================================
// Overide the delete object when delete press
function LayerSetTree::onDeleteObject(%this, %object)
{
	if (%object.layerID $= "-1")
	{
		logd("Can't delete default layer");
		return;
	}

	%object.parentGroup.removeObject(%object);
}
//------------------------------------------------------------------------------

//==============================================================================
// Called after a tree object have beenn deleted
function LayerSetTree::onObjectDeleteCompleted(%this)
{
	// This can be called when a deletion is attempted but nothing was
	// actually deleted ( cannot delete the root of the tree ) so only submit
	// the undo if we really deleted something.
	if (%this.undoDeleteList !$= "")
		MEDeleteUndoAction::submit(%this.undoDeleteList);

	Scene.onObjectDeleteCompleted();
}
//------------------------------------------------------------------------------
//==============================================================================
// SceneTree Object UnSelect Functions
//==============================================================================



//==============================================================================
// SceneTree Object Deletion Functions
//==============================================================================

