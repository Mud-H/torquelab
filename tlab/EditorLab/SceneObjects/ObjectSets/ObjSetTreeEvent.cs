//==============================================================================
// TorqueLab -> Scene Tree Mouse Events Callbacks
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================


function ObjectSetTree::onMiddleMouseDown(%this,%hitItemId, %mouseClickCount)
{
	devlog("ObjectSetTree::onMiddleMouseDown( %this,%hitItemId, %mouseClickCount )",%hitItemId, %mouseClickCount);



}
function ObjectSetTree::onMouseUp(%this,%hitItemId, %mouseClickCount)
{
	devlog("ObjectSetTree::onMouseUp( %this,%hitItemId, %mouseClickCount )",%hitItemId, %mouseClickCount);


}

//==============================================================================
function ObjectSetTree::onMouseDown(%this,%hitItemId, %mouseClickCount)
{
	//devLog("ObjectSetTree::onMonMouseDownouseUp( %this,%hitItemId, %mouseClickCount )",%hitItemId, %mouseClickCount);
	//Nothing as now (expand moved to onAddSelection)
}
function ObjectSetTree::onRightMouseUp(%this, %itemId, %mouse, %obj)
{
	%this.showContextMenu(%itemId, %mouse, %obj);
}

//==============================================================================




//==============================================================================
// Tree Items Drag and Dropping
//==============================================================================
//==============================================================================
// Called when an item as been dropped
function ObjectSetTree::onDragDropped(%this)
{
	Scene.setDirty();
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectSetTree::isValidDragTarget(%this, %id, %obj)
{

}
//------------------------------------------------------------------------------

//==============================================================================
// Set object selected in scene (Trees and WorldEditor)
//==============================================================================




