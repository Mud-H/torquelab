//==============================================================================
// TorqueLab -> Editor Gui General Scripts
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function MissionGroup::onObjectAdded(%this,%obj)
{
	devLog("New object added to MissionGroup. Obj=",%obj,%obj.getClassName());

Lab.prepareNewObject(%obj);

}
//------------------------------------------------------------------------------
//==============================================================================
function MissionGroup::onObjectRemoved(%this,%obj)
{
	logb("Object removed to MissionGroup. Obj=",%obj);
}
//------------------------------------------------------------------------------
//==============================================================================
function LevelGroup::onObjectAdded(%this,%obj)
{
	devLog("New object added to LevelGroup. Obj=",%obj,%obj.getClassName());

	Lab.prepareNewObject(%obj);

}
//------------------------------------------------------------------------------
//==============================================================================
function LevelGroup::onObjectRemoved(%this,%obj)
{
	logb("Object removed to LevelGroup. Obj=",%obj);
}
//------------------------------------------------------------------------------


//==============================================================================
function ObjectSet::onObjectAdded(%this,%obj,%allowMultiSet)
{

	$LabObjectSet[%obj.getId()] = %this.getName();
	logb("New object added to set",%this.getName(),"Obj=",%obj);
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectSet::onObjectRemoved(%this,%obj)
{

	logb("New object removed to set",%this.getName(),"Obj=",%obj);
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjectSet::addSelectedObjects(%this)
{
	%count = EWorldEditor.getSelectionSize();


	for(%j=0; %j<%count; %j++)
	{
		%obj = EWorldEditor.getSelectedObject(%j);
		%this.add(%obj);
	}

	SceneLayerTree.setSelectedItem(%obj,false,true);
}
//------------------------------------------------------------------------------



