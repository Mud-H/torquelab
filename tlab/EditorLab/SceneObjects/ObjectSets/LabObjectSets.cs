//==============================================================================
// TorqueLab -> Editor Gui General Scripts
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
%setId = -1;

//==============================================================================
// Set object selected in scene (Trees and WorldEditor)
//==============================================================================

//==============================================================================
function Lab::initLabObjectSets(%this)
{
	//Root Simgroup holding the Layers Simset
	if (!isObject(LabObjectGroup))
		new SimGroup(LabObjectGroup);

	if (!isObject(ObjSetGroup_Class))
		newSimGroup("ObjSetGroup_Class",LabObjectGroup,"Object Class","ObjectSetGroup");

	if (!isObject(ObjSetGroup_Collection))
		newSimGroup("ObjSetGroup_Collection",LabObjectGroup,"Collection","ObjectSetGroup");

    
	%this.objSetGroup["class"] = ObjSetGroup_Class;	
	%this.objSetGroup["collection"] = ObjSetGroup_Collection;

	//Default Layer SimSet for all objects not in layer
	if (!isObject(ObjSet_Default))
		%this.addObjectSet("Default","");

    SEP_ObjectSetMenu.clear();
    SEP_ObjectSetMenu.setText("[Select Object Set]");
	Lab.prepareObjectSets();
	Lab.initLabLayerSets();

	
	SceneSetTree.objMode();	

	

}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::buildObjectSetMenu(%this)
{
   SEP_ObjectSetMenu.clear();
   foreach(%obj in ObjSetGroup_Class)
   {
      SEP_ObjectSetMenu.add(%obj.internalName,%obj.getId());
   }
}
//==============================================================================
function Lab::addObjectSet(%this,%name,%type,%notUnique)
{
	%newSet = "ObjSet_"@%name;

	if (%notUnique)
		%newSet = getUniqueName(%newSet);

	if (isObject(%newSet))
		return %newSet;

	%setGroup = LabObjectGroup;

	if (%type !$= "" && isObject(%this.objSetGroup[%type]))
		%setGroup = %this.objSetGroup[%type];   

	%set = newSimSet(%newSet,%setGroup,%name,"ObjectSet");
	
	if (%type $= "Class")
      SEP_ObjectSetMenu.add(%set.internalName,%set.getId());
      
	return %set;
}
//------------------------------------------------------------------------------


//==============================================================================
function Lab::prepareObjectSets(%this)
{
	SceneEd.activeLayer = "";
	%this.buildMissionGroupSets(MissionGroup);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::buildMissionGroupSets(%this,%simGroup,%depth)
{
	if (%depth $= "")
		%depth = 0;

	if (%depth >5)
	{
		devLog("Depth is:", %depth,"Max is","10");

		if (%depth >10)
			return;
	}

	foreach(%child in %simGroup)
	{
		Lab.checkSetForObject(%child);

		if (%child.getClassName() $= "SimGroup")
			%this.buildMissionGroupSets(%child,%depth+1);
	}

}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::prepareNewObject(%this,%obj)
{
	if (%obj.getClassName() $= "SimGroup" && isObject(Scene.addNextGroupTo))
	{
		Scene.addNextGroupTo.add(%obj);
		%obj.internalName = Scene.lastExplodedName;
		Scene.lastExplodedName = "";
		Scene.addNextGroupTo = "";
	}

	//We must add it to Default object set and also verify automatic sets
	Lab.checkSetForObject(%obj);
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::checkSetForObject(%this,%obj)
{
	if (%obj.getClassName() !$= "SimGroup")
		ObjSet_Default.add(%obj);

	%classSet = "ObjSet_"@%obj.getClassName();

	if (!isObject(%classSet))
		%classSet = Lab.addObjectSet(%obj.getClassName(),"class");

	if (isObject(%classSet))
		%classSet.add(%obj);

	if (isObject(SceneEd.activeLayer))
		SceneEd.activeLayer.add(%obj);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::removeObjectFromLayer(%this,%obj)
{
	if (!isObject(%obj) || !isObject($LabObjectLayer[%obj.getId()]))
		return;

	$LabObjectLayer[%obj.getId()].remove(%obj);
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::saveObjectSets(%this)
{
	delObj(MissionLayers);

	newScriptObject(MissionLayers);
	//MissionGroup.pushToBack(MissionLayers);


	foreach(%layer in ObjSet_Layers)
	{
		//Skip default layers as all object with no layers are assigned to it
		if (%layer.layerId $= "-1")
			continue;

		MissionLayers.layer[%layer.layerId] = %layer.internalName;

		if (%layer.layers !$= "")
			MissionLayers.parentLayer[%layer.layerId] = %layer.layers;

	}

	%file = strreplace($Server::MissionFile,".mis",".layers");
	MissionLayers.save(%file);
}
//------------------------------------------------------------------------------
