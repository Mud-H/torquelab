//==============================================================================
// TorqueLab -> Editor Gui General Scripts
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function SEP_ObjectSetMenu::onSelect(%this,%id,%text)
{
   if (!isObject(%id))
      return;
	SceneSetTree.loadSet(%id);
	
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjectSetTree::rebuild(%this)
{
	%this.clear();
	%this.open(LabObjectGroup);
	%this.buildVisibleTree();
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectSetTree::objMode(%this)
{
	%this.clear();
	%this.open(ObjSet_Default);
	%this.buildVisibleTree();
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectSetTree::loadSet(%this,%set)
{
	
	if (%set.getClassName() !$= "SimSet")
	   return;
   %this.clear();
	%this.open(%set);
	%this.buildVisibleTree();
}
//------------------------------------------------------------------------------

//==============================================================================
function SceneSetTree::onDefineIcons(%this)
{
	%icons = "tlab/art/icons/iconTables/TreeViewBase/default:" @
	         "tlab/art/icons/iconTables/TreeViewBase/default:" @
	         "tlab/art/icons/iconTables/TreeViewBase/default:" @
	         "tlab/art/icons/iconTables/TreeViewBase/activegroup_close:" @//Marked Folder
	         "tlab/art/icons/iconTables/TreeViewBase/activegroup_open:" @//Marked Expanded Folder
	         "tlab/art/icons/iconTables/TreeViewBase/hidden:" @ //Hidden
	         "tlab/art/icons/iconTables/TreeViewBase/shll_icon_passworded_hi:" @ //Locked
	         "tlab/art/icons/iconTables/TreeViewBase/shll_icon_passworded:" @
	         "tlab/art/icons/iconTables/TreeViewBase/default:" @
	         "tlab/art/icons/iconTables/TreeViewBase/simgroup:" @ //Default Folder
	         "tlab/art/icons/iconTables/TreeViewBase/default";
	%this.buildIconTable(%icons);
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjectSetTree::onAdd(%this)
{
	Scene.SceneTrees = strAddWord(Scene.SceneTrees,%this.getId(),1);
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectSetTree::onRemove(%this)
{
	Scene.SceneTrees = strRemoveWord(Scene.SceneTrees,%this.getId());
}
//------------------------------------------------------------------------------
//==============================================================================
/// @name EditorPlugin Methods
/// @{
function ObjectSetTree::handleRenameObject(%this, %name, %obj)
{
	logd(" ObjectSetTree::handleRenameObject(",%name, %obj);

	if (!isObject(%obj))
		return;

	%field = (%this.renameInternal) ? "internalName" : "name";
	%isDirty = LabObj.set(%obj,%field,%name);
	//info("Group:",%obj,"Is Dirty",%isDirty);
}

//------------------------------------------------------------------------------
//==============================================================================
function ObjectSetTree::onAddGroupSelected(%this, %simGroup)
{
	logd("ObjectSetTree::onAddGroupSelected %simGroup",%simGroup);

	Scene.setNewObjectGroup(%group);
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectSetTree::onAddMultipleSelectionBegin(%this)
{
	logd("ObjectSetTree::onAddMultipleSelectionBegin");
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectSetTree::onAddMultipleSelectionEnd(%this)
{
	logd("ObjectSetTree::onAddMultipleSelectionEnd");
}
//------------------------------------------------------------------------------

//==============================================================================
// Set object selected in scene (Trees and WorldEditor)
//==============================================================================

//==============================================================================
function ObjectSetTree::onBeginReparenting(%this)
{
	if (isObject(%this.reparentUndoAction))
		%this.reparentUndoAction.delete();

	%action = UndoActionReparentObjects::create(%this);
	%this.reparentUndoAction = %action;
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjectSetTree::onReparent(%this, %obj, %oldParent, %newParent)
{
	%this.reparentUndoAction.add(%obj, %oldParent, %newParent);
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjectSetTree::onEndReparenting(%this)
{
	%action = %this.reparentUndoAction;
	%this.reparentUndoAction = "";

	if (%action.numObjects > 0)
	{
		if (%action.numObjects == 1)
			%action.actionName = "Reparent Object";
		else
			%action.actionName = "Reparent Objects";

		%action.addToManager(Editor.getUndoManager());
		EWorldEditor.syncGui();
	}
	else
		%action.delete();
}
//------------------------------------------------------------------------------
