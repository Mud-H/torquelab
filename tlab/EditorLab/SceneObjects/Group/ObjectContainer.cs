//==============================================================================
// TorqueLab -> Scene Tree Reparenting Callbacks
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function Lab::addSelToContainer(%this,%container)
{
   if (!isObject(%container))
   {
      %container = newSimGroup(getUniqueName("ObjContainer"),Scene.getActiveSimGroup(),"ObjContainer","SimContainer");  
      
   }
	%activeSelection = EWorldEditor.getActiveSelection();
   for(%i = 0; %i < %activeSelection.getCount(); %i ++)
		{
			%sel = %activeSelection.getObject(%i);			
			%container.add(%sel);
		}
}
//------------------------------------------------------------------------------