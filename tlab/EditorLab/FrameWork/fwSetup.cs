//==============================================================================
// TorqueLab -> Initialize editor plugins
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================
// Called 500 ms after EditorGui onWake (Often overwrite by FrameWork package
function FW::initialSetup(%this)
{
	Lab.closeDisabledPluginsBin();
	show(LabPluginBar);
	show(LabPaletteBar);
	LabPluginArray.reorderChild(LabPluginArray-->SceneEditorPlugin,LabPluginArray.getObject(0));
	LabPluginArray.refresh();
	FW.checkEditorCore();
}
//------------------------------------------------------------------------------

//==============================================================================
// Called 500 ms after EditorGui onWake (Often overwrite by FrameWork package
function FW::checkLaunchedLayout(%this)
{
	FW.checkEditorCore();
	Lab.schedule(100,"initSideBar");
	skipPostFx(false);
	ETools.initTools();
	Lab.initAllToolbarGroups();
	Lab.initToolbarTrash();
	ObjectBuilderGui.init();
	EditorGui-->DisabledPluginsBox.callOnChildrenNoRecurse("setVisible",true);
}
//------------------------------------------------------------------------------
function Lab::setDefaultLayoutSize(%this, %onlyIfSmaller)
{
	EditorGui-->SideBarContainer.minExtent = $LabCfg_Layout_LeftMinWidth SPC "100";
	EditorGui-->ToolsContainer.minExtent = $LabCfg_Layout_RightMinWidth SPC "100";

	if (!%onlyIfSmaller || $LabCfg_Layout_RightMinWidth >  EditorGui-->ToolsContainer.extent.x)
		EditorGui-->ToolsContainer.setExtent($LabCfg_Layout_RightMinWidth,EditorGui-->ToolsContainer.extent.y);

	if (!%onlyIfSmaller || $LabCfg_Layout_LeftMinWidth >  EditorGui-->SideBarContainer.extent.x)
		EditorGui-->SideBarContainer.setExtent($LabCfg_Layout_LeftMinWidth,EditorGui-->SideBarContainer.extent.y);
}
//==============================================================================
// Make sure all GUIs are fine once the editor is launched
function FW::onCtrlResized(%this)
{
	//EditorFrameMain.minExtent = %this.getExtent().x - 220 SPC "12";
	//%this.checkCol();
	if (isObject(FileBrowser))
		FileBrowser.onCtrlResized();

	if (isObject(ObjectCreator))
		ObjectCreator.onCtrlResized();

	if (isObject(SideBarVIS))
		SideBarVIS.onCtrlResized();

	if (isObject(ECamViewGui))
		ECamViewGui.checkArea();

	if (Lab.currentEditor.isMethod("onLayoutResized"))
		Lab.currentEditor.onLayoutResized();

	%colPosZ = %this.columns.z;

	if (%colPosZ !$= "")
	{
		%colWidthZ = 	%this.extent.x - %colPosZ;
		%this.rightColumnSize = %colWidthZ;
	}
}



//==============================================================================
// Initialize the Editor Frames
//==============================================================================




//==============================================================================
// FROM MAIN OLD FRAME
//==============================================================================

