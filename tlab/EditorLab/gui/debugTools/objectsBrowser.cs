//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

$LabGameMap.bindCmd(keyboard, "ctrl n", "Lab.toggleGameDlg(\"DebugToolDialogs\",\"ObjectsBrowser\");");



//==============================================================================
//Load GameLab system (In-Game Editor)
function debugObjects()
{
	Lab.toggleGameDlg("DebugToolDialogs","ObjectsBrowser");
	DebugTool_ObjectsTree.open(RootGroup);
	DebugTool_ObjectsTree.buildVisibleTree(true);
}
//------------------------------------------------------------------------------