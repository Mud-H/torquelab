//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
// ETools is a special system to handle common Editor Tools Dialogs. It include
// a bunch of Dialogs that the ETools object manage the display. Use it make sure
// all Tools dialogs are set correctly and make it easy to adapt all behaviors
//==============================================================================

//==============================================================================
function ETools::initTools(%this)
{
	newSimSet("EToolsGuiSet");
	newSimSet("SelfEToolsGuiSet"); //Gui which are store in their own file
	newSimSet("EmbedEToolsGuiSet");//Gui which are store in ETools.gui file
	//Lab.addGui( EToolOverlayGui ,"Overlay");
	Lab.addGui(ETools ,"Dialog");

	%pattern = "tlab/EditorLab/gui/editorTools/*.gui";

	//call exec() on all files found matching the pattern
	for(%file = findFirstFile(%pattern); %file !$= ""; %file = findNextFile(%pattern))
	{
		%found = strFind(%file,"selfload");

		if (fileBase(%file) $= "ETools" ||fileBase(%file) $= "EToolOverlayGui" || %found)
			continue;

		execGui(%file,1);
		%gui = filebase(%file);
		$EToolsUseOwnGui[%gui.getName()] = true;
		ETools.addGui(%gui,true);
		
      %gui.setFilename(%file);
	}

	if (!LabDialogGuiSet.isMember(ETools))
		Lab.addGui(ETools ,"Dialog");

	foreach(%gui in %this)
	{
		hide(%gui);
		if (!$EToolsUseOwnGui[%gui.getName()])
         EmbedEToolsGuiSet.add(%gui);
		if (%gui.isMethod("initTool"))
			%gui.initTool();
	}


}
//------------------------------------------------------------------------------
//==============================================================================
function LabToolsDlg::onAdd(%this)
{
	ETools.addGui(%this);
	EToolsGuiSet.add(%this);
}
//------------------------------------------------------------------------------
//==============================================================================
function ETools::addGui(%this,%gui,%selfFile)
{
	ETools.add(%gui);
	EToolsGuiSet.add(%gui);
	if (%selfFile)
	   SelfEToolsGuiSet.add(%gui);

}
//------------------------------------------------------------------------------
//==============================================================================
// ETools Pre/Post save - ETools have some Dialog included but some are store extern
//==============================================================================

//==============================================================================
function ETools::onPreEditorSave(%this)
{
	foreach(%gui in ETools)
	{
		//If the %gui use it's own GUI, add to temp group and save it to it's file.
		%ownGui = $EToolsUseOwnGui[%gui.getName()];

		if (!%ownGui)
		{
		   info("Pre save ETools", %gui,"Use saved inside ETools.gui",%gui.getName(),%gui.internalName);
			continue;
		}
		
		%file = %gui.getFileName();
		if (filebase(%file) $= "ETools")
		{
         //Not using own file...
         info(%gui," is set to use it own file but it use ETools file...",%gui.getName());
         continue;		 
		   
		}
		%removeGuis = strAddWord(%removeGuis,%gui.getId());
		//GuiGroup.add(%gui);
		
	}
	foreach$(%gui in %removeGuis)
	{
	   GuiGroup.add(%gui);
	   if (isFile(%gui.getFileName()))
		{
		   %backupFile = strreplace(%gui.getFileName(),".gui",".guibak");
         pathCopy(%gui.getFileName(),%backupFile,false);
         
         if (!$EToolsFirstSaveDone)
            pathCopy(%gui.getFileName(),strreplace(%gui.getFileName(),".gui",".guiorig"),false);
		}
		%name = (%gui.internalName $= "") ? %gui.getName() : %gui.internalName;
	   info("Saving ETools:",%name,"GUI as single file",%gui.getFileName());
		%gui.save(%gui.getFileName());
	}

   $EToolsFirstSaveDone = true;
}
//------------------------------------------------------------------------------
//==============================================================================
function ETools::onPostEditorSave(%this)
{
	foreach(%gui in EToolsGuiSet)
		ETools.add(%gui);
}
//------------------------------------------------------------------------------

//==============================================================================
function ETools::getToolDlg(%this,%tool)
{
   if (isObject(%tool))
		%dlg = %tool;
	else
		%dlg = %this.findObjectByInternalName(%tool,true);

   %this.fitIntoParents();
	if (!isObject(%dlg))
	{
		warnLog("Trying to toggle invalid tool:",%tool);
		return "";
	}
	
	return %dlg;
}
//==============================================================================
// Toggle Tool Dialog Display - Toggle/Show/Hide
//==============================================================================
//==============================================================================
// Set tool visible if the dialog is visible (used by toggle button with dlg
// visible as variable so it have already been toggled, just need to show/hide tool
function ETools::setTool(%this,%tool)
{
   %dlg = %this.getToolDlg(%tool);
   if (!isObject(%dlg))	
		return;	

	if (%dlg.visible)
		%this.showTool(%tool);	
	else	
		%this.hideTool(%tool);		
}
function ETools::toggleTool(%this,%tool)
{
	%dlg = %this.getToolDlg(%tool);
   if (!isObject(%dlg))	
		return;

	

   if (%dlg.visible)
		%this.hideTool(%tool);	
	else	
		%this.showTool(%tool);		
}
//------------------------------------------------------------------------------
//==============================================================================
function ETools::showTool(%this,%tool)
{
	%dlg = %this.getToolDlg(%tool);
   if (!isObject(%dlg))	
		return;

	%this.fitIntoParents();
	%toggler = LabToolbarStack.findObjectByInternalName(%tool@"Toggle",true);

	if (isObject(%toggler))
		%toggler.setStateOn(true);

	ETools.visible = true;
	%dlg.setVisible(true);

	if (isObject(%dlg.linkedButton))
		%dlg.linkedButton.setStateOn(true);

	if (%dlg.isMethod("onShow"))
		%dlg.onShow();
}
//------------------------------------------------------------------------------
//==============================================================================
function ETools::hideTool(%this,%tool)
{
	%dlg = %this.getToolDlg(%tool);
   if (!isObject(%dlg))	
		return;
		
	%toggler = LabToolbarStack.findObjectByInternalName(%tool@"Toggle",true);

	if (isObject(%toggler))
		%toggler.setStateOn(false);

	%dlg.setVisible(false);
	%hideMe = true;

	foreach(%gui in %this)
		if (%gui.visible)
			%hideMe = false;

	if (%hideMe)
		%this.visible = 0;

	if (isObject(%dlg.linkedButton))
		%dlg.linkedButton.setStateOn(false);

	if (%dlg.isMethod("onHide"))
		%dlg.onHide();
}
//------------------------------------------------------------------------------
