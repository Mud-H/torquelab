//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function EBuilderTool::getScaledPositionFromSel(%this)
{
	%trans = Scene.getPolyhedralFromSelection();
	%pos = getField(%trans,0);
	%scale = getField(%trans,1);
	%pos.z = %pos.z -(%scale.z/2);

	return %pos TAB %scale;
}
//------------------------------------------------------------------------------
