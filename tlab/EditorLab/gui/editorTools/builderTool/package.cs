//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
package EBuilderTool_Pack
{
	function Scene::onAddSelection(%this, %obj, %isLastSelection,%source)
	{
		Parent::onAddSelection(%this, %obj, %isLastSelection,%source);

		if (%obj.getClassName() $= "Trigger")
		{
			if (EBuilderTool.toolCtrl.isMethod("onTriggerSelected"))
				EBuilderTool.toolCtrl.onTriggerSelected(%obj);
		}
	}

};
//------------------------------------------------------------------------------
