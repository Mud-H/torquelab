//==============================================================================
// TorqueLab -> EBuilderTool - Trigger Manager
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function EBuilderTool::buildTrigger(%this)
{
	%this.reset();
	%this.objectClassName = "Trigger";
	%this.addField("dataBlock", "TypeDataBlock", "Data Block", "TriggerData defaultTrigger");
	%this.addField("polyhedron", "TypeTriggerPolyhedron", "Polyhedron", "-0.5 0.5 0.0 1.0 0.0 0.0 0.0 -1.0 0.0 0.0 0.0 1.0");
	%this.process();
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::getTriggerSel(%this)
{
	%count = EWorldEditor.getSelectionSize();

	if (%count < 1)
	{
		warnLog("There's no selected objects to copy!");
		return;
	}

	for(%j=0; %j<%count; %j++)
	{
		%obj = EWorldEditor.getSelectedObject(%j);

		if (%obj.getClassName() $= "Trigger")
			%objs = strAddWord(%objs,%obj.getId(),true);
	}

	return %objs;
}
//------------------------------------------------------------------------------
//==============================================================================
function EB_TriggerButton::onClick(%this)
{
	%pos = EB_TriggerTool-->autopos.getText();
	%scale = EB_TriggerTool-->autoscale.getText();

	switch$(%this.internalName)
	{
		case "getseltrans":
			%trans =  EBuilderTool.getScaledPositionFromSel();
			EB_TriggerTool-->autopos.text = getField(%trans,0);
			EB_TriggerTool-->autoscale.text = getField(%trans,1);

		case "applypos":

			%triggers = EBuilderTool.getTriggerSel();
			EBuilderTool.applyTriggersTransform(%triggers,%pos,"");

		case "applyscale":
			%triggers = EBuilderTool.getTriggerSel();
			EBuilderTool.applyTriggersTransform(%triggers,"",%scale);

		case "applyboth":
			%triggers = EBuilderTool.getTriggerSel();
			EBuilderTool.applyTriggersTransform(%triggers,%pos,%scale);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::applyTriggersTransform(%this,%triggers,%pos,%scale)
{
	if (%triggers $= "")
		return;

	foreach$(%trigger in %triggers)
	{
		if (getWordCount(%pos) > 2)
			%trigger.position = %pos;

		if (getWordCount(%scale) > 2)
			%trigger.scale = %scale;
	}
}
//------------------------------------------------------------------------------
