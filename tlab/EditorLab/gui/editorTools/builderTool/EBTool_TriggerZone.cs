//==============================================================================
// TorqueLab -> Special Trigger for Location Tool
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
$EB_TriggerZoneAutoInternal = false;
$EB_TriggerZoneInternalAsLocation = true;
$EB_TriggerZone_InternalDefault = "Loc";
//==============================================================================
function EBuilderTool::buildTriggerZone(%this)
{
	%this.reset();
	%this.objectClassName = "Trigger";
	%this.addField("dataBlock", "TypeDataBlock", "Data Block", "TriggerData LocationTrigger");
	%this.addField("polyhedron", "TypeTriggerPolyhedron", "Polyhedron", "-0.5 0.5 0.0 1.0 0.0 0.0 0.0 -1.0 0.0 0.0 0.0 1.0");

	if (!$EB_TriggerZoneInternalAsLocation)
	{
		%this.addField("location",     "TypeString",    "Location", "");
		EB_TriggerZoneSelect-->location.visible = 1;
	}
	else
		EB_TriggerZoneSelect-->location.visible = 0;

	%this.addField("internalName",     "TypeString",    "internalName", $EB_TriggerZone_InternalDefault);
	%this.process();
}
//------------------------------------------------------------------------------
//==============================================================================
function EB_TriggerToolZone::onTriggerSelected(%this,%obj)
{
	foreach$(%field in "location name internalName")
	{
		%ctrl = EB_TriggerZoneSelect.findObjectByInternalName(%field,true);
		%value = %obj.getFieldValue(%field);

		if (%value $= "")
			%value = "";

		%ctrl-->value.setText(%value);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::getTriggerZoneSel(%this)
{
	%count = EWorldEditor.getSelectionSize();

	if (%count < 1)
	{
		warnLog("There's no selected objects to copy!");
		return;
	}

	for(%j=0; %j<%count; %j++)
	{
		%obj = EWorldEditor.getSelectedObject(%j);

		if (%obj.getClassName() !$= "Trigger")
			continue;

		%obj.dataBlock = "LocationTrigger";
		%objs = strAddWord(%objs,%obj.getId(),true);
	}

	return %objs;
}
//------------------------------------------------------------------------------
//==============================================================================
function EB_TriggerToolZone::applyEdit(%this,%container)
{

	%triggers = EBuilderTool.getTriggerZoneSel();

	if (%triggers $= "")
		return;

	%field = %container.internalName;
	%value = %container-->value.getText();

	foreach$(%trigger in %triggers)
	{
		%trigger.setFieldValue(%field,%value);

		if (%field $= "location" && $EB_TriggerZoneAutoInternal)
		{
			%int = $EB_TriggerZone_InternalDefault SPC %value;
			%trigger.setFieldValue("internalName",%int);
			EB_TriggerZoneSelect-->internalName-->value.text = %int;
		}
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function EB_TriggerZoneButton::onClick(%this)
{
	%pos = EB_TriggerZoneAutoTrans-->autopos.getText();
	%scale = EB_TriggerZoneAutoTrans-->autoscale.getText();

	switch$(%this.internalName)
	{
		case "getseltrans":
			%trans =  EBuilderTool.getScaledPositionFromSel();
			EB_TriggerZoneAutoTrans-->autopos.text = getField(%trans,0);
			EB_TriggerZoneAutoTrans-->autoscale.text = getField(%trans,1);

		case "applypos":

			%triggers = EBuilderTool.getTriggerZoneSel();
			EBuilderTool.applyTriggerZonesTransform(%triggers,%pos,"");

		case "applyscale":
			%triggers = EBuilderTool.getTriggerZoneSel();
			EBuilderTool.applyTriggerZonesTransform(%triggers,"",%scale);

		case "applyboth":
			%triggers = EBuilderTool.getTriggerZoneSel();
			EBuilderTool.applyTriggerZonesTransform(%triggers,%pos,%scale);

		case "applyedit":
			EB_TriggerToolZone.applyEdit(%this.getParent());
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::applyTriggerZonesTransform(%this,%triggers,%pos,%scale)
{
	if (%triggers $= "")
		return;

	foreach$(%trigger in %triggers)
	{
		if (getWordCount(%pos) > 2)
			%trigger.position = %pos;

		if (getWordCount(%scale) > 2)
			%trigger.scale = %scale;
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function EB_TriggerZoneAutoIntCheck::onClick(%this)
{
	EB_TriggerZoneSelect-->internalName-->value.active = !%this.isStateOn();
}
//------------------------------------------------------------------------------
//==============================================================================
function EB_TriggerToolZone::onPostCreate(%this,%obj)
{
	if ($EB_TriggerZoneInternalAsLocation)
		EB_TriggerZoneSelect-->location.visible = 0;

	if ($EB_TriggerZoneAutoInternal)
	{
		%int = $EB_TriggerZone_InternalDefault SPC %obj.location;
		%obj.setFieldValue("internalName",%int);
		EB_TriggerZoneSelect-->internalName-->value.text = %int;
	}
}
//------------------------------------------------------------------------------
