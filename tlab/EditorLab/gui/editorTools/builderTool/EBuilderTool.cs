//==============================================================================
// TorqueLab -> EBuilderTool - Special Tools for Special Objects
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function EBuilderTool::onWake(%this,%id,%text)
{
	activatePackage(EBuilderTool_Pack);
	EB_FitTransMenu.clear();
	EB_FitTransMenu.add("Default transform",0);
	EB_FitTransMenu.setSelected(0,false);

	EBuilderTool.autoEval = "";
	EB_ClassMenu.clear();
	EB_ClassMenu.add("Select a tool",0);
	EB_ClassMenu.add("Trigger",1);
	EB_ClassMenu.add("TriggerZone",2);
	EB_ClassMenu.setSelected(2,true);


	%this.init();

}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::onSleep(%this,%id,%text)
{
	deactivatePackage(EBuilderTool_Pack);
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::toggle(%this)
{
	if (!ETools.isMember(%this))
		ETools.add(%this);

	ETools.toggleTool("BuilderTool");
	return;

	//%this.visible = 1;
	if (%this.isAwake())
		popDlg(%this);
	else
		pushDlg(%this);

}
//------------------------------------------------------------------------------


//==============================================================================
function EBuilderTool::setActiveTool(%this,%tool)
{
	foreach(%ctrl in EB_ClassTools)
		%ctrl.visible = 0;

	if (EBuilderTool.isMethod("build"@%tool))
		eval("EBuilderTool.build"@%tool@"();");

	EBuilderTool.autoEval = "EBuilderTool.build"@%tool@"();";

	EBuilderTool.toolCtrl = EB_ClassTools.findObjectByInternalName(%tool,true);
	EBuilderTool.toolCtrl.visible = 1;
	EB_FitTransMenu.clear();
	EB_FitTransMenu.add("Default transform",0);
	EB_FitTransMenu.add("Fit to selection",1);

	if (isObject(EBuilderTool.toolCtrl-->autopos))
		EB_FitTransMenu.add("Use autotrans values",2);

	EB_FitTransMenu.setSelected(0,false);

}
//------------------------------------------------------------------------------
//==============================================================================
function EB_ClassMenu::onSelect(%this,%id,%text)
{
	EBuilderTool.setActiveTool(%text);

}
//------------------------------------------------------------------------------

//==============================================================================
function EBuilderTool::adjustSizes(%this)
{
	if (%this.numControls == 0)
		%this.curYPos = 0;

	EB_TargetWindow.extent = getWord(EB_TargetWindow.extent, 0) SPC %this.curYPos + 88;
	EB_ContentWindow.extent = getWord(EB_ContentWindow.extent, 0) SPC %this.curYPos;
	EB_OKButton.position = getWord(EB_OKButton.position, 0) SPC %this.curYPos + 57;
	EB_CancelButton.position = getWord(EB_CancelButton.position, 0) SPC %this.curYPos + 57;
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::init(%this)
{
	EB_TriggerZoneSelect-->internalName-->value.active = 1;
	%this.baseOffsetX       = 5;
	%this.baseOffsetY       = 5;
	%this.defaultObjectName = "";
	%this.defaultFieldStep  = 22;
	%this.columnOffset      = 110;
	%this.fieldNameExtent   = "105 18";
	%this.textEditExtent    = "122 18";
	%this.checkBoxExtent    = "13 18";
	%this.popupMenuExtent   = "122 18";
	%this.fileButtonExtent  = "122 18";
	%this.matButtonExtent   = "17 18";
	//
	%this.numControls       = 0;
	%this.lastPath          = "";
	%this.reset();
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::reset(%this)
{
	%this.objectGroup       = "";
	%this.curXPos           = %this.baseOffsetX;
	%this.curYPos           = %this.baseOffsetY;
	%this.createFunction    = "";
	%this.createCallback    = "";
	%this.currentControl    = 0;
	//
	EB_ObjectName.setValue(%this.defaultObjectName);
	//
	%this.newObject         = 0;
	%this.objectClassName   = "";
	%this.numFields         = 0;

	EB_ContentWindow.clear();
	//
	/*for(%i = 0; %i < %this.numControls; %i++) {
		%this.textControls[%i].delete();
		%this.controls[%i].delete();
	}*/
	%this.numControls = 0;
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::process(%this)
{
	if (%this.objectClassName $= "")
	{
		error("EBuilderTool::process: classname is not specified");
		return;
	}

	EB_TargetWindow.text = "Create Object: " @ %this.objectClassName;
	%name = getUniqueName(%this.objectClassName@"_1");

	if (!%this.nameEmpty)
		EB_ObjectName.setText(%name);

	//
	for(%i = 0; %i < %this.numFields; %i++)
	{
		switch$(%this.field[%i, type])
		{
			case "TypeBool":
				%this.createBoolType(%i);

			case "TypeDataBlock":
				%this.createDataBlockType(%i);

			case "TypeFile":
				%this.createFileType(%i);

			case "TypeMaterialName":
				%this.createMaterialNameType(%i);

			default:
				%this.createStringType(%i);
		}
	}

	// add the controls
	for(%i = 0; %i < %this.numControls; %i++)
	{

		EB_ContentWindow.add(%this.textControls[%i]);
		EB_ContentWindow.add(%this.controls[%i]);
	}

	%this.adjustSizes();
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::processNewObject(%this, %obj)
{
	//Get selection transform now before the new object is the selection
	%fitMode = EB_FitTransMenu.getSelected();

	if (%fitMode $= "1")
	{
		%trans = %this.getScaledPositionFromSel();
		%pos = getField(%trans,0);
		%scale = getField(%trans,1);
	}
	else if (%fitMode $= "2")
	{

		%pos = EBuilderTool.toolCtrl-->autopos.getText();
		%scale = EBuilderTool.toolCtrl-->autoscale.getText();

	}


	if (%this.createCallback !$= "")
		eval(%this.createCallback);

	// Skip out if nothing was created.
	if (!isObject(%obj))
		return;

	// Add the object to the group.
	if (%this.objectGroup !$= "")
		%this.objectGroup.add(%obj);
	else
		Scene.getActiveSimGroup().add(%obj);

	// If we were given a callback to call after the
	// object has been created, do so now.  Also clear
	// the callback to make sure it's valid only for
	// a single invocation.
	%callback = %this.newObjectCallback;
	%this.newObjectCallback = "";

	if (%callback !$= "")
		eval(%callback @ "( " @ %obj @ " );");

	%noDrop = true;

	if (getWordCount(%pos) < 3)
		%noDrop = false;
	else
		%obj.position = %pos;


	if (getWordCount(%scale) > 2)
		%obj.scale = %scale;

	Scene.onObjectCreated(%obj,%noDrop);

	if (EBuilderTool.toolCtrl.isMethod("onPostCreate"))
		EBuilderTool.toolCtrl.onPostCreate(%obj);

}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::onOK(%this)
{
	// Error out if the given object name is not valid or not unique.
	%objectName = EB_ObjectName.getValue();

	if (!Lab::validateObjectName(%objectName, false))
		return;


	%this.createObject();



	%this.reset();

	if (EBuilderTool.autoEval !$= "")
		eval(EBuilderTool.autoEval);
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::createObject(%this)
{
	// Error out if the given object name is not valid or not unique.
	%objectName = EB_ObjectName.getValue();

	if (!Lab::validateObjectName(%objectName, false))
		return;

	// get current values
	for(%i = 0; %i < %this.numControls; %i++)
	{
		// uses button type where getValue returns button state!
		if (%this.field[%i, type] $= "TypeFile")
		{
			if (strchr(%this.field[%i, value],"*") !$= "")
				%this.field[%i, value] = "";

			continue;
		}

		if (%this.field[%i, type] $= "TypeMaterialName")
		{
			%this.field[%i, value] = %this.controls[%i]-->MatText.getValue();
			continue;
		}

		%this.field[%i, value] = %this.controls[%i].getValue();
	}

	// If we have a special creation function then
	// let it do the construction.
	if (%this.createFunction !$= "")
		eval(%this.createFunction);
	else
	{
		// Else we use the memento.
		%memento = %this.buildMemento();
		eval(%memento);
	}

	if (%this.newObject != 0)
		%this.processNewObject(%this.newObject);

}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::onCancel(%this)
{
	%this.reset();
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::addField(%this, %name, %type, %text, %value, %ext)
{
	%this.field[%this.numFields, name] = %name;
	%this.field[%this.numFields, type] = %type;
	%this.field[%this.numFields, text] = %text;
	%this.field[%this.numFields, value] = %value;
	%this.field[%this.numFields, ext] = %ext;
	%this.numFields++;
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::buildMemento(%this)
{
	// Build the object into a string.
	%this.memento = %this @ ".newObject = new " @ %this.objectClassName @ "(" @ EB_ObjectName.getValue() @ ") { ";

	for(%i = 0; %i < %this.numFields; %i++)
		%this.memento = %this.memento @ %this.field[%i, name] @ " = \"" @ %this.field[%i, value] @ "\"; ";

	%this.memento = %this.memento @ "};";
	return %this.memento;
}
//------------------------------------------------------------------------------

