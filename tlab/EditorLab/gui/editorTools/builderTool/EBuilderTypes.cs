//==============================================================================
// TorqueLab -> EBuilder Cor Scripts
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function EBuilderTool::createFileType(%this, %index)
{
	if (%index >= %this.numFields || %this.field[%index, name] $= "")
	{
		error("EBuilderTool::createFileType: invalid field");
		return;
	}

	//
	if (%this.field[%index, text] $= "")
		%name = %this.field[%index, name];
	else
		%name = %this.field[%index, text];

	//
	%this.textControls[%this.numControls] = new GuiTextCtrl()
	{
		profile = "ToolsGuiTextRightProfile";
		text = %name;
		extent = %this.fieldNameExtent;
		position = %this.curXPos @ " " @ %this.curYPos;
		modal = "1";
	};
	//
	%this.controls[%this.numControls] = new GuiButtonCtrl()
	{
		HorizSizing = "width";
		profile = "ToolsButtonBaseA";
		extent = %this.fileButtonExtent;
		position = %this.curXPos + %this.columnOffset @ " " @ %this.curYPos;
		modal = "1";
		command = %this @ ".getBTFileName(" @ %index @ ");";
	};
	%val = %this.field[%index, value];
	%this.controls[%this.numControls].setValue(fileBase(%val) @ fileExt(%val));
	%this.numControls++;
	%this.curYPos += %this.defaultFieldStep;
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::getFileName(%this, %index)
{
	return Parent::getFileName();
}
function EBuilderTool::getBTFileName(%this, %index)
{
	if (%index >= %this.numFields || %this.field[%index, name] $= "")
	{
		error("EBuilderTool::getFileName: invalid field");
		return;
	}

	%val = %this.field[%index, ext];
	//%path = filePath(%val);
	//%ext = fileExt(%val);
	%this.currentControl = %index;
	getLoadFilename(%val @ "|" @ %val, %this @ ".gotFileName", %this.lastPath);
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::gotFileName(%this, %name)
{
	%index = %this.currentControl;
	%name = makeRelativePath(%name,getWorkingDirectory());
	%this.field[%index, value] = %name;
	%this.controls[%this.currentControl].setText(fileBase(%name) @ fileExt(%name));
	%this.lastPath = %name;
	// This doesn't work for button controls as getValue returns their state!
	//%this.controls[%this.currentControl].setValue(%name);
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::createMaterialNameType(%this, %index)
{
	if (%index >= %this.numFields || %this.field[%index, name] $= "")
	{
		error("EBuilderTool::createMaterialNameType: invalid field");
		return;
	}

	//
	if (%this.field[%index, text] $= "")
		%name = %this.field[%index, name];
	else
		%name = %this.field[%index, text];

	//
	%this.textControls[%this.numControls] = new GuiTextCtrl()
	{
		profile = "ToolsGuiTextRightProfile";
		text = %name;
		extent = %this.fieldNameExtent;
		position = %this.curXPos @ " " @ %this.curYPos;
		modal = "1";
	};
	//
	%this.controls[%this.numControls] = new GuiControl()
	{
		HorizSizing = "width";
		profile = "ToolsDefaultProfile";
		extent = %this.textEditExtent;
		position = %this.curXPos + %this.columnOffset @ " " @ %this.curYPos;
		modal = "1";
	};
	%text = new GuiTextEditCtrl()
	{
		class = EBuilderTextEdit;
		internalName = "MatText";
		HorizSizing = "width";
		profile = "ToolsTextEdit";
		extent = getWord(%this.textEditExtent,0) - getWord(%this.matButtonExtent,0) - 2 @ " " @ getWord(%this.textEditExtent,1);
		text = %this.field[%index, value];
		position = "0 0";
		modal = "1";
	};
	%this.controls[%this.numControls].addGuiControl(%text);
	%button = new GuiBitmapButtonCtrl()
	{
		internalName = "MatButton";
		HorizSizing = "left";
		profile = "ToolsButtonBaseA";
		extent = %this.matButtonExtent;
		position = getWord(%this.textEditExtent,0) - getWord(%this.matButtonExtent,0) @ " 0";
		modal = "1";
		command = %this @ ".getMaterialName(" @ %index @ ");";
	};
	%button.setBitmap("tlab/materialEditor/assets/change-material-btn");
	%this.controls[%this.numControls].addGuiControl(%button);
	//%val = %this.field[%index, value];
	//%this.controls[%this.numControls].setValue(%val);
	//%this.controls[%this.numControls].setBitmap("tlab/materialEditor/assets/change-material-btn");
	%this.numControls++;
	%this.curYPos += %this.defaultFieldStep;
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::getMaterialName(%this, %index)
{
	if (%index >= %this.numFields || %this.field[%index, name] $= "")
	{
		error("EBuilderTool::getMaterialName: invalid field");
		return;
	}

	%this.currentControl = %index;
	MaterialSelector.showDialog(%this @ ".gotMaterialName", "name");
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::gotMaterialName(%this, %name)
{
	%index = %this.currentControl;
	%this.field[%index, value] = %name;
	%this.controls[%index]-->MatText.setText(%name);
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::createDataBlockType(%this, %index)
{
	if (%index >= %this.numFields || %this.field[%index, name] $= "")
	{
		error("EBuilderTool::createDataBlockType: invalid field");
		return;
	}

	//
	if (%this.field[%index, text] $= "")
		%name = %this.field[%index, name];
	else
		%name = %this.field[%index, text];

	//
	%this.textControls[%this.numControls] = new GuiTextCtrl()
	{
		profile = "ToolsGuiTextRightProfile";
		text = %name;
		extent = %this.fieldNameExtent;
		position = %this.curXPos @ " " @ %this.curYPos;
		modal = "1";
	};
	//
	%this.controls[%this.numControls] = new GuiPopupMenuCtrl()
	{
		HorizSizing = "width";
		profile = "ToolsDropdownProfile";
		extent = %this.popupMenuExtent;
		position = %this.curXPos + %this.columnOffset @ " " @ %this.curYPos;
		modal = "1";
		maxPopupHeight = "200";
	};
	%classname = getWord(%this.field[%index, value], 0);
	%classname_alt = getWord(%this.field[%index, value], 1);
	%this.controls[%this.numControls].add("", -1);

	// add the datablocks
	for(%i = 0; %i < DataBlockGroup.getCount(); %i++)
	{
		%obj = DataBlockGroup.getObject(%i);

		if (isMemberOfClass(%obj.getClassName(), %classname) || isMemberOfClass(%obj.getClassName(), %classname_alt))
			%this.controls[%this.numControls].add(%obj.getName(), %i);
	}

	%this.controls[%this.numControls].setValue(getWord(%this.field[%index, value], 1));
	%this.numControls++;
	%this.curYPos += %this.defaultFieldStep;
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::createBoolType(%this, %index)
{
	if (%index >= %this.numFields || %this.field[%index, name] $= "")
	{
		error("EBuilderTool::createBoolType: invalid field");
		return;
	}

	//
	if (%this.field[%index, value] $= "")
		%value = 0;
	else
		%value = %this.field[%index, value];

	//
	if (%this.field[%index, text] $= "")
		%name = %this.field[%index, name];
	else
		%name = %this.field[%index, text];

	//
	%this.textControls[%this.numControls] = new GuiTextCtrl()
	{
		profile = "ToolsGuiTextRightProfile";
		text = %name;
		extent = %this.fieldNameExtent;
		position = %this.curXPos @ " " @ %this.curYPos;
		modal = "1";
	};
	//
	%this.controls[%this.numControls] = new GuiCheckBoxCtrl()
	{
		profile = "ToolsCheckBoxMain";
		extent = %this.checkBoxExtent;
		position = %this.curXPos + %this.columnOffset @ " " @ %this.curYPos;
		modal = "1";
	};
	%this.controls[%this.numControls].setValue(%value);
	%this.numControls++;
	%this.curYPos += %this.defaultFieldStep;
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTextEdit::onGainFirstResponder(%this)
{
	%this.selectAllText();
}
//------------------------------------------------------------------------------
//==============================================================================
function EBuilderTool::createStringType(%this, %index)
{
	if (%index >= %this.numFields || %this.field[%index, name] $= "")
	{
		error("EBuilderTool::createStringType: invalid field");
		return;
	}

	//
	if (%this.field[%index, text] $= "")
		%name = %this.field[%index, name];
	else
		%name = %this.field[%index, text];

	//
	%this.textControls[%this.numControls] = new GuiTextCtrl()
	{
		profile = "ToolsGuiTextRightProfile";
		text = %name;
		extent = %this.fieldNameExtent;
		position = %this.curXPos @ " " @ %this.curYPos;
		modal = "1";
	};
	//
	%this.controls[%this.numControls] = new GuiTextEditCtrl()
	{
		class = EBuilderTextEdit;
		HorizSizing = "width";
		profile = "ToolsTextEdit";
		extent = %this.textEditExtent;
		text = %this.field[%index, value];
		position = %this.curXPos + %this.columnOffset @ " " @ %this.curYPos;
		modal = "1";
	};
	%this.numControls++;
	%this.curYPos += %this.defaultFieldStep;
}
//------------------------------------------------------------------------------
