//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
return;
//==============================================================================
function Lab::showLights(%this,%visible,%group)
{
	if (%visible $= "")
		%visible = true;
   %lights = getMissionObjectClassList("PointLight");
   foreach$(%id in %lights)
      %id.hidden = !%visible;
   
   return;

	EVisibilityLayers.setLightsVisible(%visible,%group);
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::shrinkLights(%this,%shrinked,%group)
{
	if (%shrinked $= "")
		%shrinked = true;
   %lights = getMissionObjectClassList("PointLight");
   %shrinkRadius = "0.1";
   if (%shrinked)
   {    
      foreach$(%id in %lights)
      {  
          if (%id.radius !$= %shrinkRadius)
            $LabShrinkedLightDefault[%id] = %id.radius;
            
         //%id.doAddInspect(%id);
         //%id.setFieldValue("radius",%shrinkRadius);
      }
      LabObj.updateList(%lights,"radius",%shrinkRadius);
      return;
   }
   foreach$(%id in %lights)
      {  
          if ($LabShrinkedLightDefault[%id] !$= "")
            LabObj.set(%id,"radius",$LabShrinkedLightDefault[%id]);
             //%id.setFieldValue("radius",$LabShrinkedLightDefault[%id]);            
       
         $LabShrinkedLightDefault[%id] = "";
      }
  
   
 
}
//------------------------------------------------------------------------------



//==============================================================================
function EVisibilityLayers::showLights(%this,%group)
{
	%this.setLightsVisible(true,%group);
}
//------------------------------------------------------------------------------
//==============================================================================
//EVisibilityLayers.hideLights();
function EVisibilityLayers::hideLights(%this,%group)
{
	%this.setLightsVisible(false,%group);

}
//------------------------------------------------------------------------------

//==============================================================================
function EVisibilityLayers::setLightsVisible(%this,%visible,%group)
{
	if (!isObject(%group))
	{
		if (isObject(SceneEditorCfg.lightsGroup))
			%group = SceneEditorCfg.lightsGroup;
		else
			%group = MissionGroup;
	}
   
   


	%this.lightFound = 0;
	%this.setLightGroupVisible(%visible,%group);

}
//------------------------------------------------------------------------------

//==============================================================================
function EVisibilityLayers::setLightGroupVisible(%this,%visible,%group,%level)
{
	if (!isObject(%group) || %level > 20)
		return;

	foreach(%obj in %group)
	{
		if (%obj.isMemberOfClass("SimSet"))
		{
			%this.setLightGroupVisible(%visible,%obj,%level++);
			continue;
		}

		if (%obj.isMemberOfClass("PointLight"))
		{
			%obj.hidden = !%visible;
			%this.lightFound++;
		}
	}

}
//------------------------------------------------------------------------------
