//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
$LabCfg_TransformBox_ShowPosition = true;
$LabCfg_TransformBox_ShowScale = false;
$LabCfg_TransformBox_ShowRotation = true;
$LabCfg_TransformBox_EulerRotation = false;
//==============================================================================
function ETransformBoxGui::initTool(%this)
{
	ETransformBox.fitIntoParents();
	//ETransformBox.position = "2000 0";
	hide(ETransformBoxSettings);
	ETransformBox.updateGui();
	//hide(ETransformBox);
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBox::toggleBox(%this)
{
	if (ETransformBox.notActive)
		%this.activate();
	else
		%this.deactivate();
}
//------------------------------------------------------------------------------
//ETransformBox.toggleSettings();
//==============================================================================
function ETransformBox::toggleSettings(%this)
{
   ETransformBoxSettings.visible = !ETransformBoxSettings.visible;
   
	if (!ETransformBoxSettings.visible)
		return;
   %this.setSettingsPos();
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBox::activate(%this)
{
	ETransformBox.fitIntoParents();
	ETransformBox.notActive = false;
	show(ETools);
	//show(ETransformBoxGui);
	show(%this);
	joinEvent("SceneObjectAddedChanged",ETransformBox);
	//leaveEvent("SceneSelectionChanged",ETransformBox);
	joinEvent("SceneTreeChanged",ETransformBox);
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBox::deactivate(%this)
{
	%this.notActive = true;
	hide(%this);
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBox::onSceneObjectAddedChanged(%this,%obj)
{
   %objs = SceneEditorTree.getSelectedObjectList();
   
	devLog("ETransformBox::onSceneObjectAddedChanged",%obj,"Tree",%objs);
	//if (%objSet.getCount() <= 0)
	  // return;
	//%first = %objSet.getObject(0);
	
	%first = firstWord(%objs);
	devLog("From",SceneEditorTree.getSelectedObjectList(),"Source set to:",%first);
	%this.updateSource(%first);
	
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBox::onSceneTreeChanged(%this,%objSet,%centroid)
{
   %objs = SceneEditorTree.getSelectedObjectList();
   
	devLog("ETransformBox::onSceneTreeChanged",%objs);
	//if (%objSet.getCount() <= 0)
	  // return;
	//%first = %objSet.getObject(0);
	
	%first = firstWord(%objs);
	devLog("From",SceneEditorTree.getSelectedObjectList(),"Source set to:",%first);
	%this.updateSource(%first);
	
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBox::updateGui(%this)
{
	ETransformBox.fitIntoParents();
	ETransformBoxGui.forceInsideParent();
	ETransformBoxGui-->pos.setVisible($LabCfg_TransformBox_ShowPosition);
	//%this-->scale.setVisible($LabCfg_TransformBox_ShowPosition);
	ETransformBoxGui-->rot.setVisible(0);
	ETransformBoxGui-->euler.setVisible(0);

	if ($LabCfg_TransformBox_ShowRotation)
	{
		ETransformBoxGui-->rot.setVisible(!$LabCfg_TransformBox_EulerRotation);
		ETransformBoxGui-->euler.setVisible($LabCfg_TransformBox_EulerRotation);
	}
	else
	{
		ETransformBoxGui-->rot.setVisible(0);
		ETransformBoxGui-->euler.setVisible(0);
	}

	ETransformBoxStack.updateStack();
	%this.setSettingsPos();

}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBox::setSettingsPos(%this)
{

	ETransformBoxGui.forceInsideCtrl(ETransformBox);
	%setPos = ETransformBoxGui.position;
	%setPos.x += ETransformBoxGui.extent.x - ETransformBoxSettings.extent.x;
	%setPos.y += ETransformBoxGui.extent.y;
	ETransformBoxSettings.position = %setPos;
	ETransformBoxSettings.forceInsideCtrl(ETransformBox);
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBox::updateSource(%this,%sourceObj)
{
	if (!isObject(%sourceObj) || %this.notActive)
	{
		hide(ETransformBox);
		return;
	}

	%this.fitIntoParents();
	show(%this);
	%this.sourceObject = %sourceObj;
	%posCtrl = ETransformBoxGui-->pos;
	%sourcePos = %sourceObj.position;

	foreach$(%axis in "x y z")
	{
		eval("%posCtrl"@%axis@" = %posCtrl-->"@%axis@";");
		eval("%posCtrl"@%axis@".setText(%sourcePos."@%axis@");");
	}

	%rotCtrl = ETransformBoxGui-->rot;
	%objRot = %sourceObj.rotation;

	foreach$(%axis in "x y z a")
	{
		eval("%rotCtrl"@%axis@" = %rotCtrl-->"@%axis@";");
		eval("%rotCtrl"@%axis@".setText(%objRot."@%axis@");");
	}

	%eulerCtrl = ETransformBoxGui-->euler;
	%eulerRot = rotationToEuler(%objRot);

	foreach$(%axis in "x y z")
	{
		eval("%eulerCtrl"@%axis@" = %eulerCtrl-->"@%axis@";");
		eval("%eulerCtrl"@%axis@".setText(%eulerRot."@%axis@");");
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBox::updateRotation(%this,%axis,%value)
{
	if (!isObject(%this.sourceObject))
	{
		//hide(ETransformBox);
		return;
	}

	%sourceObj = %this.sourceObject;
	%objPos = getWords(%sourceObj.getTransform(),0,2);
	%objRot = getWords(%sourceObj.getTransform(),3,6);
	eval("%objRot."@%axis@" = %angle;");
	%newTransform = %objPos SPC %objRot;
	%sourceObj.setTransform(%newTransform);
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBox::updateEuler(%this,%axis,%angle)
{
	if (!isObject(%this.sourceObject))
	{
		//hide(ETransformBox);
		return;
	}

	%sourceObj = %this.sourceObject;
	%objRot = getWords(%sourceObj.getTransform(),3,6);
	%sourceRot = rotationToEuler(%objRot);
	eval("%sourceRot."@%axis@" = %angle;");
	testEulerFromAxisAngle(%sourceRot);
	%newRot = MatrixCreateFromEuler(%sourceRot);
	%newRot = getWords(%newRot,3,6);
	%transform = %sourceObj.getTransform();
	%pos = getWords(%transform,0,2);
	%newTransform = %pos SPC %newRot;
	%sourceObj.setTransform(%newTransform);
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBox::updatePosition(%this,%axis,%pos)
{
	if (!isObject(%this.sourceObject))
	{
		//hide(ETransformBox);
		return;
	}

	%sourceObj = %this.sourceObject;
	%objRot = getWords(%sourceObj.getTransform(),3,6);
	%objPos = getWords(%sourceObj.getTransform(),0,2);
	eval("%objPos."@%axis@" = %pos;");
	%newTransform = %objPos SPC %objRot;
	%sourceObj.setTransform(%newTransform);
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBox::refresh(%this)
{
	ETransformBoxStack.updateStack();
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBoxRotationMode::OnClick(%this)
{
	if (%this.internalName $= "Euler")
		$LabCfg_TransformBox_EulerRotation = true;
	else
		$LabCfg_TransformBox_EulerRotation = false;

	ETransformBox.updateGui();
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBoxRot::OnValidate(%this)
{
	ETransformBox.updateRotation(%this.internalName,%this.getText());
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBoxEuler::OnValidate(%this)
{
	ETransformBox.updateEuler(%this.internalName,%this.getText());
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBoxPos::OnValidate(%this)
{
	ETransformBox.updatePosition(%this.internalName,%this.getText());
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBoxDrag::onMouseDragged(%this,%a1,%a2,%a3)
{
   startBasicDnD(ETransformBoxGui);
	//startDragAndDropCtrl(ETransformBoxGui);
	hide(ETransformBoxGui);
}
//------------------------------------------------------------------------------
//==============================================================================
function ETransformBoxCtrl::DragSuccess(%this,%droppedCtrl,%pos)
{
   devLog("ETransformBoxCtrl::DragSuccess(%this,%droppedOn,%pos)",%this,%droppedCtrl,%pos);
   show(%this);
   if (%droppedCtrl.droppedGui !$= "")
   {
      devLog("Dropped over a droppable ctrl:",%droppedCtrl,"TargetGui=",%droppedCtrl.droppedGui);
      if (isObject(%droppedCtrl.droppedGui))
         %droppedCtrl.droppedGui.add(%this);
      
      return;
   }
	
	%realpos = EditorGui-->WorldContainer.getRealPosition();
	%pos.x = %pos.x - %this.extent.x/2- %realpos.x;
	%pos.y = %pos.y - %this.extent.y/2 - %realpos.y;
	%this.setPosition(%pos.x,%pos.y);
	//%this.forceInsideCtrl(EditorGui-->WorldContainer);
	ETransformBox.refresh();
	ETransformBox.updateGui();
}


function ETransformBoxCtrl::droppedOnCtrl(%this,%droppedOn,%pos)
{
	devLog("ETransformBoxCtrl::droppedOnCtrl(%this,%droppedOn,%pos)",%this,%droppedOn,%pos);
}