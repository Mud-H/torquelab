//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

$GLToolColorSet_Folder = "tlab/EditorLab/tools/guiLab/data/ColorSet/";
//==============================================================================
function GLab::initToolColorSet(%this)
{

}
//------------------------------------------------------------------------------

//==============================================================================
function GLabToolColorSetApply::onClick(%this)
{
	%type = %this.internalName;

	%editGlobal = $GLab_ColorSet[%type];
	devLog("GLabToolColorSetApply",%type,%editGlobal);

	if (%editGlobal $= "" || !isObject($GLab_SelectedObject))
		return;

	colorizeProfile($GLab_SelectedObject,%editGlobal,%type);

}
//------------------------------------------------------------------------------

//==============================================================================
function GLabToolColorSetGroup::onClick(%this)
{
	%typeData = %this.internalName;
	%type = firstWord(%typeData);
	%param = restWords(%typeData);
	%editGlobal = $GLab_ColorSetGroup;

	switch$(%type)
	{
		case "Add":
			$GLToolColorSet_Group_[%editGlobal,%param] = strAddWord($GLToolColorSet_Group_[%editGlobal,%param] ,$GLab_SelectedObject.getName(),true);

		case "Remove":
			$GLToolColorSet_Group_[%editGlobal,%param] = strRemoveWord($GLToolColorSet_Group_[%editGlobal,%param] ,$GLab_SelectedObject.getName());

	}

	export("$GLToolColorSet_Group_*",$GLToolColorSet_Folder@"groupsets.cfg.cs");
	//export("$GLToolColorSet_Group_"@%editGlobal@"*",$GLToolColorSet_Folder@"groupset_"@%editGlobal@".cfg.cs");

}
//------------------------------------------------------------------------------


