//==============================================================================
// Lab GuiManager -> Profile File Analyser
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

$GLProfileSetFields_["colorFontIds"] = "fontColors[0] fontColors[1] fontColors[2] fontColors[3] fontColors[4] fontColors[5] fontColors[6] fontColors[7] fontColors[8] fontColors[9]";
$GLProfileSetFields_["colorFont"] = "fontColor fontColorHL fontColorNA fontColorSEL fontColorLink fontColorLinkHL";
$GLProfileSetFields_["All"] = $GLProfileSetFields_["colorFontIds"] SPC $GLProfileSetFields_["colorFont"];
$GLProfileFieldsToStore = "fontSize";
//==============================================================================
// Clear the Analyser saved globals
function clearProfilesGlobals()
{
	foreach$(%cType in $GLProfileColorTypes)
		$GLProfileList_Color[%cType] = "";

	GLab.resetFontProfilesList();
	$GLProfileList_["fontSource"] = "";

	foreach$(%prof in $GLProfilesWithChilds)
		$GLProfileChilds_[%prof] = "";

	$GLProfilesWithChilds = "";

	foreach$(%prof in $GLProfilesWithParent)
		$GLProfileParent_[%prof] = "";

	$GLProfilesWithParent = "";
}
//------------------------------------------------------------------------------
//==============================================================================
// Create a analyse console report of the scan result
function postProfileScanReport()
{
	devlog("//","==============================================================================");
	devlog("//","Profiles Childs Data Report");
	devlog("//","------------------------------------------------------------------------------");
	info("Profiles with childs:",$GLProfilesWithChilds);
	devlog("//","------------------------------------------------------------------------------");

	foreach$(%prof in $GLProfilesWithChilds)
	{
		info(%prof,"Childs:",$GLProfileChilds_[%prof]);
	}

	devlog("//","==============================================================================");
	devlog("//","Profiles Parents Data Report");
	devlog("//","------------------------------------------------------------------------------");
	info("Profiles with Parent:",$GLProfilesWithParent);
	devlog("//","------------------------------------------------------------------------------");


	foreach$(%prof in $GLProfilesWithParent)
	{
		info(%prof,"Parent:",$GLProfileParent_[%prof]);
	}

	info("====================================","--","==================================");
	info("Store fields info");

	foreach$(%field in $GLProfileFieldsToStore)
	{
		foreach$(%profileName in $GLProfilesWithField_[%field])
		{
			%value = $GLProfileDefaultField_[%profileName,%field];
			info("Field:",%field,"in Profile:",%profileName,"Store as:",%value);
		}
	}

	info("====================================","--","==================================");
	info("Profile Color Set Info");

	foreach$(%cType in $GLProfileColorTypes)
		info(%cType,"Profiles:",$GLProfileList_Color[%cType]);
}
//------------------------------------------------------------------------------

//==============================================================================
// Scan all profile files found in profile folder
function scanAllToolProfileFile(%postReport)
{
	clearProfilesGlobals();
	scanProfileFile("tlab/gui/profiles/baseProfiles.cs");
	%filePathScript = "tlab/gui/profiles/*.prof.cs";

	for(%file = findFirstFile(%filePathScript); %file !$= ""; %file = findNextFile(%filePathScript))
	{
		scanProfileFile(%file);
	}

	$ProfileScanDone = true;

	if (%postReport)
		postProfileScanReport();

	export("$Prof*",$GLab_DataRoot@"gameProfiles.report.txt");
}
//------------------------------------------------------------------------------
//==============================================================================
// Scan all profile files found in profile folder
function scanAllProfileFile(%postReport)
{
	clearProfilesGlobals();
	scanProfileFile("art/gui/baseProfiles.cs");
	%filePathScript = "art/gui/*profiles.cs";

	for(%file = findFirstFile(%filePathScript); %file !$= ""; %file = findNextFile(%filePathScript))
	{
		scanProfileFile(%file);
	}

	$ProfileScanDone = true;

	if (%postReport)
		postProfileScanReport();

	//if ( IsDirectory( "tlab/" ) )
	//scanAllToolProfileFile(%postReport);
}
//------------------------------------------------------------------------------
//==============================================================================
// Scan a profile line by line and get needed data
function scanProfileFile(%file)
{
	%fileObj = getFileReadObj(%file);

	if (!isObject(%fileObj)) return;

	while(!%fileObj.isEOF())
	{
		%line = %fileObj.readline();

		//------------------------------------------------------------------------
		//Check for GuiControlProfile definition START
		if (strstr(%line,"GuiControlProfile") !$= "-1")
		{
			%lineFix =  strchr(%line , "(");
			%lineFix = strReplace(%lineFix,")"," ");
			%lineFix = strReplace(%lineFix,"{","");
			%lineFix = trim(strReplace(%lineFix,"(",""));
			%lineFix = strReplace(%lineFix,":","\t");
			%profileName = trim(getField(%lineFix,0));
			%profileLink = trim(getField(%lineFix,1));
			//Set an empty global to store the fields that this profile own
			$GLProfileDefines_[%profileName] = "";

			//Check for reference to other profile
			if (isObject(%profileLink))
			{
				$GLProfileChilds_[%profileLink] = strAddWord($GLProfileChilds_[%profileLink],%profileName);
				$GLProfilesWithChilds = strAddWord($GLProfilesWithChilds,%profileLink,true);
				$GLProfileParent_[%profileName] = %profileLink;
				$GLProfilesWithParent = strAddWord($GLProfilesWithParent,%profileName,true);
			}
		}
		//------------------------------------------------------------------------
		//Check for GuiControlProfile definition END
		else if (strstr(%line,"};") !$= "-1")
		{
			%profileName = "";
			%fontType = "";
		}
		//------------------------------------------------------------------------
		//Check for GuiControlProfile field definition
		else if (strstr(%line,"=") !$= "-1")
		{
			%trimLine = trim(%line);
			%trimLine = strreplace(%trimLine,"="," ");
			%trimLine = strreplace(%trimLine,"\"","");
			%trimLine = strreplace(%trimLine,";","");
			%field = trim(getWord(%trimLine,0));
			%value = trim(removeWord(%trimLine,0));
			$GLProfileDefines_[%profileName] = strAddWord($GLProfileDefines_[%profileName],%field,true);

			if (strstr($GLProfileFieldsToStore,%field) !$= "-1")
			{
				$GLProfilesWithField_[%field] =  strAddWord($GLProfilesWithField_[%field],%profileName);
				$GLProfileDefaultField_[%profileName,%field] = %value;
			}

			if (%field $= "fontSource")
			{
				$GLProfileList_["fontSource"] = strAddWord($GLProfileList_["fontSource"],%profileName);
			}

			if (%field $= "fontType")
			{
				$GLProfilesWithFontType_[strreplace(%value," ","_")] = strAddWord($GLProfilesWithFontType_[%value],%profileName);
				$GLProfileFontTypes =  strAddWord($GLProfileFontTypes,%value);
			}

			if (strstr($GLProfileColorTypes,%field) !$= "-1")
			{
				$GLProfileList_Color[%field]  = strAddWord($GLProfileList_Color[%field],%profileName);
			}
		}
	}
}
//------------------------------------------------------------------------------
//==============================================================================
//FONTS -> Change the font to all profile or only those specified in the list
//cleanProfileFile("art/gui/TextLab.prof.cs");
function removeProfileFieldSet(%profile,%set)
{
	%targetFields = $GLProfileSetFields_[%set];

	if (%targetFields !$= "")
		removeProfileField(%profile,%targetFields);
}

function removeProfileField(%profile,%targetFields)
{
	devLog("removeProfileField",%profile.getName(),%targetFields);
	%file = %profile.getFilename();

	if (!isFile(%file))
		return;

	%fileObj = getFileReadObj(%file);

	if (!isObject(%fileObj)) return;

	while(!%fileObj.isEOF())
	{
		%line = %fileObj.readline();
		%skipLine = false;

		if (strstr(%line,"//") !$= "-1")
		{
			%skipLine = false;
		}
		else if (strstr(%line,"GuiControlProfile") !$= "-1")
		{
			// Prints 3456789
			%lineFix =  strchr(%line , "(");
			%lineFix = strReplace(%lineFix,":"," ");
			%lineFix = strReplace(%lineFix,")"," ");
			%lineFix = trim(strReplace(%lineFix,"(",""));
			%profileName = getWord(%lineFix,0);
			%targetProfile = false;

			if (%profileName $= %profile.getName())
			{
				%targetProfile = true;
			}
		}
		else if (strstr(%line,"};") !$= "-1")
		{
			//Check if default field is there
			if (%fontType !$= "")
			{
				%line[%i++] = "fontType = \"" @%fontType@"\";";
			}

			%currentProfile = "";
			%fontType = "";
		}
		else if (!%targetProfile)
		{
			%targetProfile = %targetProfile;
		}
		else if (strstr(%line,"=") !$= "-1")
		{
			%trimLine = trim(%line);
			%trimLine = strreplace(%trimLine,"="," ");
			%trimLine = strreplace(%trimLine,"\"","");
			%trimLine = strreplace(%trimLine,";","");
			%field = trim(getWord(%trimLine,0));
			%value = trim(removeWord(%trimLine,0));

			foreach$(%target in %targetFields)
			{
				if (%field $= %target)
				{
					%skipLine = true;
					//Now we should set the parent value right now
					%parent = GLab.findParentFieldSource(%profileName,%field);
					%parentValue = %parent.getFieldValue(%field);

					if (%parentValue !$= "")
						GLab.updateProfileField(%profileName,%field,%parentValue,true);

					break;
				}
			}
		}

		if (%skipLine)
		{
			continue;
		}

		%line[%i++] = %line;
	}

	closeFileObj(%fileObj);
	%fileObj = getFileWriteObj(%file);
	info("-----------------SavingFile",%file);

	//%j = 1;
	for(%j=1; %j <= %i; %j++)
	{
		%fileObj.writeLine(%line[%j]);
	}

	closeFileObj(%fileObj);

	if (%profile.getId() $= $GLab_SelectedObject.getId())
		GLab.syncProfileParamArray();
}
//------------------------------------------------------------------------------
