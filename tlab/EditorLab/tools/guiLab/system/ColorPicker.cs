//==============================================================================
// Lab GuiManager -> Profile Color Management System
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
//GetColorI( %this.backgroundColor, %this.getId() @ ".colorPicked", %this.getRoot() );
function GuiColorDefaultPicker::pickColorF(%this, %updateFunc,%arg1,%arg2,%arg3)
{
	devLog("SHOULD BE DELETED!!!!!! GuiColorPickerCtrl::pickColorF");
	%ctrl.updateCommand = %updateFunc@"(%this.internalName,%color,\""@%arg1@"\",\""@%arg2@"\",\""@%arg3@"\");";
	%currentColor =   %this.baseColor;
	%callBack = %this@".ColorPicked";
	%updateCallback = %this@".ColorUpdated";
	devLog("GuiColorPickerCtrl::pickColorF",%this,"Color",%currentColor,"Func",%updateFunc);
	GetColorF(%currentColor, %callback, %this.getRoot(), %updateCallback, %cancelCallback);
}
function GuiColorDefaultPicker::pickColorI(%this, %updateFunc,%arg1,%arg2,%arg3)
{
	devLog("SHOULD BE DELETED!!!!!! GuiColorPickerCtrl::pickColorI");
	%ctrl.updateCommand = %updateFunc@"(%this.internalName,%color,\""@%arg1@"\",\""@%arg2@"\",\""@%arg3@"\");";
	%currentColor =   ColorFloatToInt(%this.baseColor);
	%callBack = %this@".ColorPicked";
	%updateCallback = %this@".ColorUpdated";
	GetColorI(%currentColor, %callBack, %this.getRoot(),%updateCallback);
	devLog("GuiColorPickerCtrl::pickColorI",%this,"Color",%currentColor,"Func",%updateFunc);
}
//==============================================================================
// Color Picker callbacks for Gui Manager color selection
//==============================================================================

//==============================================================================
// Empty Editor Gui
function GuiColorDefaultPicker::ColorRefresh(%this)
{
	loga("GuiColorDefaultPicker::ColorRefresh(%this)",%this,%color);
	%srcObj = %this.sourceObject;
	%srcField = %this.sourceField;
	%alpha = mCeil(getWord(%color,3));
	%color = setWord(%color,3,%alpha);
	%this.baseColor = ColorIntToFloat(%color);
	%srcObj.setFieldValue(%srcField,%color);
}
//------------------------------------------------------------------------------
//==============================================================================
// Empty Editor Gui
function GuiColorDefaultPicker::ColorPicked(%this,%color)
{
	loga("GuiColorDefaultPicker::ColorPicked(%this,%color)",%this,%color);
	%srcObj = %this.sourceObject;
	%srcField = %this.sourceField;
	%alpha = mCeil(getWord(%color,3));
	%color = setWord(%color,3,%alpha);
	%this.baseColor = ColorIntToFloat(%color);
	%srcObj.setFieldValue(%srcField,%color);

	if (isObject(%this.alphaSlider))
	{
		devLog("Color Picked, updating Slider to:",%alpha);
		%this.alphaSlider.setValue(%alpha);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Empty Editor Gui
function GuiColorDefaultPicker::ColorUpdated(%this,%color)
{
	loga("GuiColorDefaultPicker::ColorUpdated(%this,%color)",%this,%color);
	%alpha = mCeil(getWord(%color,3));
	%this.baseColor = ColorIntToFloat(%color);
	// %this.sourceArray.setValue(%this.internalName,%color);
	%this.updateColor();

	if (isObject(%this.alphaSlider))
	{
		devLog("Color Updated, updating Slider to:",%alpha);
		%this.alphaSlider.setValue(%alpha);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Empty Editor Gui
function GLabSliderAlpha::update(%this)
{
	loga("GLabSliderAlpha::update(%this)",%this,%this.getValue());

	if (!isObject(%this.colorPicker))
	{
		warnLog("Invalid color picker referenced!");
		return;
	}

	%this.colorPicker.baseColor.a = %this.getValue();
	%current = $GLab_SelectedObject.getFieldValue(%this.fieldSource);
	%currentAlpha = %current.a;
	%intAlpha = mCeil(%this.getValue() * 255);
	%new = setWord(%current,3,%intAlpha);
	devLog("Update Alpha field:",%this.fieldSource,"From",%current,"To",%new);
	%this.colorPicker.evalUpdate();
}
//------------------------------------------------------------------------------
//==============================================================================
// Empty Editor Gui
function GLabSliderAlpha::altUpdate(%this)
{
	loga("GLabSliderAlpha::altUpdate(%this)",%this,%this.getValue());

	if (!isObject(%this.colorPicker))
	{
		warnLog("Invalid color picker referenced!");
		return;
	}

	%this.colorPicker.baseColor.a = %this.getValue();
}
//------------------------------------------------------------------------------
