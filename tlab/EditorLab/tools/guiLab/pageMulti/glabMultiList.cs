//==============================================================================
// GuiLab -> Page MultiList Scripts
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================


//==============================================================================
function GLMulti::initMultiList(%this)
{
	GLab.initColorManager();
}

//------------------------------------------------------------------------------



//==============================================================================
// void  onSelect (int index, string itemText)
//    Called whenever an item in the list is selected.
function GLMultiList::onSelect(%this,%index, %itemText)
{

}
//------------------------------------------------------------------------------

//==============================================================================
// void  onUnselect (int index, string itemText)
//    Called whenever a selected item in the list has been unselected.
function GLMultiList::onUnselect(%this,%index ,%itemText)
{

}
//------------------------------------------------------------------------------

//==============================================================================
// void  onClearSelection ()
//    Called whenever a selected item in the list is cleared.
function GLMultiList::onClearSelection(%this)
{

}
//------------------------------------------------------------------------------

//==============================================================================
// void  onDoubleClick ()
//    Called whenever an item in the list has been double clicked.
function GLMultiList::onDoubleClick(%this)
{

}
//------------------------------------------------------------------------------
//==============================================================================
// void  onMouseUp (int itemHit, int mouseClickCount)
//    Called whenever the mouse has previously been clicked down (onMouseDown) and
//    has now been raised on the control. If an item in the list was hit during the
//    click cycle, then the index id of the clicked object along with how many clicks
//    occured are passed into the callback.
function GLMultiList::onMouseUp(%this,%itemHit ,%mouseClickCount)
{

}
//------------------------------------------------------------------------------

//==============================================================================
// void  onMouseDragged ()
//    Called whenever the mouse is dragged across the control.
function GLMultiList::onUnselect(%this)
{

}
//------------------------------------------------------------------------------

//==============================================================================
//void  onDeleteKey ()
//    Called whenever the Delete key on the keyboard has been pressed while in this control.
function GLMultiList::onUnselect(%this)
{

}
//------------------------------------------------------------------------------
/*
Public Member Functions
virtual void  addFilteredItem ((string newItem))
  Checks if there is an item with the exact text of what is passed in, and if so the item is removed from the list and adds that item's data to the filtered list.

virtual int  addItem ((string newItem, string color=""))
  Adds an item to the end of the list with an optional color.

virtual void  clearItemColor ((int index))
  Removes any custom coloring from an item at the defined index id in the list.

virtual void  clearItems (())
  Clears all the items in the listbox.

virtual void  clearSelection (())
  Sets all currently selected items to unselected.

virtual void  deleteItem ((int itemIndex))
  Removes the list entry at the requested index id from the control and clears the memory associated with it.

virtual void  doMirror (())
  Informs the GuiListBoxCtrl object to mirror the contents of the GuiListBoxCtrl stored in the mirrorSet field.

virtual int  findItemText ((string findText, bool bCaseSensitive=false))
  Returns index of item with matching text or -1 if none found.

virtual int  getItemCount (())
  Returns the number of items in the list.

virtual string  getItemObject ((int index))
  Returns the object associated with an item. This only makes sense if you are mirroring a simset.

virtual string  getItemText ((int index))
  Returns the text of the item at the specified index.

virtual int  getLastClickItem (())
  Request the item index for the item that was last clicked.

virtual int  getSelCount (())
  Returns the number of items currently selected.

virtual int  getSelectedItem (())
  Returns the selected items index or -1 if none selected. If multiple selections exist it returns the first selected item.

virtual string  getSelectedItems (())
  Returns a space delimited list of the selected items indexes in the list.

virtual void  insertItem ((string text, int index))
  Inserts an item into the list at the specified index and returns the index assigned or -1 on error.

bool  isObjectMirrored (string indexIdString)
  Checks if a list item at a defined index id is mirrored, and returns the result.

void  onClearSelection ()
  Called whenever a selected item in the list is cleared.

void  onDeleteKey ()
  Called whenever the Delete key on the keyboard has been pressed while in this control.

void  onDoubleClick ()
  Called whenever an item in the list has been double clicked.

void  onMouseDragged ()
  Called whenever the mouse is dragged across the control.

void  onMouseUp (int itemHit, int mouseClickCount)
  Called whenever the mouse has previously been clicked down (onMouseDown) and has now been raised on the control. If an item in the list was hit during the click cycle, then the index id of the clicked object along with how many clicks occured are passed into the callback.

void  onSelect (int index, string itemText)
  Called whenever an item in the list is selected.

void  onUnselect (int index, string itemText)
  Called whenever a selected item in the list has been unselected.

virtual void  removeFilteredItem ((string itemName))
  Removes an item of the entered name from the filtered items list.

virtual void  setCurSel ((int indexId))
  Sets the currently selected item at the specified index.

virtual void  setCurSelRange ((int indexStart, int indexStop=999999))
  Sets the current selection range from index start to stop. If no stop is specified it sets from start index to the end of the list.

virtual void  setItemColor ((int index, ColorF color))
  Sets the color of a single list entry at the specified index id.

virtual void  setItemText ((int index, string newtext))
  Sets the items text at the specified index.

virtual void  setItemTooltip ((int index, string text))
  Set the tooltip text to display for the given list item.

virtual void  setMultipleSelection ((bool allowMultSelections))
  Enable or disable multiple selections for this GuiListBoxCtrl object.

virtual void  setSelected ((int index, bool setSelected=true))
  Sets the item at the index specified to selected or not.

Public Attributes
bool  allowMultipleSelections
bool  colorBullet
bool  fitParentWidth
string  makeNameCallback
string  mirrorSet

*/