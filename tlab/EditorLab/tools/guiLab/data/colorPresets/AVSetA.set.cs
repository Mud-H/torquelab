//--- OBJECT WRITE BEGIN ---
delObj(GuiColor_Group);
new SimGroup(GuiColor_Group)
{
	canSave = "1";
	canSaveDynamicFields = "1";

	new SimGroup(GuiColorFont_Group)
	{
		internalName = "colorFont";
		canSave = "1";
		canSaveDynamicFields = "1";

		new ScriptObject()
		{
			internalName = "BaseA";
			canSave = "1";
			canSaveDynamicFields = "1";
			fontColors0 = "252 254 252 255";
			fontColors1 = "252 189 81 255";
			fontColors2 = "254 227 83 255";
			fontColors3 = "254 3 62 255";
			fontColors4 = "238 255 0 255";
			fontColors5 = "3 254 148 255";
			fontColors6 = "3 21 254 255";
			fontColors7 = "254 236 3 255";
			fontColors8 = "254 3 43 255";
			fontColors9 = "3 48 248 255";
		};
		new ScriptObject()
		{
			internalName = "BaseB";
			canSave = "1";
			canSaveDynamicFields = "1";
			fontColors0 = "232 245 255 255 255";
			fontColors1 = "210 217 200 255 255";
			fontColors2 = "91 187 255 255 255";
			fontColors3 = "242 190 62 255 255";
			fontColors4 = "202 203 203 255 255";
			fontColors5 = "210 217 200 255 255";
			fontColors6 = "255 115 2 255 255";
			fontColors7 = "233 245 255 255";
			fontColors8 = "33 190 232 255 255";
			fontColors9 = "255 229 204 255 255";
		};
		new ScriptObject()
		{
			internalName = "AltA";
			canSave = "1";
			canSaveDynamicFields = "1";
			fontColors0 = "12 14 34 255";
			fontColors1 = "189 180 180 255";
			fontColors2 = "3 73 254 255";
			fontColors3 = "254 3 62 255";
			fontColors4 = "101 85 12 255";
			fontColors5 = "37 78 60 255";
			fontColors6 = "3 21 254 255";
			fontColors7 = "150 87 15 255";
			fontColors8 = "254 3 43 255";
			fontColors9 = "3 48 248 255";
		};
		new ScriptObject()
		{
			internalName = "AltB";
			canSave = "1";
			canSaveDynamicFields = "1";
			fontColors0 = "254 169 3 255";
			fontColors1 = "64 122 141 255";
			fontColors2 = "113 169 162 255";
			fontColors3 = "247 241 210 255";
			fontColors4 = "247 241 210 255";
			fontColors5 = "0 255 194 208";
			fontColors6 = "157 157 157 38";
			fontColors7 = "97 67 9 255";
			fontColors8 = "254 3 43 255";
			fontColors9 = "3 48 248 255";
		};
	};
	new SimGroup(GuiColorFill_Group)
	{
		internalName = "colorFill";
		canSave = "1";
		canSaveDynamicFields = "1";

		new ScriptObject()
		{
			internalName = "BGBaseA";
			canSave = "1";
			canSaveDynamicFields = "1";
			bevelColorLL = "219 187 11 255";
			borderColor = "234 0 255 157";
			borderColorHL = "50 53 53 255";
			borderColorNA = "39 255 0 203";
			fillColor = "69 255 0 255";
			fillColorERR = "254 3 62 255";
			fillColorHL = "255 0 9 255";
			fillColorNA = "254 227 83 255";
			fillColorSEL = "238 255 0 255";
		};
		new ScriptObject()
		{
			internalName = "BGAltA";
			canSave = "1";
			canSaveDynamicFields = "1";
			bevelColorLL = "173 136 16 255";
			borderColor = "21 255 0 95";
			borderColorHL = "50 53 252 255";
			borderColorNA = "39 255 0 203";
			fillColor = "215 3 254 255";
			fillColorERR = "254 3 62 255";
			fillColorHL = "0 9 255 255";
			fillColorNA = "254 227 39 255";
			fillColorSEL = "252 255 0 255";
		};
	};
	new ScriptObject()
	{
		internalName = "colorFill";
		canSave = "1";
		canSaveDynamicFields = "1";
		colorA = "153 113 9 255";
		ColorB = "25 25 25 255";
		ColorC = "39 60 81 255";
		DarkA = "50 255 0 255";
		DarkB = "21 20 20 255";
		DarkC = "37 36 36 255";
		LightA = "229 229 229 255";
		LightB = "192 194 194 255";
		LightC = "153 153 153 255";
	};
};
//--- OBJECT WRITE END ---
