//--- OBJECT WRITE BEGIN ---
delObj(GuiColor_Group);
new SimGroup(GuiColor_Group)
{
	canSave = "1";
	canSaveDynamicFields = "1";

	new SimGroup(GuiColorFont_Group)
	{
		internalName = "colorFont";
		canSave = "1";
		canSaveDynamicFields = "1";

		new ScriptObject()
		{
			internalName = "BaseA";
			canSave = "1";
			canSaveDynamicFields = "1";
			fontColors0 = "252 254 252 255";
			fontColors1 = "252 189 81 255";
			fontColors2 = "254 227 83 255";
			fontColors3 = "254 3 62 255";
			fontColors4 = "238 255 0 255";
			fontColors5 = "3 254 148 255";
			fontColors6 = "3 21 254 255";
			fontColors7 = "254 236 3 255";
			fontColors8 = "254 3 43 255";
			fontColors9 = "3 48 248 255";
		};
		new ScriptObject()
		{
			internalName = "BaseB";
			canSave = "1";
			canSaveDynamicFields = "1";
			fontColors0 = "254 185 5 255";
			fontColors1 = "206 196 41 255";
			fontColors2 = "32 203 14 255";
			fontColors3 = "0 30 255 255";
			fontColors4 = "143 95 14 255";
			fontColors5 = "3 254 148 255";
			fontColors6 = "3 21 254 255";
			fontColors7 = "254 236 3 255";
			fontColors8 = "254 3 43 255";
			fontColors9 = "3 48 248 255";
		};
		new ScriptObject()
		{
			internalName = "AltA";
			canSave = "1";
			canSaveDynamicFields = "1";
			fontColors0 = "12 14 34 255";
			fontColors1 = "189 180 180 255";
			fontColors2 = "3 73 254 255";
			fontColors3 = "254 3 62 255";
			fontColors4 = "101 85 12 255";
			fontColors5 = "37 78 60 255";
			fontColors6 = "3 21 254 255";
			fontColors7 = "150 87 15 255";
			fontColors8 = "254 3 43 255";
			fontColors9 = "3 48 248 255";
		};
		new ScriptObject()
		{
			internalName = "AltB";
			canSave = "1";
			canSaveDynamicFields = "1";
			fontColors0 = "12 27 15 255";
			fontColors1 = "99 83 83 255";
			fontColors2 = "3 73 254 255";
			fontColors3 = "254 3 62 255";
			fontColors4 = "67 64 12 255";
			fontColors5 = "9 71 43 255";
			fontColors6 = "3 21 254 255";
			fontColors7 = "97 67 9 255";
			fontColors8 = "254 3 43 255";
			fontColors9 = "3 48 248 255";
		};
	};
	new SimGroup(GuiColorFill_Group)
	{
		internalName = "colorFill";
		canSave = "1";
		canSaveDynamicFields = "1";

		new ScriptObject()
		{
			internalName = "BGBaseA";
			canSave = "1";
			canSaveDynamicFields = "1";
			bevelColorLL = "50 53 53 255";
			borderColor = "67 83 83 255";
			borderColorHL = "50 53 53 255";
			borderColorNA = "39 255 0 203";
			fillColor = "254 252 252 255";
			fillColorERR = "254 3 62 255";
			fillColorHL = "254 254 254 255";
			fillColorNA = "254 227 83 255";
			fillColorSEL = "0 0 0 255";
		};
		new ScriptObject()
		{
			internalName = "BGAltA";
			canSave = "1";
			canSaveDynamicFields = "1";
			bevelColorHL = "50 53 252 255";
			bevelColorLL = "0 0 0 255";
			borderColor = "67 83 83 255";
			borderColorHL = "39 39 252 203";
			borderColorNA = "39 255 0 203";
			fillColor = "120 120 122 255";
			fillColorERR = "0 0 0 255";
			fillColorHL = "129 132 129 255";
			fillColorNA = "254 227 39 255";
			fillColorSEL = "252 255 0 255";
		};
	};
	new ScriptObject()
	{
		internalName = "colorFillSet";
		canSave = "1";
		canSaveDynamicFields = "1";
		colorA = "153 113 9 255";
		ColorB = "25 25 25 255";
		ColorC = "39 60 81 255";
		DarkA = "2 2 2 255";
		DarkB = "21 20 20 255";
		DarkC = "37 36 36 255";
		LightA = "229 229 229 255";
		LightB = "192 194 194 255";
		LightC = "153 153 153 255";
	};
};
//--- OBJECT WRITE END ---
