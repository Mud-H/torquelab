//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
//$ActionMapSet["GuiEditor"] = "GuiEdMap";
//$ActionMapSet["WorldEditor"] = "EditorMap";
//==============================================================================
// Editor Open/Close Callbacks
//==============================================================================
function Lab::onEditorOpen(%this,%type)
{
	if ($TlabLogStates)
		devlog("Lab::onEditorOpen",%type);

	pushMaps($ActionMapSet[%type@"Editor"]);
	globalActionMap.unbind(mouse, button1);
	$LabEditorActiveType = %type;
	$LabEditorActive = true;
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onEditorClose(%this,%type)
{
	if ($TlabLogStates)
		devlog("Lab::onEditorClose",%type);


	globalActionMap.bind(mouse, button1, toggleCursor);
	$LabEditorActiveType = "";
	$LabEditorActive = false;

}
//------------------------------------------------------------------------------

//==============================================================================
// Editor onWake/OnSleep Callbacks
//==============================================================================
//==============================================================================
function Lab::onEditorWake(%this,%type)
{
	if ($TlabLogStates)
		devlog("Lab::onEditorWake",%type);

	$LabEditorAwake = true;
	$LabEditorAwakeType = %type;
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onEditorSleep(%this,%type)
{
	if ($TlabLogStates)
		devlog("Lab::onEditorSleep",%type);

	$LabEditorAwake = false;
	$LabEditorAwakeType = "";
	popMaps($ActionMapSet[%type@"Editor"]);
}
//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================
function Lab::onWorldEditorOpen(%this)
{
	Lab.onEditorOpen("World");

	//Check for methods in each plugin objects
	foreach(%pluginObj in EditorPluginSet)
	{
		if (%pluginObj.isMethod("onEditorOpen"))
			%pluginObj.onEditorOpen();
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onWorldEditorWake(%this)
{
	Lab.onEditorWake("World");
	EditorMap.push();

	//Check for methods in each plugin objects
	foreach(%pluginObj in EditorPluginSet)
	{
		if (%pluginObj.isMethod("onEditorWake"))
			%pluginObj.onEditorWake();
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onWorldEditorClose(%this)
{
	Lab.onEditorClose("World");

	if (isObject(HudChatEdit))
		HudChatEdit.close();

	//Restore the Client COntrolling Object
	if (isObject(Lab.clientWasControlling))
		LocalClientConnection.setControlObject(Lab.clientWasControlling);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onWorldEditorSleep(%this)
{
	Lab.onEditorSleep("World");

	// Remove the editor's ActionMaps.
	EditorMap.pop();
	MoveMap.pop();


	// Notify the editor plugins that the editor will be closing.
	foreach(%plugin in EditorPluginSet)
		%plugin.onEditorSleep();
}
//------------------------------------------------------------------------------

//==============================================================================
// GUI EDITOR CALLBACKS
//==============================================================================

//==============================================================================
function Lab::onGuiEditorOpen(%this)
{
	//devLog("Lab::onGuiEditorOpen");
	Lab.onEditorOpen("Gui");
	//GuiEdMap.schedule(100,push);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onGuiEditorWake(%this)
{
	//devLog("Lab::onGuiEditorWake");
	Lab.onEditorWake("Gui");

}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onGuiEditorClose(%this)
{
	//logd("Lab::onGuiEditorClose");
	Lab.onEditorClose("Gui");
	//guiEdMapPush(false);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onGuiEditorSleep(%this)
{
	//logd("Lab::onGuiEditorSleep");
	Lab.onEditorSleep("Gui");
	//guiEdMapPush(false);
}
//------------------------------------------------------------------------------

//==============================================================================
function edmap()
{
	checkEditorMaps();
}
//------------------------------------------------------------------------------

//==============================================================================
function checkEditorMaps()
{
	if ($LabEditorActiveType $= "")
		return;

	foreach$(%map in $ActionMapSet[$LabEditorActiveType@"Editor"])
		%map.push();

	//pushMaps($ActionMapSet[$LabEditorActiveType@"Editor"]);
}
//------------------------------------------------------------------------------
