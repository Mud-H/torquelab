//==============================================================================
// TorqueLab Editor -> FileMinimizer - Remove default values from GuiControls
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function GuiEd::initMinimizer(%this)
{
   %this.loadMinimizerDefaults();
   GuiEd.defaultLoaded = isObject(DefaultGuiFieldValues);
}
//------------------------------------------------------------------------------
//==============================================================================
//GuiEd.minimizeFolder("guiTest");
function GuiEd::minimizeFolder(%this,%folder)
{
   %pattern = %folder@"/*.gui";
   for(%file = findFirstFile(%pattern); %file !$= ""; %file = findNextFile(%pattern))
	{
	   %this.minimizeFile(%file);
	}
}
//==============================================================================
//GuiEd.minimizeFile("test.gui");
function GuiEd::minimizeFile(%this,%filename,%removeEmptyLine)
{
   if (!isObject(DefaultGuiFieldValues))
      GuiEd.initMinimizer();
   %src = DefaultGuiFieldValues;
  if ( isWriteableFileName( %filename ) ) {
     
     pathCopy(%filename,%filename@".bak",false);
		//
		// Extract any existent TorqueScript before writing out to disk
		//
		%fileObject = new FileObject();
		%fileObject.openForRead( %filename );
		%skipLines = true;
		%beforeObject = true;
		// %var++ does not post-increment %var, in torquescript, it pre-increments it,
		// because ++%var is illegal.
		%lines = -1;
		%beforeLines = -1;
		%skipLines = false;
		
		while( !%fileObject.isEOF() ) {
			%line = %fileObject.readLine();
         %oldLineCount++;
			if ( startsWith(%line,"//---" ))
			{
			   %newFileLines[ %lines++ ] = %line;
			   continue;
			}
			//Skip Empty line
			if (%removeEmptyLine && trim(%line) $= "")
			{	
			   %emptyLineRemoved++;		   
			   continue;
         }	
         if (strpos(%line,"new ") !$= "-1")
			{
			   %objInfo =  strreplace(%line,"new ",""); //Break the = " to fields
			   %objInfo = stripChars(%objInfo,"{}");
			   //info("Scanning GuiControl:",%objInfo);
			   %newFileLines[ %lines++ ] = %line;
			   continue;
			}
         
			
			if (strpos(%line," = \"") $= "-1")
			{
			   %newFileLines[ %lines++ ] = %line;
			   continue;
			}
			
			//Here we have a field value line
			// Format: position = "0 0";
          //We have found a field with default values which can be removed
        
        
         %lineData = strreplace(%line," = \"","\t"); //Break the = " to fields
          %lineData = strreplace(%lineData,"\";",""); //Remove trailing ";
          %field = trim(getField(%lineData,0)); //Trim first field as field
          %value = trim(getField(%lineData,1)); //Trim second field as value
           //info("Checking Data line",%line,"Field",%field,"Value",%value);
          //If no default value stored for the field, keep the line and continue
           if (!%src.isField(%field))
           {
               %newFileLines[ %lines++ ] = %line;
			      continue;
           }           
           
         %srcValue = %src.getFieldValue(%field);
         if (%value !$= %srcValue)
         {
             %newFileLines[ %lines++ ] = %line;
			      continue;            
         }
         
         //We have found a field with default values which can be removed
         //info("Removing default field",%field,"With Value",%value);
			
		}

		%fileObject.close();
		%fileObject.delete();
		%fo = new FileObject();
		%fo.openForWrite(%filename);


		// Write out captured TorqueScript below Gui object
		for( %i = 0; %i <= %lines; %i++ )
			%fo.writeLine( %newFileLines[ %i ] );

      %newLineCount = %lines + 1;
      %diff = %oldLineCOunt - %newLineCount;
      %ratio = %newLineCount/%oldLineCOunt * 100;
      info("Minizing result for file named:",fileBase(%filename),"Line Count Before - After:",%oldLineCOunt,%newLineCount,"Lines removed:",%diff,"Ratio(%)",%ratio);
      
		%fo.close();
		%fo.delete();
		
	} else
		LabMsgOkCancel( "Can't write to the file", "There was an error writing to file '" @ %filename @ "'. The file may be read-only.", "Ok", "Error" );
}
//------------------------------------------------------------------------------

//==============================================================================
function GuiEd::loadMinimizerDefaults(%this)
{
	%this.minimizerDefaults = new ScriptObject(DefaultGuiFieldValues)
	{
		position = "0 0";
		extent = "64 64";
		minExtent = "8 2";
		horizSizing = "right";
		vertSizing = "bottom";
		profile = "GuiDefaultProfile";
		visible = "1";
		active = "1";
		tooltipProfile = "GuiToolTipProfile";
		hovertime = "1000";
		isContainer = "1";
		canSave = "1";
		canSaveDynamicFields = "0";
		//Containers
		margin = "0 0 0 0";
		padding = "0 0 0 0";
		anchorTop = "1";
		anchorBottom = "0";
		anchorLeft = "1";
		anchorRight = "0";
		//WIndow
		resizeWidth = "1";
		resizeHeight = "1";
		canMove = "1";
		canClose = "1";
		canMinimize = "1";
		canMaximize = "1";
		canCollapse = "0";
		edgeSnap = "1";
		//LabBoxCtrl
		canMove = "1";
//GuiScrollCtrl
		willFirstRespond = "1";
		hScrollBar = "alwaysOn";
		vScrollBar = "alwaysOn";
		lockHorizScroll = "0";
		lockVertScroll = "0";
		constantThumbHeight = "0";
		childMargin = "0 0";
		mouseWheelScrollSpeed = "-1";
		// GuiRolloutCtrl
		defaultHeight = "40";
		expanded = "0";
		clickCollapse = "1";
		hideHeader = "0";
		autoCollapseSiblings = "0";
		// GuiTextCtrl
		maxLength = "1024";
		// GuiPopUpMenuCtrlEx
		hotTrackCallback = "0";
		// GuiPopUpMenuCtrl
		maxPopupHeight = "200";
		sbUsesNAColor = "0";
		reverseTextList = "0";
		bitmapBounds = "16 16";
		// GuiTextEditCtrl
		historySize = "0";
		tabComplete = "0";
		sinkAllKeyEvents = "0";
		password = "0";
		passwordMask = "*";
	};
}
//------------------------------------------------------------------------------