//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//=============================================================================================
//    GuiEditorGui.
//=============================================================================================

//---------------------------------------------------------------------------------------------

function GuiEditorGui::onWake( %this ) {
	
	Lab.onGuiEditorWake();
	GuiEdToggle.setStateOn( 1 );

   //Ruler experiments
   GuiEdRulerLeft.fitIntoParents( "height" );
    GuiEdRulerTop.fitIntoParents( "width" );
     GuiEdRulerLeft.AlignCtrlToParent( "left" );
    GuiEdRulerTop.AlignCtrlToParent( "top" );
    
   GuiEdRulerLeft.extent.y = GuiEditorScroll.extent.y;
   GuiEdRulerLeft.position.y = GuiEditorScroll.position.y;
   GuiEdRulerTop.extent = GuiEditorScroll.extent.x SPC 22;
   GuiEdRulerTop.position = GuiEditorScroll.position.x SPC 0;   
   GuiEditorRegion.pushToBack(GuiEditor);
   
	if ( !isObject( %this->SelectControlsDlg ) ) {
		GuiEdDialogs.add( GuiEditorSelectDlg );
		GuiEditorSelectDlg.setVisible( false );
	}

	// Attach our menus.

	if ( isObject( %this.menuGroup ) )
		for( %i = 0; %i < %this.menuGroup.getCount(); %i ++ )
			%this.menuGroup.getObject( %i ).attachToMenuBar();

	// Read settings.
	%this.initSettings();
	%this.readSettings();

	// Initialize toolbox.

	if ( !GuiEditorToolbox.isInitialized )
		GuiEditorToolbox.initialize();

	if (isObject(GuiEditCanvas.menuBar)) {
		// Set up initial menu toggle states.
		GuiEditCanvas.menuBar->SnapMenu.checkItem( $GUI_EDITOR_MENU_EDGESNAP_INDEX, GuiEditor.snapToEdges );
		GuiEditCanvas.menuBar->SnapMenu.checkItem( $GUI_EDITOR_MENU_CENTERSNAP_INDEX, GuiEditor.snapToCenters );
		GuiEditCanvas.menuBar->SnapMenu.checkItem( $GUI_EDITOR_MENU_GUIDESNAP_INDEX, GuiEditor.snapToGuides );
		GuiEditCanvas.menuBar->SnapMenu.checkItem( $GUI_EDITOR_MENU_CONTROLSNAP_INDEX, GuiEditor.snapToControls );
		GuiEditCanvas.menuBar->SnapMenu.checkItem( $GUI_EDITOR_MENU_CANVASSNAP_INDEX, GuiEditor.snapToCanvas );
		GuiEditCanvas.menuBar->SnapMenu.checkItem( $GUI_EDITOR_MENU_GRIDSNAP_INDEX, GuiEditor.snap2Grid );
		GuiEditCanvas.menuBar->SnapMenu.checkItem( $GUI_EDITOR_MENU_DRAWGUIDES_INDEX, GuiEditor.drawGuides );
		GuiEditCanvas.menuBar->EditMenu.checkItem( $GUI_EDITOR_MENU_FULLBOXSELECT_INDEX, GuiEditor.fullBoxSelection );
	}

	// Sync toolbar buttons.
	GuiEditorSnapCheckBox.setStateOn( GuiEditor.snap2Grid );
	GuiEditorEdgeSnapping_btn.setStateOn( GuiEditor.snapToEdges );
	GuiEditorCenterSnapping_btn.setStateOn( GuiEditor.snapToCenters );
	
	//
}

//---------------------------------------------------------------------------------------------

function GuiEditorGui::onSleep( %this) {
	Lab.onGuiEditorSleep();
	// If we are editing a control, store its guide state.
	%content = GuiEditor.getContentControl();

	if ( isObject( %content ) )
		GuiEditor.writeGuides( %content );

	// Remove our menus.

	if ( isObject( %this.menuGroup ) )
		for( %i = 0; %i < %this.menuGroup.getCount(); %i ++ )
			%this.menuGroup.getObject( %i ).removeFromMenuBar();

	// Store our preferences.
	GuiEditorGui.writeSettings();
	//Lab.onGuiEditorSleep();
}

