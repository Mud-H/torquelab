//==============================================================================
// TorqueLab -> GuiEditor - Gui Hierarchy Tree
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// Code for the main Gui Editor tree view that shows the hierarchy of the
// current GUI being edited.
//==============================================================================

//==============================================================================
/// Defines the icons to be used in the tree view control.
/// Provide the paths to each icon minus the file extension.
/// Seperate them with ':'.
/// The order of the icons must correspond to the bit array defined
/// in the GuiTreeViewCtrl.h.
function GuiEditorTreeView::onDefineIcons(%this) {
	%icons = ":" @       // Default1
				":" @       // SimGroup1
				":" @       // SimGroup2
				":" @       // SimGroup3
				":" @       // SimGroup4
				"tlab/art/icons/default/common/hidden:" @
				"tlab/art/icons/default/lockedHandle";
	GuiEditorTreeView.buildIconTable( %icons );
}
//------------------------------------------------------------------------------
//=============================================================================================
//    ActiveGui TreeView calbacks.
//=============================================================================================
//------------------------------------------------------------------------------


//==============================================================================
function GuiEditorTreeView::onAddSelection(%this,%ctrl) {
	GuiEditor.dontSyncTreeViewSelection = true;
	GuiEditor.addSelection( %ctrl );
	GuiEditor.dontSyncTreeViewSelection = false;
	GuiEditor.setFirstResponder();
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onRemoveSelection( %this, %ctrl ) {
	GuiEditor.dontSyncTreeViewSelection = true;
	GuiEditor.removeSelection( %ctrl );
	GuiEditor.dontSyncTreeViewSelection = false;
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onDeleteSelection(%this) {
   
   devLog("GuiEditorTreeView::onDeleteSelection");
   GuiEditor.deleteSelection();
	//GuiEditor.clearSelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onSelect( %this, %obj ) {
	if ( isObject( %obj ) ) {
		GuiEditor.dontSyncTreeViewSelection = true;
		GuiEditor.select( %obj );
		GuiEditor.dontSyncTreeViewSelection = false;
		GuiEditorInspectFields.update( %obj );
	}	
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::isValidDragTarget( %this, %id, %obj ) {
	return ( %obj.isContainer || %obj.getCount() > 0 );
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onBeginReparenting( %this ) {
	if ( isObject( %this.reparentUndoAction ) )
		%this.reparentUndoAction.delete();

	%action = UndoActionReparentObjects::create( %this );
	%this.reparentUndoAction = %action;
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onReparent( %this, %obj, %oldParent, %newParent ) {
	%this.reparentUndoAction.add( %obj, %oldParent, %newParent );
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onEndReparenting( %this ) {
	%action = %this.reparentUndoAction;
	%this.reparentUndoAction = "";

	if ( %action.numObjects > 0 ) {
		if ( %action.numObjects == 1 )
			%action.actionName = "Reparent Control";
		else
			%action.actionName = "Reparent Controls";

		%action.addToManager( GuiEditor.getUndoManager() );
		GuiEditor.updateUndoMenu();
	} else
		%action.delete();
}
//------------------------------------------------------------------------------
