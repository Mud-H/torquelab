//==============================================================================
// TorqueLab -> GuiEditor - Gui Hierarchy Tree
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================
function GuiEd::initGuiTreeView(%this) {
}
//------------------------------------------------------------------------------
//==============================================================================
// Code for the main Gui Editor tree view that shows the hierarchy of the
// current GUI being edited.
//==============================================================================
//==============================================================================
function GuiEditorTreeView::init(%this) {
	if ( !isObject( %this.contextMenu ) )
		%this.contextMenu = new PopupMenu() {
		superClass = "MenuBuilder";
		isPopup = true;
		item[ 0 ] = "Rename" TAB "" TAB "GuiEditorTreeView.showItemRenameCtrl( GuiEditorTreeView.findItemByObjectId( %this.object ) );";
		item[ 1 ] = "Delete" TAB "" TAB "GuiEditor.deleteControl( %this.object );";
		item[ 2 ] = "-";
		item[ 3 ] = "Locked" TAB "" TAB "%this.object.setLocked( !%this.object.locked ); GuiEditorTreeView.update();";
		item[ 4 ] = "Hidden" TAB "" TAB "%this.object.setVisible( !%this.object.isVisible() ); GuiEditorTreeView.update();";
		item[ 5 ] = "-";
		item[ 6 ] = "Add New Controls Here" TAB "" TAB "GuiEditor.setCurrentAddSet( %this.object );";
		item[ 7 ] = "Add Child Controls to Selection" TAB "" TAB "GuiEditor.selectAllControlsInSet( %this.object, false );";
		item[ 8 ] = "Remove Child Controls from Selection" TAB "" TAB "GuiEditor.selectAllControlsInSet( %this.object, true );";
		item[ 9 ] = "-";
		item[ 10 ] = "Save control as" TAB "" TAB "GuiEditCanvas.save( true,false,true );";
		object = -1;
	};

	if ( !isObject( %this.contextMenuMultiSel ) )
		%this.contextMenuMultiSel = new PopupMenu() {
		superClass = "MenuBuilder";
		isPopup = true;
		item[ 0 ] = "Delete" TAB "" TAB "GuiEditor.deleteSelection();";
	};
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::update( %this ) {
	%this.updateSourceGui();
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::updateSourceGui( %this ) {
	%obj = GuiEditorContent.getObject( 0 );

	if ( !isObject( %obj ) )
		GuiEditorTreeView.clear();
	else {
		// Open inspector tree.
		GuiEditorTreeView.open( %obj );
		// Sync selection with GuiEditor.
		GuiEditorTreeView.clearSelection();
		%selection = GuiEditor.getSelection();
		%count = %selection.getCount();

		for( %i = 0; %i < %count; %i ++ )
			GuiEditorTreeView.addSelection( %selection.getObject( %i ) );
	}
}
//------------------------------------------------------------------------------


//==============================================================================
//=============================================================================================
//    Event Handlers.
//=============================================================================================
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onRightMouseDown( %this, %item, %pts, %obj ) {
	logd("GuiEditorTreeView::onRightMouseDown",%obj);
	if ( %this.getSelectedItemsCount() > 1 ) {
		%popup = %this.contextMenuMultiSel;
		%popup.showPopup( Canvas );
	} else if ( %obj ) {
		%popup = GuiEditorTreeView.contextMenu;
		%popup.checkItem( 3, %obj.locked );
		%popup.checkItem( 4, !%obj.isVisible() );
		%popup.enableItem( 6, %obj.isContainer );
		%popup.enableItem( 7, %obj.getCount() > 0 );
		%popup.enableItem( 8, %obj.getCount() > 0 );
		%popup.object = %obj;
		%popup.showPopup( Canvas );
	}
}
//------------------------------------------------------------------------------
