//==============================================================================
// GameLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

// Code for the toolbox tab of the Gui Editor sidebar.


//---------------------------------------------------------------------------------------------


function GuiEdDlg::onAdd( %this ) {
	GuiEdDialogs.add(%this);
	%this.canSave = "0";
}
function GuiEdDlg::onPreEditorSave( %this ) {	
	%this.canSave = "1";
}
function GuiEdDlg::onPostEditorSave( %this ) {	
	%this.canSave = "0";
}
