//==============================================================================
// GameLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

// Code for the toolbox tab of the Gui Editor sidebar.


//---------------------------------------------------------------------------------------------


function GuiEditorToolbox::startGuiControlDrag( %this, %class ) {
	// Create a new control of the given class.
	%payload = eval( "return new " @ %class @ "();" );

	if ( !isObject( %payload ) )
		return;

	// this offset puts the cursor in the middle of the dragged object.
	%xOffset = getWord( %payload.extent, 0 ) / 2;
	%yOffset = getWord( %payload.extent, 1 ) / 2;
	// position where the drag will start, to prevent visible jumping.
	%cursorpos = Canvas.getCursorPos();
	%xPos = getWord( %cursorpos, 0 ) - %xOffset;
	%yPos = getWord( %cursorpos, 1 ) - %yOffset;
	// Create drag&drop control.
	%dragCtrl = new GuiDragAndDropControl() {
		canSaveDynamicFields    = "0";
		Profile                 = "ToolsSolidDefaultProfile";
		HorizSizing             = "right";
		VertSizing              = "bottom";
		Position                = %xPos SPC %yPos;
		extent                  = %payload.extent;
		MinExtent               = "32 32";
		canSave                 = "1";
		Visible                 = "1";
		hovertime               = "1000";
		deleteOnMouseUp         = true;
		class                   = "GuiDragAndDropControlType_GuiControl";
	};
	%dragCtrl.add( %payload );
	Canvas.getContent().add( %dragCtrl );
	// Start drag.
	%dragCtrl.startDragging( %xOffset, %yOffset );
}

//=============================================================================================
//    GuiEditorToolboxRolloutCtrl.
//=============================================================================================

//---------------------------------------------------------------------------------------------

function GuiEditorToolboxRolloutCtrl::onHeaderRightClick( %this ) {
	if ( !isObject( GuiEditorToolboxRolloutCtrlMenu ) )
		new PopupMenu( GuiEditorToolboxRolloutCtrlMenu ) {
		superClass = "MenuBuilder";
		isPopup = true;
		item[ 0 ] = "Expand All" TAB "" TAB %this @ ".expandAll();";
		item[ 1 ] = "Collapse All" TAB "" TAB %this @ ".collapseAll();";
	};

	GuiEditorToolboxRolloutCtrlMenu.showPopup( %this.getRoot() );
}

//---------------------------------------------------------------------------------------------

function GuiEditorToolboxRolloutCtrl::expandAll( %this ) {
	foreach( %ctrl in %this.parentGroup ) {
		if ( %ctrl.isMemberOfClass( "GuiRolloutCtrl" ) )
			%ctrl.instantExpand();
	}
}

//---------------------------------------------------------------------------------------------

function GuiEditorToolboxRolloutCtrl::collapseAll( %this ) {
	foreach( %ctrl in %this.parentGroup ) {
		if ( %ctrl.isMemberOfClass( "GuiRolloutCtrl" ) )
			%ctrl.instantCollapse();
	}
}

//=============================================================================================
//    GuiEditorToolboxButton.
//=============================================================================================

//---------------------------------------------------------------------------------------------

function GuiEditorToolboxButton::onMouseDragged( %this ) {
	GuiEditorToolbox.startGuiControlDrag( %this.text );
}

//=============================================================================================
//    Misc.
//=============================================================================================

//---------------------------------------------------------------------------------------------

/// Utility function to sort rollouts by their caption.
function _GuiEditorToolboxSortRollouts( %a, %b ) {
	return strinatcmp( %a.caption, %b.caption );
}
