//==============================================================================
// TorqueLab GUI -> WidgetBuilder system
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
// Create quickly a set of GUI based on a template
//==============================================================================
//GuiEditorProfilesTree.init
//==============================================================================
function initializeGuiEditor() {	
   if ($TLab_GuiEditorLoaded)
		return;
   if ($Cfg_GuiEditor_GuiEditor_previewResolution $= "")
      exec("tlab/config.cfg.cs");
		
	info( "TorqueLab","->","Initializing Gui Editor" );	
	if (!isObject(GuiLab))
		$GuiLab = new scriptObject("GuiLab");
   if (!isObject(GuiEd))
	   $GuiEd = new scriptObject("GuiEd");
	delObj(GuiEdMap);
	newActionMap(GuiEdMap);
	
	GuiEdMap.bind(keyboard,"delete",EditorGlobalDelete);
	GuiEdMap.bindCmd(keyboard,"delete","GuiEditorDeleteSelection(1);","");
	//GlobalActionMap.unbind(keyboard,"delete");
	// GUIs.
	execGuiEdit(true);
	$TLab_GuiEditorLoaded = true;
	
		$GuiEditor::GuiFilterList =
		"GuiEditorGui" TAB
		"AL_ShadowVizOverlayCtrl" TAB
		"ToolsMsgBoxOKDlg" TAB
		"ToolsMsgBoxOKCancelDlg" TAB
		"ToolsMsgBoxOKCancelDetailsDlg" TAB
		"ToolsMsgBoxYesNoDlg" TAB
		"ToolsMsgBoxYesNoCancelDlg" TAB
		"MessagePopupDlg";
}
//------------------------------------------------------------------------------
function execGuiLab() {
	execPattern( "tlab/guiEditor/lab/*.cs","templateManager" );
	execPattern( "tlab/guiEditor/system/*.cs" );
	execPattern( "tlab/guiEditor/helpers/*.cs" );
}
//==============================================================================

//==============================================================================
function execGuiEdit(%execGui,%execMainGui) {
	if (%execGui) {
		%execMainGui = true;
		exec( "./gui/guiEditorNewGuiDialog.gui" );
		exec( "./gui/guiEditorPrefsDlg.gui" );		
		exec( "./gui/EditorChooseGUI.gui" );

		exec( "tlab/guiEditor/gui/GuiEditFieldDuplicator.gui" );
		
	}

	if (%execMainGui) {
		exec( "tlab/guiEditor/gui/guiEditor.gui" );
		execGuiDir( "tlab/guiEditor/gui/dlgs",1);
		//exec( "tlab/guiEditor/gui/CloneEditorGui.gui" );
	}

	if (!isObject(GuiEditor)) {
	   warnLog("GuiEditor COntrol is missing, fix this before using Gui Editor");
	   return;
		addGuiEditorCtrl();
	}

	// Scripts.
	exec( "tlab/guiEditor/GuiEditorCallbacks.cs" );
	
/*	
exec( "tlab/guiEditor/scripts/GuiEditorGui.cs" );
	exec( "tlab/guiEditor/scripts/guiEditor.cs" );
	exec( "tlab/guiEditor/scripts/guiEditorTreeView.cs" );
	exec( "./scripts/guiEditorInspector.cs" );
	exec( "tlab/guiEditor/scripts/guiEditorProfiles.cs" );
	
	exec( "tlab/guiEditor/scripts/guiEditorUndo.cs" );
	exec( "tlab/guiEditor/scripts/guiEditorCanvas.cs" );
exec( "./scripts/guiEditorGroup.cs" );
	exec( "./scripts/guiEditorContentList.cs" );
	exec( "./scripts/guiEditorStatusBar.cs" );
	exec( "./scripts/guiEditorToolbox.cs" );
	exec( "./scripts/guiEditorSelectDlg.cs" );
	exec( "./scripts/guiEditorNewGuiDialog.cs" );
	exec( "./scripts/fileDialogs.cs" );
	exec( "./scripts/guiEditorPrefsDlg.cs" );
	exec( "./scripts/EditorChooseGUI.cs" );
	*/
	execPattern( "tlab/guiEditor/scripts/*.cs" );
	//exec( "tlab/guiEditor/scripts/functionControls.cs" );
	execPattern( "tlab/guiEditor/lab/*.cs","templateManager" );
	execPattern( "tlab/guiEditor/system/*.cs" );
	execPattern( "tlab/guiEditor/helpers/*.cs" );
		
	execPattern( "tlab/guiEditor/gui/dlgs/*.cs" );
	GuiEd.InitGuiEditor();
}
//------------------------------------------------------------------------------
//==============================================================================
function destroyGuiEditor() {
}
//------------------------------------------------------------------------------
function pushEdDlg(%dlg,%layer,%center)
{
	//if ($InGuiEditor)
		//return;
	Canvas.pushDialog(%dlg,%layer,%center);
}	



function guiEdMapPush(%push) {
   if (%push && !guiEd.mapPushed)
   {
      guiEd.mapPushed = true;
      guiEdMap.push();    
   }
   else if (!%push && guiEd.mapPushed)
   {
      guiEd.mapPushed = false;
      guiEdMap.pop();     
   }    
}

function GuiEditorDeleteSelection(%val) {
   devLog("GuiEditorDeleteSelection");
   if (!%val)
      return;
  GuiEditor.deleteSelection();
   
}
