//==============================================================================
// GameLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

function ForestBrushTool::onActivated( %this ) {
	ForestEditorToolbar.setVisible( true );
	%this.syncBrushToolbar();
}

function ForestBrushTool::onDeactivated( %this ) {
	ForestEditorToolbar.setVisible( false );
}

function ForestBrushTool::syncBrushToolbar( %this ) {
	return; //This have changed and need to be adapted
	%size = %this.size;
	ForestBrushSizeSliderCtrlContainer->slider.setValue( %size );
	ForestBrushSizeTextEditContainer-->textEdit.setValue( %size );
	%pres = %this.pressure;
	ForestBrushPressureSliderCtrlContainer->slider.setValue( %pres );
	ForestBrushPressureTextEditContainer-->textEdit.setValue( mCeil(100 * %pres) @ "%" );
	%hard = %this.hardness;
	ForestBrushHardnessSliderCtrlContainer->slider.setValue( %hard );
	ForestBrushHardnessTextEditContainer-->textEdit.setValue( mCeil(100 * %hard) @ "%");
}

function ForestBrushTool::onMouseDown( %this ) {
	FEP_BookData.selectPage( 0 );
}

function ForestSelectionTool::onActivated( %this ) {
}

function ForestSelectionTool::onDeactivated( %this ) {
	%this.clearSelection();
}
