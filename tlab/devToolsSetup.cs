//==============================================================================
// AlterVerse -> Special Dev Tools Setup scripts
// Copyright �2016 Alpha Entertainment Group LLC
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// Manage Developer binds conflicts
//==============================================================================

//==============================================================================
function setDevBinds(%isDev,%isSuperDev)
{
	//SuperDev don't use the menuMap but the default tools F1-F10 with
	// more tools than admins for testing and debugging

	//For developper, we need the F1 to F10 menu bind removed from menuMap
	//Instead of unbinding the keys, simply pop out the menuMap
    if (%isDev !$= "")
    $useTools = %isDev;
     if (%isSuperDev !$= "")
   $useSuperTools = %isSuperDev;
   
	
	if ($useSuperTools)
	{
	   if (!strFindWords($ActionMapAlwaysPushedList,"superDev"))
	      info("Super Dev binds mode activated", "Use F1-F10 for tools");
		$ActionMapAlwaysPushedList = strRemoveWord($ActionMapAlwaysPushedList,"menu");
		$ActionMapAlwaysPushedList = strAddWord($ActionMapAlwaysPushedList,"dev",true);
		$ActionMapAlwaysPushedList = strAddWord($ActionMapAlwaysPushedList,"superDev",true);
      
		popMap("menuMap");
		disableMap("menuMap");
		enableMap("superDevMap"); //Set enabled so it can be pushed
		pushMap("superDevMap");
		if ($defaultEditor $= "tlab")
		{
			//GlobalActionMap.bindCmd(keyboard,"F10","initTorqueLab(\"Gui\",false);","");
			//GlobalActionMap.bindCmd(keyboard,"Ctrl F10","initTorqueLab(\"Gui\",true);","");
			//GlobalActionMap.bindCmd(keyboard,"F11","initTorqueLab(\"World\");","");

			if (isObject(GuiEditor))
			{
				GlobalActionMap.bindCmd( keyboard, "f10", "toggleGuiEdit(true);","" );
				GlobalActionMap.bindCmd( keyboard, "ctrl f10", "toggleGuiEdit(true,true);","" );
				
				
			}
			else
			{
				GlobalActionMap.bindCmd(keyboard,"F10","initTorqueLab(\"Gui\",false);","");
				GlobalActionMap.bindCmd(keyboard,"Ctrl F10","initTorqueLab(\"Gui\",true);","");
			}
			if ($WorldEditorLoaded)
				GlobalActionMap.bindCmd(keyboard,"F11","toggleEditor(true);","");
			else
				GlobalActionMap.bindCmd(keyboard,"F11","initTorqueLab(\"World\");","");
		}
		else
		{
			GlobalActionMap.bind( keyboard, "f10", guiEditorToggle );
			GlobalActionMap.bind(keyboard, "f11", worldEditorToggle);
		}
	}
	else if ($useTools)
	{
	   if (!strFindWords($ActionMapAlwaysPushedList,"dev"))
	      info("Dev binds mode activated", "Use CTRL + F1-F10 for tools");
		$ActionMapAlwaysPushedList = strRemoveWord($ActionMapAlwaysPushedList,"superDev");
		$ActionMapAlwaysPushedList = strAddWord($ActionMapAlwaysPushedList,"menu");

		//devMap.bindCmd(keyboard,"ctrl F11","initTorqueLab(\"World\");","");
		if ($defaultEditor $= "tlab")
		{
			if (isObject(GuiEditor))
				devMap.bindCmd(keyboard,"ctrl F10","initTorqueLab(\"Gui\",false);","");
			else
				devMap.bindCmd( keyboard, "ctrl f10", "toggleGuiEdit(true);","" );

			if ($WorldEditorLoaded)
				devMap.bindCmd(keyboard,"ctrl F11","toggleEditor(true);","");
			else
				devMap.bindCmd(keyboard,"ctrl F11","initTorqueLab(\"World\");","");
		}
		popMap("superDevMap");
		disableMap("superDevMap");
		enableMap("menuMap"); //Set enabled so it can be pushed
		pushMap("menuMap");
	}
	else
	{
	   if (strFindWords($ActionMapAlwaysPushedList,"dev") || strFindWords($ActionMapAlwaysPushedList,"superDev"))
	      info("Dev binds mode disabled", "No tools available from binds");
		//Not any kind of dev, set default menu binds only
		$ActionMapAlwaysPushedList = strRemoveWord($ActionMapAlwaysPushedList,"dev");
		$ActionMapAlwaysPushedList = strRemoveWord($ActionMapAlwaysPushedList,"superDev");
		$ActionMapAlwaysPushedList = strAddWord($ActionMapAlwaysPushedList,"menu",true);
		enableMap("menuMap"); //Set enabled so it can be pushed
		pushMap("menuMap");
	}

	if (!$useSuperTools)
	{
		GlobalActionMap.unbind(keyboard,"F10");
		GlobalActionMap.unbind(keyboard,"Ctrl F10");
		GlobalActionMap.unbind(keyboard,"F11");
	}
}
//------------------------------------------------------------------------------
