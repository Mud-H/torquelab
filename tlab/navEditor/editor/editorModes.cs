//==============================================================================
// TorqueLab -> NavEditor Modes Script
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// NavEditor used rollout callback to set current edit mode
//==============================================================================
//==============================================================================
function NavEdModeRollout::onExpanded(%this) {
   devLog("NavEdModeRollout onExpanded",%this.internalName,NavEd.modeLocked);
  if (NavEd.modeLocked)
   return;
	NavEd.setActiveMode(%this.internalName);
}
//------------------------------------------------------------------------------
//==============================================================================
function NavEdModeRollout::onCollapsed(%this) {
	
}
//------------------------------------------------------------------------------
//==============================================================================
// Used to prevent onExpanded to call setMode when expanded from scripts
function NavEd::setModeLocked(%this,%locked) {
NavEd.modeLocked = %locked;
	
}
//------------------------------------------------------------------------------
//==============================================================================
function NavEd::setActiveMode(%this,%mode) {

	switch$(%mode) {
	case "Select":
		NavEditorGui.prepSelectionMode();

	case "Link":
		NavEditorGui.setMode("LinkMode");

	case "Cover":
		NavEditorGui.setMode("CoverMode");

	case "Tile":
		NavEditorGui.setMode("TileMode");

	case "Test":
		NavEditorGui.setMode("TestMode");
	}
	info("Nav Editor Mode set to:",%mode);
}
//------------------------------------------------------------------------------
//==============================================================================
function NavEditorGui::prepSelectionMode(%this) {
	%this.setMode("SelectMode");
	LabPaletteArray-->NavEditorSelectMode.setStateOn(1);
}
//------------------------------------------------------------------------------
//==============================================================================
function NavEditorGui::onModeSet(%this, %mode) {
	// Callback when the nav editor changes mode. Set the appropriate dynamic
	// GUI contents in the properties/actions boxes.
	//NavInspector.setVisible(false);
	%actions = NavEditorOptionsWindow-->ActionsBox;
	%actions->SelectActions.setVisible(false);
	%actions->LinkActions.setVisible(false);
	%actions->CoverActions.setVisible(false);
	%actions->TileActions.setVisible(false);
	%actions->TestActions.setVisible(false);
	//%properties = NavEd_PropertiesBox;
	//NavEd_PropertiesBox->LinkProperties.setVisible(false);
	//NavEd_PropertiesBox-->TileProperties.setVisible(false);
	//NavEd_PropertiesBox-->TestProperties.setVisible(false);
	
	%shortMode = strReplace(%mode,"Mode","");
    NavEd.setModeLocked(true);
   foreach(%rollout in NavEd_ModeRolloutBox)
   {
      %expandMe = 0;
        devLog("DevMode=",%shortMode,"From",%rollout.internalName);
      if (%rollout.internalName $= %shortMode)
         %expandMe = 1;
      
      %isExpanded = %rollout.isExpanded();
      
      if (%expandMe != %isExpanded)         
       %rollout.expanded = %expandMe;
   }
   
  
	switch$(%mode) {
	case "SelectMode":
		//NavEd_ActionBook.selectPage(0);
		NavInspector.setVisible(true);
		%actions->SelectActions.setVisible(true);

	case "LinkMode":
		//NavEd_ActionBook.selectPage(1);
		%actions->LinkActions.setVisible(true);
		//NavEd_PropertiesBox->LinkProperties.setVisible(true);

	case "CoverMode":
		//
		//NavEd_ActionBook.selectPage(2);
		%actions->CoverActions.setVisible(true);

	case "TileMode":
		//NavEd_ActionBook.selectPage(3);
		%actions->TileActions.setVisible(true);
		//NavEd_PropertiesBox->TileProperties.setVisible(true);

	case "TestMode":
	if (!isObject($NavEd_TestFollowObject))
	   $NavEd_TestFollowObject = LocalClientCOnnection.player;
		//NavEd_ActionBook.selectPage(4);
		%actions->TestActions.setVisible(true);
		//NavEd_PropertiesBox->TestProperties.setVisible(true);
	}
	 //NavEd.setModeLocked = false;
	NavEd.schedule(1000,setModeLocked,false);
}
//------------------------------------------------------------------------------
//==============================================================================
// OLD NEED REVIEWED
//==============================================================================

//==============================================================================
$Nav::EditorOpen = false;
/*
function NavEd_ActionBook::onTabSelected(%this,%text,%index) {
	switch$(%text) {
	case "Edit":
		NavEditorGui.prepSelectionMode();

	case "Link":
		NavEditorGui.setMode("LinkMode");

	case "Cover":
		NavEditorGui.setMode("CoverMode");

	case "Tile":
		NavEditorGui.setMode("TileMode");

	case "Test":
		NavEditorGui.setMode("TestMode");
	}
}

*/

function NavEditorGui::onEditorActivated(%this) {
   NavEd_NavMeshMenu.clear();
   foreach(%obj in ServerNavMeshSet)
      NavEd_NavMeshMenu.add(%obj.getName(),%obj.getId());
   
   if (%this.selectedObject)
      NavEd_NavMeshMenu.setText(NavEditorGui.selectedObject.getName());   
   
	if (%this.selectedObject)
		%this.selectObject(%this.selectedObject);

	%this.prepSelectionMode();
}

function NavEditorGui::onEditorDeactivated(%this) {
	if (%this.getMesh())
		%this.deselect();
}

function NavEditorGui::paletteSync(%this, %mode) {
	// Synchronise the palette (small buttons on the left) with the actual mode
	// the nav editor is in.
	%evalShortcut = "ToolsPaletteArray-->" @ %mode @ ".setStateOn(1);";
	eval(%evalShortcut);
}

function NavEditorGui::onEscapePressed(%this) {
	return false;
}


function NavEditorGui::deleteMesh(%this) {
	if (isObject(%this.selectedObject)) {
		%this.selectedObject.delete();
		%this.selectObject(-1);
	}
}

function NavEditorGui::deleteSelected(%this) {
	switch$(%this.getMode()) {
	case "SelectMode":

		// Try to delete the selected NavMesh.
		if (isObject(NavEditorGui.selectedObject))
			MessageBoxYesNo("Warning",
								 "Are you sure you want to delete" SPC NavEditorGui.selectedObject.getName(),
								 "NavEditorGui.deleteMesh();");

	case "TestMode":
		%this.getPlayer().delete();
		%this.onPlayerDeselected();

	case "LinkMode":
		%this.deleteLink();
		%this.isDirty = true;
	}
}

function NavEditorGui::buildSelectedMeshes(%this) {
	if (isObject(%this.getMesh())) {
		%this.getMesh().build(NavEditorGui.backgroundBuild, NavEditorGui.saveIntermediates);
		%this.isDirty = true;
	}
}



//-----------------------------------------------------------------------------

function ENavEditorPaletteButton::onClick(%this) {
	// When clicking on a pelette button, add its description to the bottom of
	// the editor window.
	EditorGuiStatusBar.setInfo(%this.DetailedDesc);
}

//-----------------------------------------------------------------------------
