//==============================================================================
// TorqueLab -> NavEditor Modes Script
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// NavEditor used rollout callback to set current edit mode
//==============================================================================
//==============================================================================
function NavEd::initMarkerData(%this)
{
   
	$NavEd_ObjList_AIMarker =  getMissionObjectClassList("StaticShapeData","isAiMarker true");
	$NavEd_ObjCount_AIMarker = getWordCount($NavEd_ObjList_AIMarker);

	if (!isObject(NavEdAIMarkerSet))
		new SimSet(NavEdAIMarkerSet);

	%aiList = getDatablockClassList("PlayerData");
	foreach$(%id in $NavEd_ObjList_AIMarker)
	{
		NavEdAIMarkerSet.add(%id);

	}
	NavEd_MarkerList.makeNameCallback = "$ThisObject.getMarkerDisplay();";
	NavEd_MarkerList.mirrorSet = NavEdAIMarkerSet;
	NavEd_MarkerList.doMirror();
	
	NavEd.buildMarkerFields();
	
	
	
	
}
//-----------------------------------------------------------------------------
//==============================================================================
function StaticShape::getMarkerDisplay(%this)
{
	if (%this.getName() !$= "")
		%display = %this.getName();

	if (%this.internalName !$= "")
		%display = %display @ "["@%this.internalName@"]";

	if (%display $= "")
		%display = %this.getId();

	return %display;
}
//-----------------------------------------------------------------------------
//==============================================================================
function NavEdMarkerButton::onClick(%this)
{

	%action = %this.internalName;
	switch$(%action)
	{
		case "showall":
			foreach(%obj in NavEdAIMarkerSet)
				%obj.hidden = false;
		//LabObj.update(%obj,"hidden",false);

		case "hideall":
			foreach(%obj in NavEdAIMarkerSet)
				LabObj.update(%obj,"hidden",true);
			//%obj.hidden = true;// Marker stay visible, must pass by slow inspect
		case "toggleall":
			foreach(%obj in NavEdAIMarkerSet)
			{
				if (%obj.hidden)
					%obj.hidden = false;
				else
					LabObj.update(%obj,"hidden",true);
			}


	}



}
//-----------------------------------------------------------------------------

//==============================================================================
function NavEd::updateAIDatablockMenu(%this)
{  

   NavEdMarkerBlockMenu.clear();
	%aiList = getDatablockClassList("PlayerData");
	foreach$(%id in %aiList)
	{
	   if (%id.team $= "")
	      continue;
	      
      if (%defaultData $= "")
         %defaultData = %id;
         
      NavEdMarkerBlockMenu.add(%id.getName(),%id);
	}   

}
//-----------------------------------------------------------------------------
//==============================================================================
function NavEd::updateAIWeaponMenu(%this)
{  

   NavEdMarkerWeaponMenu.clear();
	%weapons = getDatablockClassList("ShapeBaseImageData");
	foreach$(%id in %weapons)
	{
	 
	      
      if (%defaultData $= "")
         %defaultData = %id;
         
      NavEdMarkerWeaponMenu.add(%id.getName(),%id);
	}   

}
//-----------------------------------------------------------------------------