//==============================================================================
// TorqueLab -> 
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

$IPSP_Editor_DEFAULT_FILENAME = "art/gfx/particles/managedParticleData.cs";


//=============================================================================================
//    IPSP_Editor.
//=============================================================================================

//---------------------------------------------------------------------------------------------

function IPSP_Editor::doSystemSave( %this ) {
	%this.doEmitterSave();
	%this.doParticleSave();
}

function IPSP_Editor::refreshParticleList( %this ) {
	
	IPSP_Editor.guiSync(true);
}
function IpsEditor::pickedParticleColor( %this,%color,%colorPicker ) {
	logd("IpsEditor::pickedParticleColor",%color,%colorPicker);
	%colorPicker.baseColor = %color;
	IPSP_Editor.updateParticle("colors["@%colorPicker.internalName@"]",%color,0,0);
}
function IpsEditor::updatedParticleColor( %this ) {
	
	logd("IpsEditor::updatedParticleColor",%color,%arg);
}
function IpsEditor::cancelParticleColor( %this ) {
	
	logd("IpsEditor::cancelParticleColor",%color,%arg);
}
function IpsEditor::getParticleColor( %this,%colorPicker ) {
   %layerId = %colorPicker.internalName;
	%particle = IPSP_Editor.currParticle;
	%currentColor = %particle.colors[%layerId];	
	
	getColorF(%currentColor,"IpsEditor.pickedParticleColor",%colorPicker,"IpsEditor.updatedParticleColor","IpsEditor.cancelParticleColor");
}
