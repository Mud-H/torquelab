//==============================================================================
// GameLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

// Undo actions that are useful in multiple editors.


function Editor::getUndoManager(%this)
{
	if (!isObject(%this.undoManager))
	{
		/// This is the global undo manager used by all
		/// of the mission editor sub-editors.
		%this.undoManager = new UndoManager(EUndoManager)
		{
			numLevels = 200;
			internalName = "DefaultUndoManager";
		};
	}

	return %this.undoManager;
}

function Editor::setUndoManager(%this, %undoMgr)
{
	%this.undoManager = %undoMgr;
}
//=============================================================================================
//    Undo reparenting.
//=============================================================================================

//---------------------------------------------------------------------------------------------

function UndoActionReparentObjects::create(%treeView)
{
	pushInstantGroup();
	%action = new UndoScriptAction()
	{
		class = "UndoActionReparentObjects";
		numObjects = 0;
		treeView = %treeView;
	};
	popInstantGroup();
	return %action;
}

//---------------------------------------------------------------------------------------------

function UndoActionReparentObjects::add(%this, %object, %oldParent, %newParent)
{
	%index = %this.numObjects;
	%this.objects[ %index ] = %object;
	%this.oldParents[ %index ] = %oldParent;
	%this.newParents[ %index ] = %newParent;
	%this.numObjects = %this.numObjects + 1;
}

//---------------------------------------------------------------------------------------------

function UndoActionReparentObjects::undo(%this)
{
	%numObjects = %this.numObjects;

	for(%i = 0; %i < %numObjects; %i ++)
	{
		%obj = %this.objects[ %i ];
		%group = %this.oldParents[ %i ];

		if (isObject(%obj) && isObject(%group))
			%obj.parentGroup = %group;
	}

	if (isObject(%this.treeView))
		%this.treeView.update();
}

//---------------------------------------------------------------------------------------------

function UndoActionReparentObjects::redo(%this)
{
	%numObjects = %this.numObjects;

	for(%i = 0; %i < %numObjects; %i ++)
	{
		%obj = %this.objects[ %i ];
		%group = %this.newParents[ %i ];

		if (isObject(%obj) && isObject(%group))
			%obj.parentGroup = %group;
	}

	if (isObject(%this.treeView))
		%this.treeView.update();
}
