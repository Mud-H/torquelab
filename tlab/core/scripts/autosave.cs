//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================


//==============================================================================
// Handle the escape bind
function scSceneAutoSaveDelayEdit::onValidate(%this)
{
	%time = %this.getText();

	if (!strIsNumeric(%time))
		return;

	%newDelay = mFloatLength(%time,1);

	if (%newDelay $= $Cfg_Common_Saving_AutoSaveDelay)
		return;

	$Cfg_Common_Saving_AutoSaveDelay = %newDelay;
	info("Autosaving delay changed to",$Cfg_Common_Saving_AutoSaveDelay,"minutes");
	Lab.SetAutoSave();
}
//------------------------------------------------------------------------------

//==============================================================================
// Handle the escape bind
function scSceneAutoSaveCheck::onClick(%this)
{
	Lab.SetAutoSave();
	//if ($Cfg_Common_Saving_AutoSaveEnabled)
	//info("Autosaving enabled! Saving each",$Cfg_Common_Saving_AutoSaveDelay,"minutes!");
}
//------------------------------------------------------------------------------
//==============================================================================
// Handle the escape bind
function Lab::SetAutoSave(%this,%state)
{
	if (%state !$= "")
		$Cfg_Common_Saving_AutoSaveEnabled = %state;

	//This is not very good, it should do a backup instead
	if ($Cfg_Common_Saving_AutoSaveEnabled)
	{
		if ($Cfg_Common_Saving_AutoSaveDelay $= "" || !strIsNumeric($Cfg_Common_Saving_AutoSaveDelay))
			$Cfg_Common_Saving_AutoSaveDelay = "1";

		cancel($LabAutoSaveSchedule);
		$LabAutoSaveSchedule = Lab.schedule($Cfg_Common_Saving_AutoSaveDelay * 60000,"AutoSaveScene");

		//$LabAutoSaveSchedule = Lab.schedule(Lab.autoSaveDelay * 60000,"AutoSaveScene");
		if (!Lab.autoBackup)
			info("Auto backup (.bak) enabled! New backup file each",$Cfg_Common_Saving_AutoSaveDelay,"minutes!");

		Lab.autoBackup = true;
	}
	else
	{
		if ($LabAutoSaveSchedule)
			cancel($LabAutoSaveSchedule);

		//cancel($LabAutoSaveSchedule);
		//delObj($LabAutoSaveSchedule);
		if (Lab.autoBackup)
			info("Auto backup disabled");

		Lab.autoBackup = false;
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Handle the escape bind
function Lab::AutoSaveScene(%this)
{
	if (!EditorGui.isAwake() || !$Cfg_Common_Saving_AutoSaveEnabled)
		return;

	%file = $Server::MissionFile@".bak";


	Lab.BackupMission(true);
	info("Scene has been autosaved to:",%file);
	Lab.SetAutoSave();
}
//------------------------------------------------------------------------------
