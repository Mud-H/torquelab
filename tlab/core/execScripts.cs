//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
//
//==============================================================================
//------------------------------------------------------------------------------
//Create Lab Editor Core Objects
exec("tlab/core/initLabEditor.cs");
exec("tlab/core/commonSettings.cs");

//==============================================================================
//Load GameLab system (In-Game Editor)
function tlabExecCore()
{
	execPattern("tlab/core/helpers/*.cs");
	execPattern("tlab/core/classBase/*.cs","",true);
	execPattern("tlab/core/scripts/*.cs","ed.cs");
	execPattern("tlab/core/settings/*.cs","cfg.cs");
	execPattern("tlab/core/menubar/*.cs");
	execPattern("tlab/core/eventManager/*.cs");
}
%execCore = strAddWord(%execCore,"tlabExecCore");
//------------------------------------------------------------------------------

//==============================================================================
//Load GameLab system (In-Game Editor)
function tlabExecHelpers(%loadGui)
{
	if (!isObject(LabParamsDlg))
	{
		execPattern("tlab/core/*.gui");
	}
}
%execInit = strAddWord(%execInit,"tlabExecHelpers");
//------------------------------------------------------------------------------

//==============================================================================
//Load the LabGui (Cleaned EditorGui files)
function tlabExecEditor(%loadGui)
{
	execPattern("tlab/EditorLab/SceneObjects/*.cs");
	execPattern("tlab/EditorLab/guiSystem/*.cs");
	exec("tlab/EditorLab/gui/EditorGui.cs");

	if (%loadGui)
	{
		execGui("tlab/EditorLab/gui/EditorGui.gui");

		execGuiDir("tlab/EditorLab/SceneObjects",true);
		execGuiDir("tlab/EditorLab/gui/GameLab",true);

		execGuiDir("tlab/EditorLab/tools",true);
	}

	exec("tlab/EditorLab/FrameWork/fwInit.cs");
	Lab.initFrameWorkSystem();
	exec("tlab/EditorLab/EditorOpen.cs");
	exec("tlab/EditorLab/EditorClose.cs");
	exec("tlab/EditorLab/EditorScript.cs");
	exec("tlab/EditorLab/EditorActivate.cs");
	exec("tlab/EditorLab/EditorCallbacks.cs");
	exec("tlab/EditorLab/TorqueLabPackage.cs");
	execPattern("tlab/EditorLab/guiSystem/*.cs");
	execPattern("tlab/EditorLab/plugin/*.cs");
	execPattern("tlab/EditorLab/guiSystem/*.cs");
	execPattern("tlab/EditorLab/gui/GameLab/*.cs");
	execPattern("tlab/EditorLab/tools/*.cs");
}
%execInit = strAddWord(%execInit,"tlabExecEditor");
//------------------------------------------------------------------------------

//==============================================================================
//Load the LabGui (Cleaned EditorGui files)
function tlabExecEditorLast(%loadGui)
{
	if (%loadGui)
	{
		//No Guis
	}

	exec("tlab/EditorLab/EditorClose.cs");
}
%execLast = strAddWord(%execLast,"tlabExecEditorLast");
//------------------------------------------------------------------------------

//==============================================================================
//Load the LabGui Ctrl (Cleaned EditorGui files)
function tlabExecGuiLast(%loadGui ,%skip)
{
	if (!isObject(ScriptEditorDlg))
		%ignoreGuiFiles = strAddWord(%ignoreGuiFiles,"scriptEditorDlg",true);

	if (%loadGui)
	{
		execGui("tlab/EditorLab/gui/messageBoxes/LabMsgBoxesGui.gui");

		if (!%skip)
			execGuiDir("tlab/EditorLab/gui/dlgs",true);
       execGuiDir("tlab/EditorLab/gui/dialogs/",true);
	}

	exec("tlab/EditorLab/gui/messageBoxes/LabMsgBoxesGui.cs");

	if (!%skip)
		execPattern("tlab/EditorLab/gui/dlgs/*.cs");

   execPattern("tlab/EditorLab/gui/dialogs/*.cs");
	//Don't do a execPattern on the gui/core, some are load individually
	EditorMap.bindCmd(keyboard, "ctrl g", "toggleDlg(LabTestGui);","");
}
%execLast = strAddWord(%execLast,"tlabExecGuiLast");
//------------------------------------------------------------------------------

//==============================================================================
//Old Settings Dialog for temporary references
function tlabExecTools(%loadGui)
{
	if (%loadGui)
	{
		execGui("tlab/EditorLab/gui/editorTools/ETools.gui");
		//ETools independant guis are loaded from initTools
		execPattern("tlab/EditorLab/gui/editorTools/*.gui");
	}

	execPattern("tlab/EditorLab/gui/editorTools/*.cs");
}
%execLast = strAddWord(%execLast,"tlabExecTools");
//------------------------------------------------------------------------------

//==============================================================================
function execTools(%execGui)
{
	tlabExecTools(%execGui);
}
//------------------------------------------------------------------------------

//==============================================================================
$TLabExecInitCore = %execCore;
$TLabExecInitList = %execInit;
$TLabExecMainList = %execMain;
$TLabExecLastList = %execLast;
//==============================================================================
function tlabExec(%loadGui)
{
	tlabExecList("InitCore",%loadGui);
	tlabExecList("InitList",%loadGui);
	tlabExecList("MainList",%loadGui);
	tlabExecList("LastList",%loadGui);
}
//------------------------------------------------------------------------------

//==============================================================================
function tlabExecList(%list,%loadGui)
{
	if (%loadGui $= "")
		%loadGui = !$LabGuiExeced;

	%execlist = $TLabExec[%list];

	foreach$(%func in %execlist)
	{
		timerStart(%list@"_"@%func);
		eval(%func@"(%loadGui);");
		timerStop();
	}
}
//------------------------------------------------------------------------------
