//==============================================================================
// HelpersLab -> MissionGroup related helpers
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// Add the default Team Spawn Group to Mission
function addDefaultAVSpawns(%addToGroup)
{
	%spawnGroup = new SimGroup()
	{
		internalName = "PlayerDropPoints";
		canSave = "1";
		canSaveDynamicFields = "1";
		byGroup = "0";
		Enabled = "1";

		new SimGroup()
		{
			internalName = "MemberSpawns";
			canSave = "1";
			canSaveDynamicFields = "1";
			byGroup = "0";

			new SpawnSphere()
			{
				autoSpawn = "0";
				spawnTransform = "0";
				radius = "3";
				sphereWeight = "1";
				indoorWeight = "1";
				outdoorWeight = "1";
				isAIControlled = "0";
				dataBlock = "SpawnSphereMarker";
				position = "-3.63377 -4.61441 -0.176512";
				Rotation = "0 0 -1 96.1723";
				scale = "1 1 1";
				canSave = "1";
				canSaveDynamicFields = "1";
				byGroup = "0";
				Enabled = "1";
				homingCount = "0";
				lockCount = "0";
			};
			new SpawnSphere()
			{
				autoSpawn = "0";
				spawnTransform = "0";
				radius = "3";
				sphereWeight = "1";
				indoorWeight = "1";
				outdoorWeight = "1";
				isAIControlled = "0";
				dataBlock = "SpawnSphereMarker";
				position = "-1.05916 -4.17451 -0.176512";
				Rotation = "0 0 1 180";
				scale = "1 1 1";
				canSave = "1";
				canSaveDynamicFields = "1";
				byGroup = "0";
				Enabled = "1";
				homingCount = "0";
				lockCount = "0";
			};
			new SpawnSphere()
			{
				autoSpawn = "0";
				spawnTransform = "0";
				radius = "3";
				sphereWeight = "1";
				indoorWeight = "1";
				outdoorWeight = "1";
				isAIControlled = "0";
				dataBlock = "SpawnSphereMarker";
				position = "1.51544 -4.05761 -0.176512";
				Rotation = "0 0 -1 96.1723";
				scale = "1 1 1";
				canSave = "1";
				canSaveDynamicFields = "1";
				byGroup = "0";
				Enabled = "1";
				homingCount = "0";
				lockCount = "0";
			};
		};
		new SimGroup()
		{
			internalName = "FoeSpawns";
			canSave = "1";
			canSaveDynamicFields = "1";
			byGroup = "0";

			new SpawnSphere()
			{
				autoSpawn = "0";
				spawnTransform = "0";
				radius = "3";
				sphereWeight = "1";
				indoorWeight = "1";
				outdoorWeight = "1";
				isAIControlled = "0";
				dataBlock = "SpawnSphereMarker";
				position = "-7.49287 -1.19501 -0.176512";
				Rotation = "0 0 1 180";
				scale = "1 1 1";
				canSave = "1";
				canSaveDynamicFields = "1";
				byGroup = "0";
				Enabled = "1";
				homingCount = "0";
				lockCount = "0";
			};
			new SpawnSphere()
			{
				autoSpawn = "0";
				spawnTransform = "0";
				radius = "3";
				sphereWeight = "1";
				indoorWeight = "1";
				outdoorWeight = "1";
				isAIControlled = "0";
				dataBlock = "SpawnSphereMarker";
				position = "-7.49287 1.5561 -0.176512";
				Rotation = "0 0 1 180";
				scale = "1 1 1";
				canSave = "1";
				canSaveDynamicFields = "1";
				byGroup = "0";
				Enabled = "1";
				homingCount = "0";
				lockCount = "0";
			};
			new SpawnSphere()
			{
				autoSpawn = "0";
				spawnTransform = "0";
				radius = "3";
				sphereWeight = "1";
				indoorWeight = "1";
				outdoorWeight = "1";
				isAIControlled = "0";
				dataBlock = "SpawnSphereMarker";
				position = "-7.49287 3.9842 -0.176512";
				Rotation = "0 0 1 180";
				scale = "1 1 1";
				canSave = "1";
				canSaveDynamicFields = "1";
				byGroup = "0";
				Enabled = "1";
				homingCount = "0";
				lockCount = "0";
			};
		};
		new SimGroup()
		{
			internalName = "FriendSpawns";
			canSave = "1";
			canSaveDynamicFields = "1";
			byGroup = "0";

			new SpawnSphere()
			{
				autoSpawn = "0";
				spawnTransform = "0";
				radius = "3";
				sphereWeight = "1";
				indoorWeight = "1";
				outdoorWeight = "1";
				isAIControlled = "0";
				dataBlock = "SpawnSphereMarker";
				position = "6.51644 -1.19501 -0.176512";
				Rotation = "0 0 1 180";
				scale = "1 1 1";
				canSave = "1";
				canSaveDynamicFields = "1";
				byGroup = "0";
				Enabled = "1";
				homingCount = "0";
				lockCount = "0";
			};
			new SpawnSphere()
			{
				autoSpawn = "0";
				spawnTransform = "0";
				radius = "3";
				sphereWeight = "1";
				indoorWeight = "1";
				outdoorWeight = "1";
				isAIControlled = "0";
				dataBlock = "SpawnSphereMarker";
				position = "6.51644 1.5561 -0.176512";
				Rotation = "0 0 1 180";
				scale = "1 1 1";
				canSave = "1";
				canSaveDynamicFields = "1";
				byGroup = "0";
				Enabled = "1";
				homingCount = "0";
				lockCount = "0";
			};
			new SpawnSphere()
			{
				autoSpawn = "0";
				spawnTransform = "0";
				radius = "3";
				sphereWeight = "1";
				indoorWeight = "1";
				outdoorWeight = "1";
				isAIControlled = "0";
				dataBlock = "SpawnSphereMarker";
				position = "6.51644 3.9842 -0.176512";
				Rotation = "0 0 1 180";
				scale = "1 1 1";
				canSave = "1";
				canSaveDynamicFields = "1";
				byGroup = "0";
				Enabled = "1";
				homingCount = "0";
				lockCount = "0";
			};
		};
	};

	if (!isObject(%addToGroup))
		%addToGroup = MissionGroup;

	%addToGroup.add(%spawnGroup);

}
