//==============================================================================
// GameLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================

$LabCommentor_StartBlockNames = "function new datablock singleton";
$LabCommentor_CommentPreBlock = "//==============================================================================";
$LabCommentor_CommentPostBlock = "//------------------------------------------------------------------------------";
//==============================================================================
//GuiEd.minimizeFolder("guiTest");
function Lab::commentFolder(%this,%folder)
{
	%pattern = %folder@"/*.gui";
	for (%file = findFirstFile(%pattern); %file !$= ""; %file = findNextFile(%pattern))
	{
		%this.minimizeFile(%file);
	}
}
//==============================================================================
//Lab.commentFile("tmp.cs");
function Lab::commentFile(%this,%filename,%removeEmptyLine)
{

	%src = DefaultGuiFieldValues;
	if ( isWriteableFileName( %filename ) )
	{

		%fileObject = new FileObject();
		%fileObject.openForRead( %filename );

		%lines = -1;

		while ( !%fileObject.isEOF() )
		{
			%lines++;
			%readLine[%lines] = %line;
			%line = %fileObject.readLine();
info(%lines,"CLine",%line);	
			%firstWord = firstWord(%line);
			if (getWordIndex($LabCommentor_StartBlockNames,%firstWord) !$= "-1")
			{
				//The previous line need a commentIntro
				%prev1 =  %readLine[%lines-1];
				if (startsWith(%prev1,"//---") || startsWith(%prev1,"//===") || trim(%prev1) $= "")
			   {
			      
			      info(%lines-1,"Cmment prepended",$LabCommentor_CommentPreBlock);	
			      %readLine[%lines] = $LabCommentor_CommentPreBlock;		      
			      %readLine[%lines++] = %line;
				   continue;
				}
			}
			if (%doCommentBlockPost )
			{
				if (startsWith(%line,"//---") || startsWith(%line,"//===") || trim(%line) $= "")
			   {
				   %readLine[%lines] = $LabCommentor_CommentPostBlock;
				   info(%lines,"Cmment postpended",$LabCommentor_CommentPostBlock);
				   %doCommentBlockPost = false;
				}
			}

			if (strpos(%line,"{") !$= "-1")
			{
				%braceLevel++;
			}
			if (strpos(%line,"}") !$= "-1")
			{
				%braceLevel--;
				if (%braceLevel <= 0)
				{
					devLog("Last Brace found, comment on next line");
					%doCommentBlockPost = true;
				}
			}

		}
		%fileObject.close();
		%fileObject.delete();
		//%fo = new FileObject();
		//%fo.openForWrite(%filename);


		// Write out captured TorqueScript below Gui object
		devLog("==============================>","Start of simulation");
		for ( %i = 0; %i <= %lines; %i++ )
		   info(%i,"Line->", %readLine[ %i ]);
			//%fo.writeLine( %readLine[ %i ] );
		
		//%fo.close();
		//%fo.delete();
	}
	else
		LabMsgOkCancel( "Can't write to the file", "There was an error writing to file '" @ %filename @ "'. The file may be read-only.", "Ok", "Error" );
}
//------------------------------------------------------------------------------
