//==============================================================================
// TorqueLab -> Copy-Paste-Cut-Delete-Deselect
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================



function EditorMenuEditDelete()
{
   devLog("EditorMenuEditDelete");
   
   if ($InGuiEditor)
   {
      GuiEditorDeleteSelection(1);
   }
	else if (isObject(Lab.currentEditor))
	{
		if (Lab.currentEditor.isMethod(handleDelete))
			Lab.currentEditor.handleDelete();
		else
			warnLog("EditorMenuEditDelete missing in current editor",Lab.currentEditor.plugin);
	}
	else
		warnLog("EditorMenuEditDelete no active editor",Lab.currentEditor);
}

function EditorMenuEditDeselect()
{
	if (isObject(Lab.currentEditor))
	{
		if (Lab.currentEditor.isMethod(handleDeselect))
			Lab.currentEditor.handleDeselect();
		else
			warnLog("EditorMenuEditDeselect missing in current editor",Lab.currentEditor.plugin);
	}
	else
		warnLog("EditorMenuEditDeselect no active editor",Lab.currentEditor);
}

function EditorMenuEditCut()
{

	if (isObject(Lab.currentEditor))
	{
		if (Lab.currentEditor.isMethod(handleCut))
			Lab.currentEditor.handleCut();
		else
			warnLog("EditorMenuEditCut missing in current editor",Lab.currentEditor.plugin);
	}
	else
		warnLog("EditorMenuEditCut no active editor",Lab.currentEditor);
}

function EditorMenuEditCopy()
{
	if (isObject(Lab.currentEditor))
	{
		if (Lab.currentEditor.isMethod(handleCopy))
			Lab.currentEditor.handleCopy();
		else
			warnLog("EditorMenuEditCopy missing in current editor",Lab.currentEditor.plugin);
	}
	else
		warnLog("EditorMenuEditCopy no active editor",Lab.currentEditor);
}

function EditorMenuEditPaste()
{
	if (isObject(Lab.currentEditor))
	{
		if (Lab.currentEditor.isMethod(handlePaste))
			Lab.currentEditor.handlePaste();
		else
			warnLog("EditorMenuEditPaste missing in current editor",Lab.currentEditor.plugin);
	}
	else
		warnLog("EditorMenuEditPaste no active editor",Lab.currentEditor);

}
