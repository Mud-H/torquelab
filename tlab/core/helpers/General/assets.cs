//==============================================================================
// HelpersLab -> ForestData Brush and Items Generator
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================
// Start forest data generation functions
//==============================================================================


//==============================================================================
function trackFileChanges(%enabled)
{
	if (%enabled $= "")
		%enabled = !$TLabTrackingFileChange;


	if (%enabled)
		startFileChangeNotifications();
	else
		stopFileChangeNotifications();

	$TLabTrackingFileChange = %enabled;
}
//------------------------------------------------------------------------------
