//==============================================================================
// TorqueLab ->
// Copyright (c) 2015 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
//------------------------------------------------------------------------------
// Create the EventManager.
if (!isObject($EManager))
{
	$EManager = new EventManager(EventLab)
	{
		queue = "EManager";
	};
}

//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function Lab::activateEventManager(%this)
{
	setEvent("SceneChanged");

	setEvent("SceneSelectionChanged");
   setEvent("SceneObjectAddedChanged");
}

function setEvent(%event)
{
	if (EventLab.isRegisteredEvent(%event))
		return;

	// Trigger the event.
	$EManager.registerEvent(%event);
}
function postEvent(%event, %data)
{
	if (!$EManager.isRegisteredEvent(%event))
		$EManager.registerEvent(%event);

	// Trigger the event.
	$EManager.postEvent(%event, %data);
}
function joinEvent(%event, %listener)
{
	if (!isObject(%listener))
	{
		%class = "LabListener";

		if (%listener !$= "")
			%class = %listener;

		%listener = new ScriptMsgListener()
		{
			// class = "LabMsgListener";
			superClass = %class;
		};
	}


	// Trigger the event.
	$EManager.subscribe(%listener,  %event);
	return %listener;
}

function leaveEvent(%event, %listener)
{	
	// Trigger the event.
	$EManager.remove(%listener,  %event);
	return true;
}
//==============================================================================
// EventManager Example
//==============================================================================
/*
// Create the EventManager.
$MyEventManager = new EventManager() { queue = "MyEventManager"; };

// Create an event.
$MyEventManager.registerEvent( "SomeCoolEvent" );

// Create a listener and subscribe.
$MyListener = new ScriptMsgListener() { class = MyListener; };
$MyEventManager.subscribe( $MyListener, "SomeCoolEvent" );

function MyListener::onSomeCoolEvent( %this, %data )
{
     echo( "onSomeCoolEvent Triggered" );
}

// Trigger the event.
$MyEventManager.postEvent( "SomeCoolEvent", "Data" );
*/
//==============================================================================
// EventManager References
//==============================================================================
/*
void 	dumpEvents ()
 	Print all registered events to the console.
void 	dumpSubscribers (String event)
 	Print all subscribers to an event to the console.
bool 	isRegisteredEvent (String event)
 	Check if an event is registered or not.
bool 	postEvent (String event, String data)
 	~Trigger an event.
bool 	registerEvent (String event)
 	Register an event with the event manager.
void 	remove (SimObject listener, String event)
 	Remove a listener from an event.
void 	removeAll (SimObject listener)
 	Remove a listener from all events.
bool 	subscribe (SimObject listener, String event, String callback)
 	Subscribe a listener to an event.
void 	unregisterEvent (String event)
 	Remove an event from the EventManager.
 	*/
//==============================================================================
// ScriptMsgListener References
//==============================================================================
/*
Script accessible version of Dispatcher::IMessageListener. Often used in conjunction with EventManager.

The main use of ScriptMsgListener is to allow script to listen formessages. You can subclass ScriptMsgListener in script to receivethe Dispatcher::IMessageListener callbacks.

Alternatively, you can derive from it in C++ instead of SimObject toget an object that implements Dispatcher::IMessageListener with scriptcallbacks.
If you need to derive from something other then SimObject,then you will need to implement the Dispatcher::IMessageListenerinterface yourself.

void 	onAdd ()
 	Script callback when a listener is first created and registered.
void 	onAddToQueue (string queue)
 	Callback for when the listener is added to a queue.
bool 	onMessageObjectReceived (string queue, Message msg)
 	Called when a message object (not just the message data) is passed to a listener.
bool 	onMessageReceived (string queue, string event, string data)
 	Called when the listener has received a message.
void 	onRemove ()
 	Script callback when a listener is deleted.
void 	onRemoveFromQueue (string queue)
 	Callback for when the listener is removed from a queue.
*/
